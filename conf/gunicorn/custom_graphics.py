NAME = 'custom_graphics'
# gunicorn can reload source files while this is set to True
# you, probably, want to set it to False if on production
reload = False

bind = 'unix:/tmp/{basename}.sock'.format(basename=NAME)
pidfile = '/tmp/{basename}.pid'.format(basename=NAME)
daemon = False
keepalive = 2

import os
import multiprocessing

workers = 2 * multiprocessing.cpu_count()
# threads = 3 * multiprocessing.cpu_count() # do not work

# logging
accesslog =  '/var/www/custom_graphics_project/custom-graphics-erp/logs/gunicorn/access.log'
errorlog =  '/var/www/custom_graphics_project/custom-graphics-erp/logs/gunicorn/errors.log'

chdir = '/var/www/custom_graphics_project/custom-graphics-erp/'
pythonpath = chdir

