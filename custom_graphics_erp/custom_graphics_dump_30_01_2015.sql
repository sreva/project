--
-- PostgreSQL database dump
--

SET statement_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;

--
-- Name: plpgsql; Type: EXTENSION; Schema: -; Owner: 
--

CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;


--
-- Name: EXTENSION plpgsql; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';


SET search_path = public, pg_catalog;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: auth_group; Type: TABLE; Schema: public; Owner: stas; Tablespace: 
--

CREATE TABLE auth_group (
    id integer NOT NULL,
    name character varying(80) NOT NULL
);


ALTER TABLE public.auth_group OWNER TO stas;

--
-- Name: auth_group_id_seq; Type: SEQUENCE; Schema: public; Owner: stas
--

CREATE SEQUENCE auth_group_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.auth_group_id_seq OWNER TO stas;

--
-- Name: auth_group_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: stas
--

ALTER SEQUENCE auth_group_id_seq OWNED BY auth_group.id;


--
-- Name: auth_group_permissions; Type: TABLE; Schema: public; Owner: stas; Tablespace: 
--

CREATE TABLE auth_group_permissions (
    id integer NOT NULL,
    group_id integer NOT NULL,
    permission_id integer NOT NULL
);


ALTER TABLE public.auth_group_permissions OWNER TO stas;

--
-- Name: auth_group_permissions_id_seq; Type: SEQUENCE; Schema: public; Owner: stas
--

CREATE SEQUENCE auth_group_permissions_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.auth_group_permissions_id_seq OWNER TO stas;

--
-- Name: auth_group_permissions_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: stas
--

ALTER SEQUENCE auth_group_permissions_id_seq OWNED BY auth_group_permissions.id;


--
-- Name: auth_permission; Type: TABLE; Schema: public; Owner: stas; Tablespace: 
--

CREATE TABLE auth_permission (
    id integer NOT NULL,
    name character varying(50) NOT NULL,
    content_type_id integer NOT NULL,
    codename character varying(100) NOT NULL
);


ALTER TABLE public.auth_permission OWNER TO stas;

--
-- Name: auth_permission_id_seq; Type: SEQUENCE; Schema: public; Owner: stas
--

CREATE SEQUENCE auth_permission_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.auth_permission_id_seq OWNER TO stas;

--
-- Name: auth_permission_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: stas
--

ALTER SEQUENCE auth_permission_id_seq OWNED BY auth_permission.id;


--
-- Name: django_admin_log; Type: TABLE; Schema: public; Owner: stas; Tablespace: 
--

CREATE TABLE django_admin_log (
    id integer NOT NULL,
    action_time timestamp with time zone NOT NULL,
    object_id text,
    object_repr character varying(200) NOT NULL,
    action_flag smallint NOT NULL,
    change_message text NOT NULL,
    content_type_id integer,
    user_id integer NOT NULL,
    CONSTRAINT django_admin_log_action_flag_check CHECK ((action_flag >= 0))
);


ALTER TABLE public.django_admin_log OWNER TO stas;

--
-- Name: django_admin_log_id_seq; Type: SEQUENCE; Schema: public; Owner: stas
--

CREATE SEQUENCE django_admin_log_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.django_admin_log_id_seq OWNER TO stas;

--
-- Name: django_admin_log_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: stas
--

ALTER SEQUENCE django_admin_log_id_seq OWNED BY django_admin_log.id;


--
-- Name: django_content_type; Type: TABLE; Schema: public; Owner: stas; Tablespace: 
--

CREATE TABLE django_content_type (
    id integer NOT NULL,
    name character varying(100) NOT NULL,
    app_label character varying(100) NOT NULL,
    model character varying(100) NOT NULL
);


ALTER TABLE public.django_content_type OWNER TO stas;

--
-- Name: django_content_type_id_seq; Type: SEQUENCE; Schema: public; Owner: stas
--

CREATE SEQUENCE django_content_type_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.django_content_type_id_seq OWNER TO stas;

--
-- Name: django_content_type_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: stas
--

ALTER SEQUENCE django_content_type_id_seq OWNED BY django_content_type.id;


--
-- Name: django_migrations; Type: TABLE; Schema: public; Owner: stas; Tablespace: 
--

CREATE TABLE django_migrations (
    id integer NOT NULL,
    app character varying(255) NOT NULL,
    name character varying(255) NOT NULL,
    applied timestamp with time zone NOT NULL
);


ALTER TABLE public.django_migrations OWNER TO stas;

--
-- Name: django_migrations_id_seq; Type: SEQUENCE; Schema: public; Owner: stas
--

CREATE SEQUENCE django_migrations_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.django_migrations_id_seq OWNER TO stas;

--
-- Name: django_migrations_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: stas
--

ALTER SEQUENCE django_migrations_id_seq OWNED BY django_migrations.id;


--
-- Name: django_session; Type: TABLE; Schema: public; Owner: stas; Tablespace: 
--

CREATE TABLE django_session (
    session_key character varying(40) NOT NULL,
    session_data text NOT NULL,
    expire_date timestamp with time zone NOT NULL
);


ALTER TABLE public.django_session OWNER TO stas;

--
-- Name: office_attachment; Type: TABLE; Schema: public; Owner: stas; Tablespace: 
--

CREATE TABLE office_attachment (
    id integer NOT NULL,
    attachment_file character varying(100) NOT NULL,
    request_id integer NOT NULL
);


ALTER TABLE public.office_attachment OWNER TO stas;

--
-- Name: office_attachment_id_seq; Type: SEQUENCE; Schema: public; Owner: stas
--

CREATE SEQUENCE office_attachment_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.office_attachment_id_seq OWNER TO stas;

--
-- Name: office_attachment_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: stas
--

ALTER SEQUENCE office_attachment_id_seq OWNED BY office_attachment.id;


--
-- Name: office_company; Type: TABLE; Schema: public; Owner: stas; Tablespace: 
--

CREATE TABLE office_company (
    id integer NOT NULL,
    name character varying(100),
    email character varying(75),
    phone character varying(50),
    category_id integer,
    city character varying(100),
    website character varying(200),
    address character varying(1000),
    state character varying(200),
    post_code character varying(200),
    description text
);


ALTER TABLE public.office_company OWNER TO stas;

--
-- Name: office_company_id_seq; Type: SEQUENCE; Schema: public; Owner: stas
--

CREATE SEQUENCE office_company_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.office_company_id_seq OWNER TO stas;

--
-- Name: office_company_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: stas
--

ALTER SEQUENCE office_company_id_seq OWNED BY office_company.id;


--
-- Name: office_companycategory; Type: TABLE; Schema: public; Owner: stas; Tablespace: 
--

CREATE TABLE office_companycategory (
    id integer NOT NULL,
    title character varying(200) NOT NULL
);


ALTER TABLE public.office_companycategory OWNER TO stas;

--
-- Name: office_companycategory_id_seq; Type: SEQUENCE; Schema: public; Owner: stas
--

CREATE SEQUENCE office_companycategory_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.office_companycategory_id_seq OWNER TO stas;

--
-- Name: office_companycategory_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: stas
--

ALTER SEQUENCE office_companycategory_id_seq OWNED BY office_companycategory.id;


--
-- Name: office_contactperson; Type: TABLE; Schema: public; Owner: stas; Tablespace: 
--

CREATE TABLE office_contactperson (
    id integer NOT NULL,
    company_id integer NOT NULL,
    name character varying(200) NOT NULL,
    email character varying(75) NOT NULL,
    phone character varying(50) NOT NULL,
    "position" character varying(100) NOT NULL
);


ALTER TABLE public.office_contactperson OWNER TO stas;

--
-- Name: office_contactperson_id_seq; Type: SEQUENCE; Schema: public; Owner: stas
--

CREATE SEQUENCE office_contactperson_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.office_contactperson_id_seq OWNER TO stas;

--
-- Name: office_contactperson_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: stas
--

ALTER SEQUENCE office_contactperson_id_seq OWNED BY office_contactperson.id;


--
-- Name: office_customertype; Type: TABLE; Schema: public; Owner: stas; Tablespace: 
--

CREATE TABLE office_customertype (
    id integer NOT NULL,
    title character varying(200) NOT NULL,
    index_sort integer NOT NULL,
    material double precision NOT NULL,
    tisk double precision NOT NULL
);


ALTER TABLE public.office_customertype OWNER TO stas;

--
-- Name: office_customertype_id_seq; Type: SEQUENCE; Schema: public; Owner: stas
--

CREATE SEQUENCE office_customertype_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.office_customertype_id_seq OWNER TO stas;

--
-- Name: office_customertype_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: stas
--

ALTER SEQUENCE office_customertype_id_seq OWNED BY office_customertype.id;


--
-- Name: office_lamination; Type: TABLE; Schema: public; Owner: stas; Tablespace: 
--

CREATE TABLE office_lamination (
    id integer NOT NULL,
    title character varying(200) NOT NULL,
    index_sort integer NOT NULL,
    value double precision NOT NULL
);


ALTER TABLE public.office_lamination OWNER TO stas;

--
-- Name: office_lamination_id_seq; Type: SEQUENCE; Schema: public; Owner: stas
--

CREATE SEQUENCE office_lamination_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.office_lamination_id_seq OWNER TO stas;

--
-- Name: office_lamination_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: stas
--

ALTER SEQUENCE office_lamination_id_seq OWNED BY office_lamination.id;


--
-- Name: office_material; Type: TABLE; Schema: public; Owner: stas; Tablespace: 
--

CREATE TABLE office_material (
    id integer NOT NULL,
    title character varying(200) NOT NULL,
    index_sort integer NOT NULL,
    value double precision NOT NULL
);


ALTER TABLE public.office_material OWNER TO stas;

--
-- Name: office_material_id_seq; Type: SEQUENCE; Schema: public; Owner: stas
--

CREATE SEQUENCE office_material_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.office_material_id_seq OWNER TO stas;

--
-- Name: office_material_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: stas
--

ALTER SEQUENCE office_material_id_seq OWNED BY office_material.id;


--
-- Name: office_myuser; Type: TABLE; Schema: public; Owner: stas; Tablespace: 
--

CREATE TABLE office_myuser (
    id integer NOT NULL,
    password character varying(128) NOT NULL,
    last_login timestamp with time zone NOT NULL,
    email character varying(255) NOT NULL,
    date_of_birth date,
    is_active boolean NOT NULL,
    is_admin boolean NOT NULL,
    name character varying(400) NOT NULL,
    phone character varying(40) NOT NULL,
    group_id integer
);


ALTER TABLE public.office_myuser OWNER TO stas;

--
-- Name: office_myuser_id_seq; Type: SEQUENCE; Schema: public; Owner: stas
--

CREATE SEQUENCE office_myuser_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.office_myuser_id_seq OWNER TO stas;

--
-- Name: office_myuser_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: stas
--

ALTER SEQUENCE office_myuser_id_seq OWNED BY office_myuser.id;


--
-- Name: office_organizer; Type: TABLE; Schema: public; Owner: stas; Tablespace: 
--

CREATE TABLE office_organizer (
    id integer NOT NULL,
    text text NOT NULL,
    date timestamp with time zone NOT NULL,
    user_id integer NOT NULL
);


ALTER TABLE public.office_organizer OWNER TO stas;

--
-- Name: office_organizer_id_seq; Type: SEQUENCE; Schema: public; Owner: stas
--

CREATE SEQUENCE office_organizer_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.office_organizer_id_seq OWNER TO stas;

--
-- Name: office_organizer_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: stas
--

ALTER SEQUENCE office_organizer_id_seq OWNED BY office_organizer.id;


--
-- Name: office_plot; Type: TABLE; Schema: public; Owner: stas; Tablespace: 
--

CREATE TABLE office_plot (
    id integer NOT NULL,
    title character varying(200) NOT NULL,
    index_sort integer NOT NULL,
    value double precision NOT NULL
);


ALTER TABLE public.office_plot OWNER TO stas;

--
-- Name: office_plot_id_seq; Type: SEQUENCE; Schema: public; Owner: stas
--

CREATE SEQUENCE office_plot_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.office_plot_id_seq OWNER TO stas;

--
-- Name: office_plot_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: stas
--

ALTER SEQUENCE office_plot_id_seq OWNED BY office_plot.id;


--
-- Name: office_plotpricesam; Type: TABLE; Schema: public; Owner: stas; Tablespace: 
--

CREATE TABLE office_plotpricesam (
    id integer NOT NULL,
    title character varying(200) NOT NULL,
    index_sort integer NOT NULL,
    value double precision NOT NULL
);


ALTER TABLE public.office_plotpricesam OWNER TO stas;

--
-- Name: office_plotpricesam_id_seq; Type: SEQUENCE; Schema: public; Owner: stas
--

CREATE SEQUENCE office_plotpricesam_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.office_plotpricesam_id_seq OWNER TO stas;

--
-- Name: office_plotpricesam_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: stas
--

ALTER SEQUENCE office_plotpricesam_id_seq OWNED BY office_plotpricesam.id;


--
-- Name: office_print; Type: TABLE; Schema: public; Owner: stas; Tablespace: 
--

CREATE TABLE office_print (
    id integer NOT NULL,
    title character varying(200) NOT NULL,
    index_sort integer NOT NULL,
    value double precision NOT NULL
);


ALTER TABLE public.office_print OWNER TO stas;

--
-- Name: office_print_id_seq; Type: SEQUENCE; Schema: public; Owner: stas
--

CREATE SEQUENCE office_print_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.office_print_id_seq OWNER TO stas;

--
-- Name: office_print_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: stas
--

ALTER SEQUENCE office_print_id_seq OWNED BY office_print.id;


--
-- Name: office_request; Type: TABLE; Schema: public; Owner: stas; Tablespace: 
--

CREATE TABLE office_request (
    id integer NOT NULL,
    contact_person_id integer,
    company_id integer,
    order_details text,
    price integer,
    payment boolean NOT NULL,
    sales_manager_id integer,
    office_manager_id integer,
    graphics_user_id integer,
    graphics_description text,
    graphics_date_start timestamp with time zone,
    graphics_date_end timestamp with time zone,
    installation_user_id integer,
    installation_description text,
    installation_date_start timestamp with time zone,
    installation_date_end timestamp with time zone,
    product_user_id integer,
    product_description text,
    product_date_start timestamp with time zone,
    product_date_end timestamp with time zone,
    status character varying(50) NOT NULL,
    sending_date timestamp with time zone,
    calculation_json text,
    graphics_min integer,
    installation_min integer,
    product_min integer,
    graphics_finish boolean NOT NULL,
    installation_finish boolean NOT NULL,
    product_finish boolean NOT NULL,
    quotation_declined text,
    sent_invoice boolean NOT NULL
);


ALTER TABLE public.office_request OWNER TO stas;

--
-- Name: office_request_id_seq; Type: SEQUENCE; Schema: public; Owner: stas
--

CREATE SEQUENCE office_request_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.office_request_id_seq OWNER TO stas;

--
-- Name: office_request_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: stas
--

ALTER SEQUENCE office_request_id_seq OWNED BY office_request.id;


--
-- Name: office_settingscontent; Type: TABLE; Schema: public; Owner: stas; Tablespace: 
--

CREATE TABLE office_settingscontent (
    id integer NOT NULL,
    title character varying(300) NOT NULL,
    key character varying(100) NOT NULL,
    image character varying(100),
    is_image boolean NOT NULL,
    text text,
    is_text boolean NOT NULL
);


ALTER TABLE public.office_settingscontent OWNER TO stas;

--
-- Name: office_settingscontent_id_seq; Type: SEQUENCE; Schema: public; Owner: stas
--

CREATE SEQUENCE office_settingscontent_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.office_settingscontent_id_seq OWNER TO stas;

--
-- Name: office_settingscontent_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: stas
--

ALTER SEQUENCE office_settingscontent_id_seq OWNED BY office_settingscontent.id;


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: stas
--

ALTER TABLE ONLY auth_group ALTER COLUMN id SET DEFAULT nextval('auth_group_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: stas
--

ALTER TABLE ONLY auth_group_permissions ALTER COLUMN id SET DEFAULT nextval('auth_group_permissions_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: stas
--

ALTER TABLE ONLY auth_permission ALTER COLUMN id SET DEFAULT nextval('auth_permission_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: stas
--

ALTER TABLE ONLY django_admin_log ALTER COLUMN id SET DEFAULT nextval('django_admin_log_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: stas
--

ALTER TABLE ONLY django_content_type ALTER COLUMN id SET DEFAULT nextval('django_content_type_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: stas
--

ALTER TABLE ONLY django_migrations ALTER COLUMN id SET DEFAULT nextval('django_migrations_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: stas
--

ALTER TABLE ONLY office_attachment ALTER COLUMN id SET DEFAULT nextval('office_attachment_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: stas
--

ALTER TABLE ONLY office_company ALTER COLUMN id SET DEFAULT nextval('office_company_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: stas
--

ALTER TABLE ONLY office_companycategory ALTER COLUMN id SET DEFAULT nextval('office_companycategory_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: stas
--

ALTER TABLE ONLY office_contactperson ALTER COLUMN id SET DEFAULT nextval('office_contactperson_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: stas
--

ALTER TABLE ONLY office_customertype ALTER COLUMN id SET DEFAULT nextval('office_customertype_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: stas
--

ALTER TABLE ONLY office_lamination ALTER COLUMN id SET DEFAULT nextval('office_lamination_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: stas
--

ALTER TABLE ONLY office_material ALTER COLUMN id SET DEFAULT nextval('office_material_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: stas
--

ALTER TABLE ONLY office_myuser ALTER COLUMN id SET DEFAULT nextval('office_myuser_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: stas
--

ALTER TABLE ONLY office_organizer ALTER COLUMN id SET DEFAULT nextval('office_organizer_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: stas
--

ALTER TABLE ONLY office_plot ALTER COLUMN id SET DEFAULT nextval('office_plot_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: stas
--

ALTER TABLE ONLY office_plotpricesam ALTER COLUMN id SET DEFAULT nextval('office_plotpricesam_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: stas
--

ALTER TABLE ONLY office_print ALTER COLUMN id SET DEFAULT nextval('office_print_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: stas
--

ALTER TABLE ONLY office_request ALTER COLUMN id SET DEFAULT nextval('office_request_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: stas
--

ALTER TABLE ONLY office_settingscontent ALTER COLUMN id SET DEFAULT nextval('office_settingscontent_id_seq'::regclass);


--
-- Data for Name: auth_group; Type: TABLE DATA; Schema: public; Owner: stas
--

COPY auth_group (id, name) FROM stdin;
1	Sales manager
3	Office manager
4	Graphics manager
5	Production manager
6	Installation manager
2	General manager
7	Admin
\.


--
-- Name: auth_group_id_seq; Type: SEQUENCE SET; Schema: public; Owner: stas
--

SELECT pg_catalog.setval('auth_group_id_seq', 7, true);


--
-- Data for Name: auth_group_permissions; Type: TABLE DATA; Schema: public; Owner: stas
--

COPY auth_group_permissions (id, group_id, permission_id) FROM stdin;
\.


--
-- Name: auth_group_permissions_id_seq; Type: SEQUENCE SET; Schema: public; Owner: stas
--

SELECT pg_catalog.setval('auth_group_permissions_id_seq', 1, false);


--
-- Data for Name: auth_permission; Type: TABLE DATA; Schema: public; Owner: stas
--

COPY auth_permission (id, name, content_type_id, codename) FROM stdin;
1	Can add log entry	1	add_logentry
2	Can change log entry	1	change_logentry
3	Can delete log entry	1	delete_logentry
4	Can add permission	2	add_permission
5	Can change permission	2	change_permission
6	Can delete permission	2	delete_permission
7	Can add group	3	add_group
8	Can change group	3	change_group
9	Can delete group	3	delete_group
10	Can add content type	4	add_contenttype
11	Can change content type	4	change_contenttype
12	Can delete content type	4	delete_contenttype
13	Can add session	5	add_session
14	Can change session	5	change_session
15	Can delete session	5	delete_session
16	Can add my user	6	add_myuser
17	Can change my user	6	change_myuser
18	Can delete my user	6	delete_myuser
19	Can add company category	7	add_companycategory
20	Can change company category	7	change_companycategory
21	Can delete company category	7	delete_companycategory
22	Can add company	8	add_company
23	Can change company	8	change_company
24	Can delete company	8	delete_company
25	Can add contact person	9	add_contactperson
26	Can change contact person	9	change_contactperson
27	Can delete contact person	9	delete_contactperson
28	Can add request	10	add_request
29	Can change request	10	change_request
30	Can delete request	10	delete_request
31	Can add attachment	11	add_attachment
32	Can change attachment	11	change_attachment
33	Can delete attachment	11	delete_attachment
34	Can add organizer	12	add_organizer
35	Can change organizer	12	change_organizer
36	Can delete organizer	12	delete_organizer
37	Can add material	13	add_material
38	Can change material	13	change_material
39	Can delete material	13	delete_material
40	Can add lamination	14	add_lamination
41	Can change lamination	14	change_lamination
42	Can delete lamination	14	delete_lamination
43	Can add Plot price sam	15	add_plotpricesam
44	Can change Plot price sam	15	change_plotpricesam
45	Can delete Plot price sam	15	delete_plotpricesam
46	Can add customer type	16	add_customertype
47	Can change customer type	16	change_customertype
48	Can delete customer type	16	delete_customertype
49	Can add print	17	add_print
50	Can change print	17	change_print
51	Can delete print	17	delete_print
52	Can add plot	18	add_plot
53	Can change plot	18	change_plot
54	Can delete plot	18	delete_plot
55	Can add settings content	19	add_settingscontent
56	Can change settings content	19	change_settingscontent
57	Can delete settings content	19	delete_settingscontent
\.


--
-- Name: auth_permission_id_seq; Type: SEQUENCE SET; Schema: public; Owner: stas
--

SELECT pg_catalog.setval('auth_permission_id_seq', 57, true);


--
-- Data for Name: django_admin_log; Type: TABLE DATA; Schema: public; Owner: stas
--

COPY django_admin_log (id, action_time, object_id, object_repr, action_flag, change_message, content_type_id, user_id) FROM stdin;
1	2014-12-23 13:07:10.637483+01	1	CompanyCategory object	1		7	2
2	2014-12-23 13:07:15.49232+01	2	CompanyCategory object	1		7	2
3	2014-12-23 13:07:17.86363+01	1	Company object	1		8	2
4	2014-12-23 13:07:27.29075+01	2	Company object	1		8	2
5	2014-12-23 13:17:55.578822+01	1	ContactPerson object	1		9	2
6	2014-12-23 14:36:50.756729+01	2	ContactPerson object	1		9	2
7	2014-12-23 15:41:31.609449+01	1	Request object	2	Deleted attachment "Attachment object". Deleted attachment "Attachment object".	10	2
8	2014-12-23 15:51:21.829185+01	1	Request object	2	Deleted attachment "Attachment object". Deleted attachment "Attachment object". Deleted attachment "Attachment object". Deleted attachment "Attachment object". Deleted attachment "Attachment object".	10	2
9	2014-12-23 16:46:31.019718+01	2	Request object	2	Changed sales_manager.	10	2
10	2014-12-23 16:47:07.974229+01	1	Request object	2	Changed sales_manager.	10	2
11	2014-12-23 16:51:55.253917+01	2	Request object	2	Changed sending_date.	10	2
12	2014-12-23 16:52:04.48271+01	1	Request object	2	Changed sending_date.	10	2
13	2014-12-25 15:42:31.535882+01	1	material1	1		13	2
14	2014-12-25 15:42:40.003663+01	2	material2	1		13	2
15	2014-12-25 15:42:51.633756+01	3	material3	1		13	2
16	2014-12-25 16:30:18.316867+01	1	1111	1		14	2
17	2014-12-25 16:30:24.056306+01	2	222222	1		14	2
18	2014-12-25 16:30:46.769963+01	1	1111111111	1		18	2
19	2014-12-25 16:30:48.981391+01	1	1111111111	2	No fields changed.	18	2
20	2014-12-25 16:30:55.198745+01	2	22222222222	1		18	2
21	2014-12-25 16:31:06.403086+01	1	11	1		15	2
22	2014-12-25 16:31:12.399907+01	2	22	1		15	2
23	2014-12-25 16:33:22.463597+01	1	1234	1		17	2
24	2014-12-25 16:33:29.254127+01	2	234	1		17	2
25	2014-12-25 16:33:34.510378+01	2	234	2	Changed index_sort.	17	2
26	2014-12-26 01:04:23.004886+01	4	sales@4-com.pro	1		6	3
27	2014-12-26 01:35:04.518564+01	2	CompanyCategory object	2	Changed title.	7	3
28	2014-12-26 01:35:10.106116+01	1	CompanyCategory object	2	Changed title.	7	3
29	2014-12-26 01:35:14.233262+01	1	CompanyCategory object	2	Changed title.	7	3
30	2014-12-26 01:37:02.746141+01	1	Company object	2	Changed name.	8	3
31	2014-12-26 01:37:07.864799+01	2	Company object	2	Changed name.	8	3
32	2014-12-26 01:37:12.514111+01	3	Company object	2	Changed name.	8	3
33	2014-12-26 01:38:27.728488+01	1	ContactPerson object	2	Changed name.	9	3
34	2014-12-26 01:38:42.024181+01	2	ContactPerson object	2	Changed name.	9	3
35	2014-12-26 01:38:56.343286+01	1	ContactPerson object	2	Changed position.	9	3
36	2014-12-26 01:39:00.835116+01	2	ContactPerson object	2	Changed position.	9	3
37	2014-12-26 09:40:58.721381+01	2	material2	2	Changed value.	13	2
38	2014-12-26 09:41:05.899664+01	3	material3	2	Changed value.	13	2
39	2014-12-26 10:11:40.969132+01	5	gin.rad@gmail.com	1		6	2
40	2014-12-26 10:11:51.377192+01	5	gin.rad@gmail.com	2	Changed is_admin.	6	2
41	2014-12-26 11:38:53.734721+01	1	type1	1		16	2
42	2014-12-26 11:39:05.245408+01	2	type2	1		16	2
43	2014-12-27 01:29:29.671984+01	3	Organizer object	1		12	3
44	2014-12-27 13:31:51.084495+01	1	Request object	2	Added attachment "Attachment object".	10	2
45	2014-12-27 13:31:51.282973+01	1	Request object	2	Added attachment "Attachment object".	10	2
46	2014-12-27 13:32:21.913248+01	1	Request object	2	Deleted attachment "Attachment object". Deleted attachment "Attachment object". Deleted attachment "Attachment object". Deleted attachment "Attachment object".	10	2
47	2014-12-27 13:32:55.327972+01	1	Request object	2	Added attachment "Attachment object". Added attachment "Attachment object".	10	2
48	2014-12-27 14:10:12.102648+01	15	Request object	3		10	2
49	2014-12-27 14:10:12.27971+01	14	Request object	3		10	2
50	2014-12-27 14:10:12.455313+01	13	Request object	3		10	2
51	2014-12-27 14:10:12.631144+01	12	Request object	3		10	2
52	2014-12-27 14:10:12.807633+01	11	Request object	3		10	2
53	2014-12-27 14:10:12.983738+01	10	Request object	3		10	2
54	2014-12-27 14:10:13.16014+01	9	Request object	3		10	2
55	2014-12-27 14:10:13.335915+01	8	Request object	3		10	2
56	2014-12-27 14:10:13.512269+01	7	Request object	3		10	2
57	2014-12-27 14:10:13.688755+01	6	Request object	3		10	2
58	2014-12-27 14:10:13.865194+01	5	Request object	3		10	2
59	2014-12-27 14:10:14.042101+01	4	Request object	3		10	2
60	2014-12-27 14:10:14.217769+01	3	Request object	3		10	2
61	2014-12-27 14:10:14.393925+01	2	Request object	3		10	2
62	2014-12-27 14:10:14.569859+01	1	Request object	3		10	2
63	2014-12-27 14:22:52.820739+01	17	Request object	3		10	2
64	2014-12-27 14:22:52.972411+01	16	Request object	3		10	2
65	2014-12-27 14:40:58.381472+01	23	Request object	3		10	2
66	2014-12-27 14:40:58.558778+01	22	Request object	3		10	2
67	2014-12-27 14:40:58.733885+01	21	Request object	3		10	2
68	2014-12-27 14:40:58.9096+01	20	Request object	3		10	2
69	2014-12-27 14:40:59.08499+01	19	Request object	3		10	2
70	2014-12-27 14:40:59.259891+01	18	Request object	3		10	2
71	2014-12-28 03:09:19.349412+01	32	Request object	3		10	3
72	2014-12-28 03:09:19.351333+01	31	Request object	3		10	3
73	2014-12-28 03:09:19.352417+01	30	Request object	3		10	3
74	2014-12-28 03:09:19.353464+01	29	Request object	3		10	3
75	2014-12-28 03:09:19.35443+01	28	Request object	3		10	3
76	2014-12-28 03:09:19.355557+01	27	Request object	3		10	3
77	2014-12-28 03:09:19.356617+01	26	Request object	3		10	3
78	2014-12-28 03:09:19.357697+01	25	Request object	3		10	3
79	2014-12-28 03:09:19.358662+01	24	Request object	3		10	3
80	2014-12-28 03:37:12.342896+01	3	material3	3		13	3
81	2014-12-28 03:37:12.345217+01	2	material2	3		13	3
82	2014-12-28 03:37:12.34741+01	1	material1	3		13	3
83	2014-12-28 03:37:25.42773+01	4	NO	1		13	3
84	2014-12-28 03:37:32.475328+01	5	CWF1-\\MPI 1005SC\\	1		13	3
85	2014-12-28 03:37:42.976836+01	6	MPI 2105EA	1		13	3
86	2014-12-28 03:37:52.961727+01	7	MPI 2000GW	1		13	3
87	2014-12-28 03:38:02.739526+01	8	MPI 2120MW	1		13	3
88	2014-12-28 03:38:13.20388+01	9	MPI 2040GC	1		13	3
89	2014-12-28 03:38:21.629905+01	10	MPI 3000PP	1		13	3
90	2014-12-28 03:38:29.320647+01	11	MPI 3041PP	1		13	3
91	2014-12-28 03:38:39.590666+01	12	SMBG440-/MPI4130/	1		13	3
92	2014-12-28 03:38:48.525932+01	13	SMBM440-/MPI4230/	1		13	3
93	2014-12-28 03:38:56.041039+01	14	SMBM680-/MPI4330/	1		13	3
94	2014-12-28 03:39:03.771736+01	15	SMP-/IP 2405/	1		13	3
95	2014-12-28 03:39:11.41851+01	16	SMP-/IP 2408/	1		13	3
96	2014-12-28 03:39:28.57838+01	17	One Way Vision Auto View	1		13	3
97	2014-12-28 03:39:37.129451+01	18	MPI 2611	1		13	3
98	2014-12-28 03:39:49.673857+01	19	EasyPrint 1702	1		13	3
99	2014-12-28 03:40:14.864808+01	20	15 AUD	1		13	3
100	2014-12-28 03:40:22.341898+01	21	20 AUD	1		13	3
101	2014-12-28 03:40:30.055519+01	22	25 AUD	1		13	3
102	2014-12-28 03:40:37.223463+01	23	30 AUD	1		13	3
103	2014-12-28 03:40:45.278431+01	24	35 AUD	1		13	3
104	2014-12-28 03:40:55.034992+01	25	40 AUD	1		13	3
105	2014-12-28 03:41:01.941451+01	26	45 AUD	1		13	3
106	2014-12-28 03:41:09.489763+01	27	50 AUD	1		13	3
107	2014-12-28 03:46:27.209179+01	1	NO	2	Changed title and value.	14	3
108	2014-12-28 03:46:35.692954+01	2	CWGL152-/DOL 1460/	2	Changed title and value.	14	3
109	2014-12-28 03:46:43.886537+01	3	CWML152-/DOL 1480/	1		14	3
110	2014-12-28 03:46:51.379979+01	4	CWGL137-/DOL 1360/	1		14	3
111	2014-12-28 03:46:59.860753+01	5	CWML137-/DOL 1380/	1		14	3
112	2014-12-28 03:47:09.96523+01	6	SMGL-/DOL2000/	1		14	3
113	2014-12-28 03:47:17.201345+01	7	SMGL-/DOL2100/	1		14	3
114	2014-12-28 03:47:24.910057+01	8	SMGL-/DOL 3000 overlaminate/	1		14	3
115	2014-12-28 03:47:32.031129+01	9	SMML137-/DOL 3100 overlaminate/	1		14	3
116	2014-12-28 03:47:42.209002+01	10	One Way Lam 	1		14	3
117	2014-12-28 03:48:47.607562+01	1	YES	2	Changed title and value.	17	3
118	2014-12-28 03:48:56.666788+01	2	NO	2	Changed title and value.	17	3
119	2014-12-28 03:49:17.237888+01	1	YES	2	Changed title.	18	3
120	2014-12-28 03:49:22.254093+01	2	NO	2	Changed title and value.	18	3
121	2014-12-28 03:50:32.202453+01	1	0	2	Changed title and value.	15	3
122	2014-12-28 03:50:42.063221+01	2	5	2	Changed title, value and index_sort.	15	3
123	2014-12-28 03:50:52.875254+01	3	10	1		15	3
124	2014-12-28 03:51:00.137897+01	4	15	1		15	3
125	2014-12-28 03:51:05.116205+01	5	20	1		15	3
126	2014-12-28 03:51:11.382638+01	6	25	1		15	3
127	2014-12-28 03:51:19.261917+01	7	30	1		15	3
128	2014-12-28 03:51:24.675223+01	8	35	1		15	3
129	2014-12-28 03:51:29.896741+01	9	40	1		15	3
130	2014-12-28 03:51:34.722534+01	10	45	1		15	3
131	2014-12-28 03:51:40.109268+01	11	50	1		15	3
132	2014-12-28 03:51:44.931891+01	12	55	1		15	3
133	2014-12-28 03:51:49.791815+01	13	60	1		15	3
134	2014-12-28 03:51:54.592586+01	14	65	1		15	3
135	2014-12-28 03:51:59.295944+01	15	70	1		15	3
136	2014-12-28 03:52:04.205299+01	16	75	1		15	3
137	2014-12-28 03:52:09.402282+01	17	80	1		15	3
138	2014-12-28 03:54:59.246839+01	1	GROUP 1 	2	Changed title, material and tisk.	16	3
139	2014-12-28 03:55:14.165289+01	2	GROUP 2 	2	Changed title, material and tisk.	16	3
140	2014-12-28 03:55:22.990634+01	3	GROUP 3 	1		16	3
141	2014-12-28 03:55:32.309363+01	4	GROUP 4 	1		16	3
142	2014-12-28 03:55:43.915914+01	5	GROUP 5	1		16	3
143	2014-12-28 03:55:53.083808+01	6	GROUP 6	1		16	3
144	2014-12-28 03:56:03.161619+01	7	GROUP 7	1		16	3
145	2014-12-28 03:56:12.509534+01	8	GROUP 8	1		16	3
146	2014-12-28 10:01:22.479911+01	35	Request object	3		10	2
147	2014-12-28 15:53:19.582679+01	1	GROUP 1 	2	Changed material and tisk.	16	3
148	2014-12-28 15:53:41.082805+01	2	GROUP 2 	2	Changed material and tisk.	16	3
149	2014-12-28 15:53:47.42077+01	3	GROUP 3 	2	Changed material and tisk.	16	3
150	2014-12-28 15:53:57.913275+01	4	GROUP 4 	2	Changed material and tisk.	16	3
151	2014-12-28 15:54:05.272396+01	5	GROUP 5	2	Changed material and tisk.	16	3
152	2014-12-28 15:54:11.465539+01	5	GROUP 5	2	Changed material and tisk.	16	3
153	2014-12-28 15:55:24.103558+01	5	GROUP 5	2	Changed material and tisk.	16	3
154	2014-12-28 15:55:31.458498+01	6	GROUP 6	2	Changed material and tisk.	16	3
155	2014-12-28 15:55:36.964065+01	7	GROUP 7	2	Changed material and tisk.	16	3
156	2014-12-28 15:55:41.416971+01	8	GROUP 8	2	Changed material and tisk.	16	3
157	2014-12-28 15:57:33.53788+01	1	GROUP 1 	2	Changed tisk.	16	3
158	2014-12-28 15:57:39.247956+01	2	GROUP 2 	2	Changed tisk.	16	3
159	2014-12-28 15:58:32.599701+01	3	GROUP 3 	2	Changed tisk.	16	3
160	2014-12-28 15:58:38.463154+01	4	GROUP 4 	2	Changed tisk.	16	3
161	2014-12-28 15:58:46.522506+01	5	GROUP 5	2	Changed tisk.	16	3
162	2014-12-28 15:58:52.3061+01	6	GROUP 6	2	Changed tisk.	16	3
163	2014-12-28 15:58:58.245589+01	7	GROUP 7	2	Changed tisk.	16	3
164	2014-12-28 15:59:03.442018+01	8	GROUP 8	2	Changed tisk.	16	3
165	2014-12-28 18:39:09.646022+01	34	Request object	2	Changed status.	10	3
166	2014-12-28 19:05:17.402255+01	34	Request object	2	Changed status.	10	3
167	2014-12-28 19:06:31.513363+01	34	Request object	2	Changed status.	10	3
168	2014-12-29 09:43:28.284757+01	34	Request object	2	Changed status.	10	3
169	2014-12-29 10:41:07.67642+01	6	max@graphicsforcars.com.au	1		6	3
170	2014-12-29 10:41:12.241117+01	6	max@graphicsforcars.com.au	2	Changed is_admin.	6	3
171	2015-01-06 10:16:32.157679+01	33	Request object	2	Added attachment "Attachment object".	10	2
172	2015-01-06 10:39:18.476021+01	33	Request object	2	Changed price.	10	2
173	2015-01-06 10:40:08.780176+01	33	Request object	2	Changed order_details.	10	2
174	2015-01-06 12:31:53.177444+01	33	Request object	2	Changed status.	10	2
175	2015-01-06 15:00:48.62206+01	33	Request object	2	Changed status.	10	2
176	2015-01-09 09:34:04.63553+01	33	Request object	2	Changed status.	10	2
177	2015-01-09 09:34:06.404827+01	33	Request object	2	No fields changed.	10	2
178	2015-01-09 09:46:40.22159+01	7	reva@revasik.com	1		6	2
179	2015-01-09 09:46:45.662769+01	7	reva@revasik.com	2	No fields changed.	6	2
180	2015-01-09 09:50:45.307903+01	33	Request object	2	Changed status.	10	2
181	2015-01-09 09:52:01.374461+01	33	Request object	2	Changed status.	10	2
182	2015-01-09 09:59:57.162974+01	33	Request object	2	Changed status.	10	2
183	2015-01-09 10:07:37.575422+01	33	Request object	2	Changed status.	10	2
184	2015-01-09 12:32:55.612037+01	1	Company 1	2	Changed email, phone, city, website, address, state, post_code and description.	8	2
185	2015-01-09 13:40:55.361252+01	38	Request object	2	Changed calculation_json.	10	2
186	2015-01-09 14:17:47.977606+01	33	Request object	2	No fields changed.	10	2
187	2015-01-09 14:22:25.108032+01	33	Request object	2	Changed calculation_json.	10	2
188	2015-01-09 14:23:12.512778+01	33	Request object	2	Changed calculation_json.	10	2
189	2015-01-09 15:12:27.591675+01	1	group	1		3	2
190	2015-01-09 15:28:21.969112+01	1	Sales manager	2	Changed name.	3	2
191	2015-01-09 15:28:35.861918+01	2	Admin	1		3	2
192	2015-01-09 15:29:01.02742+01	3	Office manager	1		3	2
193	2015-01-09 15:29:17.226135+01	4	Graphics manager	1		3	2
194	2015-01-09 15:29:34.179904+01	5	Production manager	1		3	2
195	2015-01-09 15:29:56.24942+01	6	Installation manager	1		3	2
196	2015-01-09 15:48:30.219777+01	2	revastanislav@gmail.com	2	Changed group.	6	2
197	2015-01-09 16:23:40.99881+01	2	revastanislav@gmail.com	2	Changed group.	6	2
198	2015-01-09 16:28:41.954502+01	2	revastanislav@gmail.com	2	Changed group.	6	2
199	2015-01-09 16:51:58.243895+01	8	test@test.ru	1		6	2
200	2015-01-09 16:52:23.583387+01	8	test@test.ru	2	Changed name, phone and group.	6	2
201	2015-01-09 20:11:55.477482+01	5	gin.rad@gmail.com	2	Changed group.	6	2
202	2015-01-10 10:41:24.10569+01	6	max@graphicsforcars.com.au	2	Changed group.	6	2
203	2015-01-10 11:56:00.988026+01	33	Request object	2	Changed graphics_date_start.	10	2
204	2015-01-10 11:58:24.847062+01	33	Request object	2	Changed status.	10	2
205	2015-01-10 12:05:53.585282+01	33	Request object	2	Changed graphics_date_start, graphics_date_end, installation_date_start, installation_date_end, product_date_start and product_date_end.	10	2
206	2015-01-10 12:11:37.086058+01	36	Request object	2	Changed product_date_start and status.	10	2
207	2015-01-10 14:06:43.982624+01	33	Request object	2	Changed installation_date_end.	10	2
208	2015-01-10 14:12:43.814598+01	36	Request object	2	Changed graphics_date_start, graphics_date_end, installation_date_start, installation_date_end, product_date_start and product_date_end.	10	2
209	2015-01-10 14:45:25.542138+01	33	Request object	2	Changed status.	10	2
210	2015-01-10 14:46:10.316642+01	33	Request object	2	Changed status.	10	2
211	2015-01-10 14:46:23.249737+01	36	Request object	2	Changed status.	10	2
212	2015-01-10 16:18:29.983461+01	33	Request object	2	Changed product_date_start and product_date_end.	10	2
213	2015-01-10 16:22:07.565233+01	33	Request object	2	Changed product_date_start and product_date_end.	10	2
214	2015-01-10 16:24:59.232562+01	33	Request object	2	Changed product_date_start and product_date_end.	10	2
215	2015-01-10 16:56:55.420879+01	34	Request object	2	Changed graphics_date_start, graphics_date_end, installation_date_start, installation_date_end, product_date_start, product_date_end and status.	10	2
216	2015-01-10 16:58:17.035813+01	34	Request object	2	Changed installation_date_start.	10	2
217	2015-01-10 17:23:44.063132+01	3	ceo@4-com.pro	2	Changed group.	6	2
218	2015-01-11 08:32:07.696946+01	11	provn	3		8	6
219	2015-01-11 08:32:07.716003+01	10		3		8	6
220	2015-01-11 08:32:07.718214+01	9		3		8	6
221	2015-01-11 08:32:07.720391+01	8		3		8	6
222	2015-01-11 08:32:07.722539+01	7		3		8	6
223	2015-01-11 08:32:07.724381+01	6	xcvbcvb54654677777777777777	3		8	6
224	2015-01-11 08:32:07.725607+01	5		3		8	6
225	2015-01-11 08:32:07.727114+01	3	Company 3	3		8	6
226	2015-01-11 08:32:07.72925+01	2	Company 2	3		8	6
227	2015-01-11 08:32:07.731486+01	1	Company 1	3		8	6
228	2015-01-11 11:50:26.790807+01	43	Request object	2	Changed graphics_date_start, graphics_date_end, installation_date_start, installation_date_end, product_date_start, product_date_end and status.	10	2
229	2015-01-11 12:02:47.482047+01	43	Request object	2	Changed graphics_date_start.	10	2
230	2015-01-11 12:03:46.786047+01	43	Request object	2	Changed graphics_date_start.	10	2
231	2015-01-11 12:32:33.952601+01	43	Request object	2	No fields changed.	10	2
232	2015-01-11 12:43:20.521496+01	43	Request object	2	No fields changed.	10	2
233	2015-01-11 13:01:11.806554+01	43	Request object	2	Changed status.	10	2
234	2015-01-11 13:09:40.406015+01	43	Request object	2	Changed status.	10	2
235	2015-01-11 13:11:23.942513+01	43	Request object	2	Changed status.	10	2
236	2015-01-11 13:48:46.803168+01	43	Request object	2	Changed status.	10	2
237	2015-01-12 02:26:09.291517+01	9	danny@graphicsforcars.com.au	1		6	6
238	2015-01-12 02:27:20.383167+01	9	danny@graphicsforcars.com.au	2	Changed name, phone and group.	6	6
239	2015-01-12 08:56:06.168016+01	43	Request object	2	Changed graphics_date_start and graphics_date_end.	10	2
240	2015-01-12 08:57:31.67956+01	43	Request object	2	Changed status.	10	2
241	2015-01-12 11:45:55.686557+01	43	Request object	2	Changed calculation_json. Added attachment "Attachment object".	10	2
242	2015-01-12 14:57:03.797797+01	44	Request object	2	Changed status.	10	2
243	2015-01-12 14:57:51.674557+01	43	Request object	2	Changed status.	10	2
244	2015-01-12 15:02:38.620724+01	43	Request object	2	Changed status.	10	2
245	2015-01-12 15:09:26.495874+01	43	Request object	2	Changed status.	10	2
246	2015-01-12 15:12:57.306047+01	43	Request object	2	Changed graphics_date_start and graphics_date_end.	10	2
247	2015-01-12 15:57:24.7871+01	44	Request object	2	Changed graphics_date_start, graphics_date_end, installation_date_start, installation_date_end, product_date_start and product_date_end.	10	2
248	2015-01-12 15:57:33.504874+01	46	Request object	3		10	2
249	2015-01-12 15:57:33.657702+01	45	Request object	3		10	2
250	2015-01-12 15:58:00.06931+01	43	Request object	2	Changed graphics_date_start and graphics_date_end.	10	2
251	2015-01-13 09:43:21.45261+01	1	SettingsContent object	1		19	2
252	2015-01-13 09:45:36.665296+01	2	SettingsContent object	1		19	2
253	2015-01-13 09:45:54.316122+01	3	SettingsContent object	1		19	2
254	2015-01-13 11:08:14.075885+01	43	Request object	2	Changed sending_date.	10	2
255	2015-01-14 09:19:33.209772+01	2	General manager	2	Changed name.	3	2
256	2015-01-14 09:22:18.312451+01	2	revastanislav@gmail.com	2	No fields changed.	6	2
257	2015-01-14 09:23:35.150533+01	7	Admin	1		3	2
258	2015-01-14 09:28:44.703525+01	2	revastanislav@gmail.com	2	Changed group.	6	2
259	2015-01-14 09:38:21.624508+01	17	cust	2	Changed website.	8	2
260	2015-01-14 09:39:00.272414+01	18	Torvac 	2	Changed website.	8	2
261	2015-01-14 09:39:46.854594+01	17	cust	2	No fields changed.	8	2
262	2015-01-14 09:40:02.117864+01	17	cust	2	Changed website.	8	2
263	2015-01-14 09:40:36.238157+01	12	aasdasd	2	Changed website.	8	2
264	2015-01-14 10:33:02.951331+01	10	funky.gin@yandex.ru	1		6	2
265	2015-01-14 10:33:38.324339+01	10	funky.gin@yandex.ru	2	Changed name, phone and group.	6	2
266	2015-01-14 10:34:11.527667+01	10	funky.gin@yandex.ru	2	Changed is_admin.	6	2
267	2015-01-14 10:35:51.030593+01	19	asdf	2	Changed city.	8	10
268	2015-01-14 10:36:20.407361+01	10	funky.gin@yandex.ru	2	Changed is_admin.	6	10
269	2015-01-14 12:30:08.006948+01	43	Request object	2	Changed status.	10	2
270	2015-01-14 12:30:41.257136+01	43	Request object	2	Changed status.	10	2
271	2015-01-14 18:40:55.327199+01	101	Request object	2	Changed status.	10	2
272	2015-01-14 20:05:45.35289+01	109	Request object	2	Changed status.	10	2
273	2015-01-14 20:26:28.109986+01	110	Request object	3		10	2
274	2015-01-14 20:26:28.11246+01	109	Request object	3		10	2
275	2015-01-14 20:26:28.114578+01	108	Request object	3		10	2
276	2015-01-14 20:26:28.116671+01	107	Request object	3		10	2
277	2015-01-14 20:26:28.118862+01	106	Request object	3		10	2
278	2015-01-14 20:26:28.121099+01	105	Request object	3		10	2
279	2015-01-14 20:26:28.123207+01	104	Request object	3		10	2
280	2015-01-14 20:26:28.125391+01	103	Request object	3		10	2
281	2015-01-14 20:26:28.127844+01	102	Request object	3		10	2
282	2015-01-14 20:26:28.12993+01	101	Request object	3		10	2
283	2015-01-14 20:26:28.132061+01	100	Request object	3		10	2
284	2015-01-14 20:26:28.134219+01	99	Request object	3		10	2
285	2015-01-14 20:26:28.136181+01	98	Request object	3		10	2
286	2015-01-14 20:26:28.138525+01	97	Request object	3		10	2
287	2015-01-14 20:26:28.140504+01	96	Request object	3		10	2
288	2015-01-14 20:26:28.1425+01	95	Request object	3		10	2
289	2015-01-14 20:26:28.144425+01	94	Request object	3		10	2
290	2015-01-14 20:26:28.146429+01	93	Request object	3		10	2
291	2015-01-14 20:26:28.148585+01	92	Request object	3		10	2
292	2015-01-14 20:26:28.150824+01	91	Request object	3		10	2
293	2015-01-14 20:26:28.152963+01	90	Request object	3		10	2
294	2015-01-14 20:26:28.154992+01	89	Request object	3		10	2
295	2015-01-14 20:26:28.157203+01	88	Request object	3		10	2
296	2015-01-14 20:26:28.159269+01	87	Request object	3		10	2
297	2015-01-14 20:26:28.161558+01	86	Request object	3		10	2
298	2015-01-14 20:26:28.163791+01	85	Request object	3		10	2
299	2015-01-14 20:26:28.165893+01	84	Request object	3		10	2
300	2015-01-14 20:26:28.168202+01	83	Request object	3		10	2
301	2015-01-14 20:26:28.170291+01	82	Request object	3		10	2
302	2015-01-14 20:26:28.172342+01	81	Request object	3		10	2
303	2015-01-14 20:26:28.174287+01	80	Request object	3		10	2
304	2015-01-14 20:26:28.176289+01	79	Request object	3		10	2
305	2015-01-14 20:26:28.178232+01	78	Request object	3		10	2
306	2015-01-14 20:26:28.180401+01	77	Request object	3		10	2
307	2015-01-14 20:26:28.182496+01	76	Request object	3		10	2
308	2015-01-14 20:26:28.184591+01	75	Request object	3		10	2
309	2015-01-14 20:26:28.186647+01	74	Request object	3		10	2
310	2015-01-14 20:26:28.189182+01	73	Request object	3		10	2
311	2015-01-14 20:26:28.191349+01	72	Request object	3		10	2
312	2015-01-14 20:26:28.193449+01	71	Request object	3		10	2
313	2015-01-14 20:26:28.195514+01	70	Request object	3		10	2
314	2015-01-14 20:26:28.197672+01	69	Request object	3		10	2
315	2015-01-14 20:26:28.199794+01	68	Request object	3		10	2
316	2015-01-14 20:26:28.201948+01	67	Request object	3		10	2
317	2015-01-14 20:26:28.204033+01	66	Request object	3		10	2
318	2015-01-14 20:26:28.20614+01	65	Request object	3		10	2
319	2015-01-14 20:26:28.208257+01	64	Request object	3		10	2
320	2015-01-14 20:26:28.209354+01	63	Request object	3		10	2
321	2015-01-14 20:26:28.21027+01	62	Request object	3		10	2
322	2015-01-14 20:26:28.211328+01	61	Request object	3		10	2
323	2015-01-14 20:26:28.212247+01	60	Request object	3		10	2
324	2015-01-14 20:26:28.213298+01	59	Request object	3		10	2
325	2015-01-14 20:26:28.214287+01	58	Request object	3		10	2
326	2015-01-14 20:26:28.215367+01	57	Request object	3		10	2
327	2015-01-14 20:26:28.216421+01	56	Request object	3		10	2
328	2015-01-14 20:26:28.21752+01	55	Request object	3		10	2
329	2015-01-14 20:26:28.218635+01	54	Request object	3		10	2
330	2015-01-14 20:26:28.219767+01	53	Request object	3		10	2
331	2015-01-14 20:26:28.220805+01	52	Request object	3		10	2
332	2015-01-14 20:26:28.221768+01	51	Request object	3		10	2
333	2015-01-14 20:26:28.222676+01	50	Request object	3		10	2
334	2015-01-14 20:26:28.223575+01	49	Request object	3		10	2
335	2015-01-14 20:26:28.22452+01	48	Request object	3		10	2
336	2015-01-14 20:26:28.225494+01	47	Request object	3		10	2
337	2015-01-14 20:26:28.226473+01	44	Request object	3		10	2
338	2015-01-14 20:26:28.227416+01	43	Request object	3		10	2
339	2015-01-14 20:31:15.484224+01	111	Request object	1		10	2
340	2015-01-14 20:42:39.559242+01	112	Request object	2	No fields changed.	10	2
341	2015-01-14 20:42:51.823497+01	111	Request object	3		10	2
342	2015-01-14 20:43:09.347185+01	112	Request object	2	Changed status.	10	2
343	2015-01-14 20:48:50.805349+01	113	Request object	3		10	2
344	2015-01-14 20:48:50.807807+01	112	Request object	3		10	2
345	2015-01-14 20:51:19.026828+01	114	Request object	2	Changed status.	10	2
346	2015-01-14 21:05:29.174427+01	114	Request object	2	Changed status.	10	2
347	2015-01-14 21:54:48.000118+01	114	Request object	2	Changed status.	10	2
348	2015-01-14 22:13:25.879223+01	21	you	2	Changed website.	8	2
349	2015-01-14 22:56:14.003548+01	9	danny@graphicsforcars.com.au	2	Changed group.	6	6
350	2015-01-15 09:43:46.66716+01	115	Request object	2	Changed status.	10	2
351	2015-01-15 10:18:24.578163+01	115	Request object	2	Changed graphics_date_start.	10	2
352	2015-01-15 10:59:40.893577+01	10	funky.gin@yandex.ru	2	Changed group.	6	2
353	2015-01-15 11:32:00.061934+01	6	max@graphicsforcars.com.au	2	Changed group.	6	2
354	2015-01-15 12:10:13.15059+01	5	gin.rad@gmail.com	2	Changed name, phone and group.	6	2
355	2015-01-15 13:13:31.115115+01	127	Request object	2	Changed sent_invoice.	10	2
356	2015-01-15 13:14:44.330607+01	127	Request object	2	Changed sent_invoice.	10	2
357	2015-01-15 13:16:42.588475+01	127	Request object	2	Changed sent_invoice and status.	10	2
358	2015-01-15 13:20:26.5444+01	127	Request object	2	Changed status.	10	2
359	2015-01-15 18:29:56.733363+01	135	Request object	2	Changed graphics_date_start.	10	2
360	2015-01-15 19:05:53.403416+01	135	Request object	2	Changed status.	10	2
361	2015-01-15 23:02:41.102907+01	9	danny@graphicsforcars.com.au	3		6	6
362	2015-01-15 23:03:49.738732+01	11	danny@graphicsforcars.com.au	1		6	6
363	2015-01-15 23:04:28.767595+01	11	danny@graphicsforcars.com.au	2	Changed name, phone and group.	6	6
364	2015-01-16 13:00:32.645252+01	140	Request object	2	Changed sent_invoice.	10	2
365	2015-01-18 10:45:04.821367+01	136	Request object	2	Changed product_date_end.	10	2
366	2015-01-18 10:46:06.50517+01	136	Request object	2	Changed product_date_start and product_date_end.	10	2
367	2015-01-18 19:13:26.475448+01	137	Request object	2	Changed installation_date_end.	10	2
368	2015-01-18 19:21:59.188571+01	136	Request object	2	Changed product_date_start.	10	2
369	2015-01-18 19:22:46.572506+01	136	Request object	2	Changed product_min, product_date_start and product_date_end.	10	2
370	2015-01-18 19:45:49.134203+01	148	Request object	2	Changed installation_date_start and installation_date_end.	10	2
371	2015-01-18 19:59:44.032167+01	137	Request object	2	No fields changed.	10	2
372	2015-01-18 20:00:27.630063+01	137	Request object	2	Changed product_date_start.	10	2
373	2015-01-18 20:03:53.10664+01	136	Request object	2	No fields changed.	10	2
374	2015-01-18 20:04:59.76393+01	137	Request object	2	Changed product_date_start.	10	2
375	2015-01-18 20:38:28.761158+01	143	Request object	3		10	2
376	2015-01-18 22:53:11.75054+01	150	Request object	3		10	6
377	2015-01-18 22:53:11.754416+01	149	Request object	3		10	6
378	2015-01-18 22:53:11.756749+01	148	Request object	3		10	6
379	2015-01-18 22:53:11.759081+01	147	Request object	3		10	6
380	2015-01-18 22:53:11.761362+01	146	Request object	3		10	6
381	2015-01-18 22:53:11.763643+01	145	Request object	3		10	6
382	2015-01-18 22:53:11.765646+01	144	Request object	3		10	6
383	2015-01-18 22:53:11.767928+01	142	Request object	3		10	6
384	2015-01-18 22:53:11.770008+01	141	Request object	3		10	6
385	2015-01-18 22:53:11.771984+01	140	Request object	3		10	6
386	2015-01-18 22:53:11.773999+01	139	Request object	3		10	6
387	2015-01-18 22:53:11.775952+01	138	Request object	3		10	6
388	2015-01-18 22:53:11.77798+01	137	Request object	3		10	6
389	2015-01-18 22:53:11.779932+01	136	Request object	3		10	6
390	2015-01-18 22:53:11.781921+01	135	Request object	3		10	6
391	2015-01-18 22:53:11.783124+01	134	Request object	3		10	6
392	2015-01-18 22:53:11.785144+01	133	Request object	3		10	6
393	2015-01-18 22:53:11.787096+01	132	Request object	3		10	6
394	2015-01-18 22:53:11.789179+01	131	Request object	3		10	6
395	2015-01-18 22:53:11.791132+01	130	Request object	3		10	6
396	2015-01-18 22:53:11.79325+01	129	Request object	3		10	6
397	2015-01-18 22:53:11.795488+01	128	Request object	3		10	6
398	2015-01-18 22:53:11.797702+01	127	Request object	3		10	6
399	2015-01-18 22:53:11.799661+01	124	Request object	3		10	6
400	2015-01-18 22:53:11.801799+01	123	Request object	3		10	6
401	2015-01-18 22:53:11.803696+01	122	Request object	3		10	6
402	2015-01-18 22:53:11.805803+01	121	Request object	3		10	6
403	2015-01-18 22:53:11.8077+01	120	Request object	3		10	6
404	2015-01-18 22:53:11.809666+01	119	Request object	3		10	6
405	2015-01-18 22:53:11.811566+01	118	Request object	3		10	6
406	2015-01-18 22:53:11.813477+01	117	Request object	3		10	6
407	2015-01-18 22:53:11.815412+01	116	Request object	3		10	6
408	2015-01-18 22:53:11.81691+01	115	Request object	3		10	6
409	2015-01-18 22:53:11.817857+01	114	Request object	3		10	6
410	2015-01-18 22:53:31.492473+01	22	Production	3		7	6
411	2015-01-18 22:53:31.494407+01	21	trrrrrr	3		7	6
412	2015-01-18 22:53:31.495681+01	20	Design	3		7	6
413	2015-01-18 22:53:31.497186+01	19	sh	3		7	6
414	2015-01-18 22:53:31.499323+01	18	max	3		7	6
415	2015-01-18 22:53:31.501596+01	17	T	3		7	6
416	2015-01-18 22:53:31.503714+01	16	sasha custom	3		7	6
417	2015-01-18 22:53:31.50567+01	15	sasha	3		7	6
418	2015-01-18 22:53:31.506858+01	14	asdasdasd	3		7	6
419	2015-01-18 22:53:31.507943+01	13	T	3		7	6
420	2015-01-18 22:53:31.509113+01	12	123123123123123123	3		7	6
421	2015-01-18 22:53:31.51015+01	11	123123123	3		7	6
422	2015-01-18 22:53:31.51112+01	10	123	3		7	6
423	2015-01-18 22:53:31.51215+01	9	22222	3		7	6
424	2015-01-18 22:53:31.5131+01	8	qweqweqwe	3		7	6
425	2015-01-18 22:53:31.514027+01	7	asdasdasd	3		7	6
426	2015-01-18 22:53:31.514944+01	6	ca	3		7	6
427	2015-01-18 22:53:31.515886+01	5	you	3		7	6
428	2015-01-18 22:53:31.517431+01	4	c	3		7	6
429	2015-01-18 22:53:31.519688+01	3	category3	3		7	6
430	2015-01-18 22:53:31.521443+01	2	Category 1	3		7	6
431	2015-01-18 22:53:31.523198+01	1	Category 2	3		7	6
432	2015-01-18 23:27:40.315375+01	12	paul@graphicsforcars.com.au	1		6	6
433	2015-01-18 23:28:28.359299+01	12	paul@graphicsforcars.com.au	2	Changed name, phone and group.	6	6
434	2015-01-19 00:07:58.174973+01	13	Meegan@graphicsforcars.com.au	1		6	6
435	2015-01-19 00:13:33.760354+01	13	Meegan@graphicsforcars.com.au	2	Changed name, phone and group.	6	6
436	2015-01-19 09:27:10.307852+01	152	Request object	3		10	2
437	2015-01-19 22:59:46.629525+01	38	test34	3		8	6
438	2015-01-19 22:59:46.631564+01	37	stgfsdfsdfdsf	3		8	6
439	2015-01-19 22:59:46.632638+01	36	test	3		8	6
440	2015-01-19 23:04:22.866543+01	154	Request object	3		10	6
441	2015-01-19 23:04:22.868479+01	153	Request object	3		10	6
442	2015-01-19 23:04:22.869747+01	151	Request object	3		10	6
443	2015-01-19 23:05:05.921321+01	51	Organizer object	3		12	6
444	2015-01-19 23:05:05.923263+01	50	Organizer object	3		12	6
445	2015-01-19 23:05:05.924537+01	49	Organizer object	3		12	6
446	2015-01-19 23:05:05.925564+01	48	Organizer object	3		12	6
447	2015-01-19 23:05:05.926498+01	47	Organizer object	3		12	6
448	2015-01-19 23:05:05.927431+01	46	Organizer object	3		12	6
449	2015-01-19 23:05:05.92836+01	45	Organizer object	3		12	6
450	2015-01-19 23:05:05.929343+01	44	Organizer object	3		12	6
451	2015-01-19 23:05:05.930395+01	43	Organizer object	3		12	6
452	2015-01-19 23:05:05.931444+01	42	Organizer object	3		12	6
453	2015-01-19 23:05:05.932437+01	41	Organizer object	3		12	6
454	2015-01-19 23:05:05.933343+01	40	Organizer object	3		12	6
455	2015-01-19 23:05:05.934307+01	39	Organizer object	3		12	6
456	2015-01-19 23:05:05.935247+01	38	Organizer object	3		12	6
457	2015-01-19 23:05:05.936496+01	37	Organizer object	3		12	6
458	2015-01-19 23:05:05.937415+01	36	Organizer object	3		12	6
459	2015-01-19 23:05:05.938294+01	35	Organizer object	3		12	6
460	2015-01-19 23:05:05.939156+01	34	Organizer object	3		12	6
461	2015-01-19 23:05:05.940036+01	33	Organizer object	3		12	6
462	2015-01-19 23:05:05.94095+01	32	Organizer object	3		12	6
463	2015-01-19 23:05:05.941887+01	31	Organizer object	3		12	6
464	2015-01-19 23:05:05.942753+01	30	Organizer object	3		12	6
465	2015-01-19 23:05:05.943667+01	29	Organizer object	3		12	6
466	2015-01-19 23:05:05.944704+01	28	Organizer object	3		12	6
467	2015-01-19 23:05:05.945647+01	27	Organizer object	3		12	6
468	2015-01-19 23:05:05.946642+01	26	Organizer object	3		12	6
469	2015-01-19 23:05:05.947685+01	25	Organizer object	3		12	6
470	2015-01-19 23:05:05.948709+01	24	Organizer object	3		12	6
471	2015-01-19 23:05:05.949618+01	23	Organizer object	3		12	6
472	2015-01-19 23:05:05.950595+01	22	Organizer object	3		12	6
473	2015-01-19 23:05:05.951692+01	21	Organizer object	3		12	6
474	2015-01-19 23:05:05.952809+01	20	Organizer object	3		12	6
475	2015-01-19 23:05:05.953822+01	19	Organizer object	3		12	6
476	2015-01-19 23:05:05.954791+01	18	Organizer object	3		12	6
477	2015-01-19 23:05:05.955731+01	17	Organizer object	3		12	6
478	2015-01-19 23:05:05.956744+01	16	Organizer object	3		12	6
479	2015-01-19 23:05:05.957768+01	15	Organizer object	3		12	6
480	2015-01-19 23:05:05.958729+01	14	Organizer object	3		12	6
481	2015-01-19 23:05:05.959777+01	13	Organizer object	3		12	6
482	2015-01-19 23:05:05.960866+01	12	Organizer object	3		12	6
483	2015-01-19 23:05:05.961881+01	11	Organizer object	3		12	6
484	2015-01-19 23:05:05.962868+01	10	Organizer object	3		12	6
485	2015-01-19 23:05:05.96387+01	9	Organizer object	3		12	6
486	2015-01-19 23:05:05.964929+01	8	Organizer object	3		12	6
487	2015-01-19 23:05:05.966007+01	7	Organizer object	3		12	6
488	2015-01-19 23:05:05.96702+01	6	Organizer object	3		12	6
489	2015-01-19 23:05:05.967992+01	5	Organizer object	3		12	6
490	2015-01-19 23:05:05.969001+01	4	Organizer object	3		12	6
491	2015-01-19 23:05:05.970039+01	3	Organizer object	3		12	6
492	2015-01-19 23:06:07.980373+01	12	paul@graphicsforcars.com.au	2	No fields changed.	6	6
493	2015-01-19 23:32:20.095578+01	12	paul@graphicsforcars.com.au	2	Changed group.	6	6
494	2015-01-19 23:37:15.95486+01	12	paul@graphicsforcars.com.au	2	Changed group.	6	6
495	2015-01-19 23:41:24.108211+01	12	paul@graphicsforcars.com.au	2	Changed group.	6	6
496	2015-01-19 23:48:56.996324+01	12	paul@graphicsforcars.com.au	2	Changed is_admin.	6	6
497	2015-01-19 23:57:27.691757+01	12	paul@graphicsforcars.com.au	2	Changed group and is_admin.	6	6
498	2015-01-20 00:02:27.582913+01	2	CWGL152-/DOL 1460/	2	Changed value.	14	6
499	2015-01-20 00:04:36.88059+01	2	CWGL152-/DOL 1460/	2	Changed value.	14	6
500	2015-01-20 00:05:04.825193+01	3	CWML152-/DOL 1480/	2	Changed value.	14	6
501	2015-01-20 00:05:18.369799+01	4	CWGL137-/DOL 1360/	2	Changed value.	14	6
502	2015-01-20 00:05:34.900851+01	5	CWML137-/DOL 1380/	2	Changed value.	14	6
503	2015-01-20 00:05:55.563849+01	5	CWF1-\\MPI 1005SC\\	2	No fields changed.	13	6
504	2015-01-20 00:08:53.823364+01	2	CWGL152-/DOL 1460/	2	No fields changed.	14	6
505	2015-01-20 00:11:28.216216+01	156	Request object	3		10	6
506	2015-01-20 00:11:28.218065+01	155	Request object	3		10	6
507	2015-01-20 00:14:02.864527+01	3	SettingsContent object	2	Changed text.	19	6
508	2015-01-20 00:14:13.339472+01	2	SettingsContent object	2	Changed text.	19	6
509	2015-01-20 00:14:26.189291+01	1	SettingsContent object	2	Changed text.	19	6
510	2015-01-20 00:14:33.814315+01	1	SettingsContent object	2	Changed text.	19	6
511	2015-01-20 00:16:45.450326+01	1	GROUP 1 	2	No fields changed.	16	6
512	2015-01-20 00:16:51.462767+01	7	GROUP 7	2	No fields changed.	16	6
513	2015-01-20 00:17:03.033131+01	2	GROUP 2 	2	No fields changed.	16	6
514	2015-01-20 00:17:11.150834+01	3	GROUP 3 	2	No fields changed.	16	6
515	2015-01-20 00:17:18.119289+01	4	GROUP 4 	2	No fields changed.	16	6
516	2015-01-20 00:17:24.848142+01	5	GROUP 5	2	No fields changed.	16	6
517	2015-01-20 00:17:31.047812+01	6	GROUP 6	2	No fields changed.	16	6
518	2015-01-20 00:17:35.991972+01	7	GROUP 7	2	No fields changed.	16	6
519	2015-01-20 00:17:44.300899+01	8	GROUP 8	2	No fields changed.	16	6
520	2015-01-20 00:18:10.78098+01	1	YES	2	No fields changed.	17	6
521	2015-01-20 13:35:41.339873+01	157	Request object	2	Changed status.	10	2
522	2015-01-20 13:37:12.452618+01	157	Request object	2	Changed status.	10	2
523	2015-01-20 13:39:09.27756+01	157	Request object	2	Changed status.	10	2
524	2015-01-20 15:10:39.616022+01	157	Request object	2	Changed status.	10	2
525	2015-01-20 15:12:03.682707+01	157	Request object	2	Changed status.	10	2
526	2015-01-20 16:00:17.651418+01	157	Request object	2	Changed product_date_start and product_date_end.	10	2
527	2015-01-20 16:02:16.259985+01	157	Request object	2	Changed installation_date_start and installation_date_end.	10	2
528	2015-01-20 16:03:04.611844+01	170	Request object	2	Changed graphics_date_start.	10	2
529	2015-01-20 16:11:47.067069+01	159	Request object	2	Changed product_date_start and product_date_end.	10	2
530	2015-01-20 16:12:39.811412+01	159	Request object	2	Changed graphics_date_start and graphics_date_end.	10	2
531	2015-01-20 16:13:57.593956+01	159	Request object	2	No fields changed.	10	2
532	2015-01-20 16:15:06.551316+01	171	Request object	2	Changed graphics_date_start, graphics_date_end, installation_date_start, installation_date_end, product_date_start and product_date_end.	10	2
533	2015-01-21 09:51:25.024943+01	56	stas revas	3		8	2
534	2015-01-22 05:19:36.885288+01	48	567tyyty	3		8	6
535	2015-01-22 05:19:36.887377+01	47	123test	3		8	6
536	2015-01-22 05:19:59.937952+01	35	XXX	3		7	6
537	2015-01-22 05:19:59.939946+01	33	f	3		7	6
538	2015-01-22 05:19:59.941283+01	32	fuck	3		7	6
539	2015-01-22 05:19:59.942516+01	25	Tra	3		7	6
540	2015-01-22 05:20:08.79483+01	34	Car dealers	2	Changed title.	7	6
541	2015-01-22 05:21:18.790724+01	38		3		9	6
542	2015-01-22 05:21:18.792661+01	37	123432	3		9	6
543	2015-01-22 05:21:18.793923+01	32	Danny Vinkl	3		9	6
544	2015-01-22 05:21:18.79494+01	28		3		9	6
545	2015-01-22 05:21:58.46+01	177	Request object	3		10	6
546	2015-01-22 05:21:58.462147+01	176	Request object	3		10	6
547	2015-01-22 05:21:58.463191+01	175	Request object	3		10	6
548	2015-01-22 05:21:58.464648+01	171	Request object	3		10	6
549	2015-01-22 05:21:58.466855+01	168	Request object	3		10	6
550	2015-01-22 05:21:58.467969+01	165	Request object	3		10	6
551	2015-01-22 05:21:58.469027+01	164	Request object	3		10	6
552	2015-01-22 05:21:58.469993+01	162	Request object	3		10	6
553	2015-01-22 05:21:58.470928+01	161	Request object	3		10	6
554	2015-01-22 05:21:58.472101+01	160	Request object	3		10	6
555	2015-01-22 05:21:58.473247+01	159	Request object	3		10	6
556	2015-01-22 05:55:29.256331+01	179	Request object	2	Changed order_details.	10	6
557	2015-01-23 00:37:24.886377+01	5	CWF1-\\MPI 1005SC\\	2	No fields changed.	13	6
558	2015-01-23 00:45:59.602241+01	11	danny@graphicsforcars.com.au	2	Changed group.	6	6
559	2015-01-23 00:46:08.059931+01	13	Meegan@graphicsforcars.com.au	2	No fields changed.	6	6
560	2015-01-23 00:52:04.839596+01	32	Torvac	2	Changed email.	8	6
561	2015-01-23 00:53:56.953022+01	180	Request object	2	No fields changed.	10	6
562	2015-01-23 02:39:44.417462+01	11	danny@graphicsforcars.com.au	2	Changed is_admin.	6	6
563	2015-01-23 02:45:16.97371+01	180	Request object	2	No fields changed.	10	6
564	2015-01-23 02:48:19.614195+01	11	danny@graphicsforcars.com.au	2	Changed group.	6	6
565	2015-01-23 02:50:14.722608+01	12	paul@graphicsforcars.com.au	2	Changed group.	6	6
566	2015-01-23 03:32:26.405418+01	12	paul@graphicsforcars.com.au	2	Changed is_admin.	6	6
567	2015-01-23 03:37:33.27691+01	43	Sign	3		7	6
568	2015-01-23 03:37:33.279404+01	40	Vehicle Signage	3		7	6
569	2015-01-23 03:37:33.281626+01	34	Car dealers	3		7	6
570	2015-01-23 03:37:33.283906+01	30	Vehicle Signage	3		7	6
571	2015-01-23 03:37:33.286144+01	29	Vehicles hire	3		7	6
572	2015-01-23 13:38:26.596858+01	6	max@graphicsforcars.com.au	2	Changed name.	6	2
573	2015-01-26 23:52:52.009459+01	14	lukas@graphicsforcars.com.au	1		6	6
574	2015-01-26 23:53:41.804293+01	14	lukas@graphicsforcars.com.au	2	Changed name, phone and group.	6	6
575	2015-01-26 23:55:05.475377+01	15	george@graphicsforcars.com.au	1		6	6
576	2015-01-26 23:56:34.882907+01	15	george@graphicsforcars.com.au	2	Changed name, phone and group.	6	6
577	2015-01-27 00:03:28.845722+01	15	george@graphicsforcars.com.au	2	No fields changed.	6	6
578	2015-01-27 01:15:36.977477+01	73	111111111	3		8	6
579	2015-01-27 01:15:36.979322+01	72	3434	3		8	6
580	2015-01-27 01:16:20.948898+01	27	Troy	2	Changed email.	9	6
581	2015-01-27 01:18:36.765342+01	58		3		9	6
582	2015-01-27 01:18:36.767492+01	56	nick 	3		9	6
583	2015-01-27 01:18:36.770134+01	51	Nick	3		9	6
584	2015-01-27 01:18:36.773164+01	49		3		9	6
585	2015-01-27 01:18:36.775967+01	48		3		9	6
586	2015-01-27 01:18:36.777441+01	47		3		9	6
587	2015-01-27 01:18:36.778863+01	45		3		9	6
588	2015-01-27 01:18:36.780268+01	44		3		9	6
589	2015-01-27 01:18:36.781962+01	43		3		9	6
590	2015-01-27 01:18:36.783565+01	40		3		9	6
591	2015-01-27 02:05:35.809173+01	53	The Natural Boutique	3		7	6
592	2015-01-27 02:05:35.811259+01	51	d	3		7	6
593	2015-01-27 02:05:35.812524+01	46	C	3		7	6
594	2015-01-27 02:05:48.044178+01	26	Tradies	3		7	6
595	2015-01-27 04:33:08.404725+01	13	Meegan@graphicsforcars.com.au	3		6	6
596	2015-01-27 04:33:23.431369+01	16	Meegan@graphicsforcars.com.au	1		6	6
597	2015-01-27 04:33:40.428445+01	16	Meegan@graphicsforcars.com.au	2	Changed name, phone and group.	6	6
598	2015-01-27 04:36:01.831115+01	16	meegan@graphicsforcars.com.au	2	Changed email.	6	6
599	2015-01-27 11:03:07.511661+01	116	sdfsdfsdf	3		8	2
600	2015-01-27 11:03:07.665674+01	115	dfgdfgd	3		8	2
601	2015-01-27 11:03:07.817153+01	113	stas revas	3		8	2
602	2015-01-27 13:18:04.022481+01	188	Request object	2	Changed status.	10	2
603	2015-01-27 14:41:20.905237+01	199	Request object	2	Changed calculation_json.	10	2
604	2015-01-27 14:41:52.346672+01	199	Request object	2	Changed status.	10	2
605	2015-01-27 22:05:57.655711+01	200	Request object	2	Changed status.	10	2
606	2015-01-28 01:43:50.440247+01	200	Request object	3		10	6
607	2015-01-28 01:43:50.442872+01	199	Request object	3		10	6
608	2015-01-28 02:11:58.128151+01	197	Request object	2	Changed status.	10	6
609	2015-01-28 02:12:15.257715+01	194	Request object	2	Changed status.	10	6
610	2015-01-28 02:15:01.315327+01	201	Request object	2	Changed company.	10	6
611	2015-01-28 02:15:58.810509+01	194	Request object	2	No fields changed.	10	6
612	2015-01-28 02:16:18.882291+01	192	Request object	2	Changed status.	10	6
613	2015-01-28 10:22:03.041188+01	63	a	3		7	2
614	2015-01-28 10:22:03.232022+01	67	h	3		7	2
615	2015-01-28 10:22:03.408172+01	44	Sign Maker	3		7	2
616	2015-01-28 10:22:03.584171+01	42	Sign Maker	3		7	2
617	2015-01-28 10:22:16.79683+01	66	Sign Maker	2	Changed title.	7	2
618	2015-01-29 00:13:13.213206+01	188	Request object	3		10	6
619	2015-01-29 06:32:55.749014+01	105	MoneyX	3		8	6
620	2015-01-29 07:06:52.927497+01	204	Request object	3		10	6
621	2015-01-30 00:33:40.580645+01	218	Request object	2	Changed company.	10	6
622	2015-01-30 00:34:42.708917+01	212	Request object	2	No fields changed.	10	6
623	2015-01-30 00:36:03.750618+01	217	Request object	2	Changed company.	10	6
624	2015-01-30 00:36:27.63632+01	217	Request object	3		10	6
625	2015-01-30 00:36:57.450005+01	223	Request object	3		10	6
626	2015-01-30 00:36:57.452194+01	222	Request object	3		10	6
627	2015-01-30 00:36:57.453463+01	221	Request object	3		10	6
628	2015-01-30 00:36:57.455101+01	220	Request object	3		10	6
629	2015-01-30 00:36:57.457111+01	219	Request object	3		10	6
630	2015-01-30 00:38:26.917077+01	216	Request object	3		10	6
631	2015-01-30 01:36:07.57876+01	8	test@test.ru	3		6	6
632	2015-01-30 01:36:42.624409+01	17	rene@graphicsforcars.com.au	1		6	6
633	2015-01-30 01:37:04.617444+01	17	rene@graphicsforcars.com.au	2	Changed name, phone and group.	6	6
634	2015-01-30 01:38:05.725138+01	18	vinklpetr@gmail.com	1		6	6
635	2015-01-30 01:38:45.206368+01	18	vinklpetr@gmail.com	2	Changed name, phone and group.	6	6
636	2015-01-30 02:04:35.760214+01	75	hgjbkmnl	3		7	6
637	2015-01-30 02:04:35.762525+01	74	F	3		7	6
638	2015-01-30 02:54:52.784473+01	80	Car dealer	2	Changed title.	7	6
\.


--
-- Name: django_admin_log_id_seq; Type: SEQUENCE SET; Schema: public; Owner: stas
--

SELECT pg_catalog.setval('django_admin_log_id_seq', 638, true);


--
-- Data for Name: django_content_type; Type: TABLE DATA; Schema: public; Owner: stas
--

COPY django_content_type (id, name, app_label, model) FROM stdin;
1	log entry	admin	logentry
2	permission	auth	permission
3	group	auth	group
4	content type	contenttypes	contenttype
5	session	sessions	session
6	my user	office	myuser
7	company category	office	companycategory
8	company	office	company
9	contact person	office	contactperson
10	request	office	request
11	attachment	office	attachment
12	organizer	office	organizer
13	material	office	material
14	lamination	office	lamination
15	Plot price sam	office	plotpricesam
16	customer type	office	customertype
17	print	office	print
18	plot	office	plot
19	settings content	office	settingscontent
\.


--
-- Name: django_content_type_id_seq; Type: SEQUENCE SET; Schema: public; Owner: stas
--

SELECT pg_catalog.setval('django_content_type_id_seq', 19, true);


--
-- Data for Name: django_migrations; Type: TABLE DATA; Schema: public; Owner: stas
--

COPY django_migrations (id, app, name, applied) FROM stdin;
1	contenttypes	0001_initial	2014-12-23 12:12:15.618969+01
2	admin	0001_initial	2014-12-23 12:12:16.263671+01
3	auth	0001_initial	2014-12-23 12:12:17.162869+01
4	sessions	0001_initial	2014-12-23 12:12:17.658065+01
5	office	0001_initial	2014-12-23 14:48:35.918109+01
6	office	0002_auto_20141223_1351	2014-12-23 14:51:31.114026+01
7	office	0003_attachment_request	2014-12-23 15:21:54.959731+01
8	office	0004_auto_20141223_1536	2014-12-23 16:36:43.944857+01
9	office	0005_auto_20141223_1537	2014-12-23 16:37:56.071878+01
10	office	0006_request_calculation_json	2014-12-24 15:14:40.430823+01
11	office	0007_auto_20141226_0837	2014-12-26 09:38:27.350159+01
12	office	0008_auto_20141226_1046	2014-12-26 11:46:45.460761+01
13	office	0009_auto_20141227_0026	2014-12-27 01:26:17.583723+01
14	office	0010_organizer_user	2014-12-27 01:27:02.807405+01
15	office	0011_auto_20150106_0827	2015-01-06 09:27:17.338415+01
16	office	0012_auto_20150106_0949	2015-01-06 11:40:12.333396+01
17	office	0013_myuser_name	2015-01-09 12:26:00.708687+01
18	office	0014_myuser_phone	2015-01-09 12:28:09.567695+01
20	office	0015_auto_20150109_1423	2015-01-09 15:26:31.273905+01
21	office	0016_myuser_group	2015-01-09 15:26:32.231166+01
22	office	0017_settingscontent	2015-01-13 09:33:33.222576+01
23	office	0018_auto_20150113_0843	2015-01-13 09:43:16.697077+01
24	office	0019_request_quotation_declined	2015-01-13 09:59:38.779033+01
25	office	0020_auto_20150115_0924	2015-01-15 10:24:57.997661+01
26	office	0021_auto_20150119_0846	2015-01-19 09:46:54.63764+01
27	office	0022_auto_20150124_0000	2015-01-23 14:01:09.653673+01
28	office	0023_auto_20150127_2100	2015-01-27 11:00:18.064961+01
\.


--
-- Name: django_migrations_id_seq; Type: SEQUENCE SET; Schema: public; Owner: stas
--

SELECT pg_catalog.setval('django_migrations_id_seq', 28, true);


--
-- Data for Name: django_session; Type: TABLE DATA; Schema: public; Owner: stas
--

COPY django_session (session_key, session_data, expire_date) FROM stdin;
kl9qn7oves5ssv2ktv3un15ty7tdv612	MGM3ZjMwODBiZDc2NzcyMDE0ZjFjYzg4MjkxNGYxNWIxNTJlYTkyOTp7Il9hdXRoX3VzZXJfaGFzaCI6ImQxZjlmOTIyNmY1NWQyMzRmYTYxNTk5ZGQwMzg0ZjAwNzYwZmY5OGEiLCJfYXV0aF91c2VyX2JhY2tlbmQiOiJkamFuZ28uY29udHJpYi5hdXRoLmJhY2tlbmRzLk1vZGVsQmFja2VuZCIsIl9hdXRoX3VzZXJfaWQiOjJ9	2015-01-09 09:40:43.866304+01
nvonitxfah8eb7hf1uxl59yduxzl9c1p	MGM3ZjMwODBiZDc2NzcyMDE0ZjFjYzg4MjkxNGYxNWIxNTJlYTkyOTp7Il9hdXRoX3VzZXJfaGFzaCI6ImQxZjlmOTIyNmY1NWQyMzRmYTYxNTk5ZGQwMzg0ZjAwNzYwZmY5OGEiLCJfYXV0aF91c2VyX2JhY2tlbmQiOiJkamFuZ28uY29udHJpYi5hdXRoLmJhY2tlbmRzLk1vZGVsQmFja2VuZCIsIl9hdXRoX3VzZXJfaWQiOjJ9	2015-01-11 09:01:46.337515+01
jwodwpt0ggnts9652b7xh98x1kcl7hn4	MGM3ZjMwODBiZDc2NzcyMDE0ZjFjYzg4MjkxNGYxNWIxNTJlYTkyOTp7Il9hdXRoX3VzZXJfaGFzaCI6ImQxZjlmOTIyNmY1NWQyMzRmYTYxNTk5ZGQwMzg0ZjAwNzYwZmY5OGEiLCJfYXV0aF91c2VyX2JhY2tlbmQiOiJkamFuZ28uY29udHJpYi5hdXRoLmJhY2tlbmRzLk1vZGVsQmFja2VuZCIsIl9hdXRoX3VzZXJfaWQiOjJ9	2015-01-09 10:10:54.319283+01
6t6ibuhkwmatq16fz27fywrtvunfkp03	ZmYxODIwZDg2NWVhNDU4MWFiZGQwNzYzMDcwMDkxN2UzODdhYTUxZjp7fQ==	2015-01-07 09:35:27.875895+01
wtyb9bkeoqskotzpoa8kxjm9ce7o6l71	MGM3ZjMwODBiZDc2NzcyMDE0ZjFjYzg4MjkxNGYxNWIxNTJlYTkyOTp7Il9hdXRoX3VzZXJfaGFzaCI6ImQxZjlmOTIyNmY1NWQyMzRmYTYxNTk5ZGQwMzg0ZjAwNzYwZmY5OGEiLCJfYXV0aF91c2VyX2JhY2tlbmQiOiJkamFuZ28uY29udHJpYi5hdXRoLmJhY2tlbmRzLk1vZGVsQmFja2VuZCIsIl9hdXRoX3VzZXJfaWQiOjJ9	2015-01-28 20:24:41.548412+01
5t3h4srru46gsf5fhagg5i6nymeycpba	MGM3ZjMwODBiZDc2NzcyMDE0ZjFjYzg4MjkxNGYxNWIxNTJlYTkyOTp7Il9hdXRoX3VzZXJfaGFzaCI6ImQxZjlmOTIyNmY1NWQyMzRmYTYxNTk5ZGQwMzg0ZjAwNzYwZmY5OGEiLCJfYXV0aF91c2VyX2JhY2tlbmQiOiJkamFuZ28uY29udHJpYi5hdXRoLmJhY2tlbmRzLk1vZGVsQmFja2VuZCIsIl9hdXRoX3VzZXJfaWQiOjJ9	2015-01-11 09:21:38.387524+01
1355tl2n8ehto5guc9t2phusfcf1yrk2	MGM3ZjMwODBiZDc2NzcyMDE0ZjFjYzg4MjkxNGYxNWIxNTJlYTkyOTp7Il9hdXRoX3VzZXJfaGFzaCI6ImQxZjlmOTIyNmY1NWQyMzRmYTYxNTk5ZGQwMzg0ZjAwNzYwZmY5OGEiLCJfYXV0aF91c2VyX2JhY2tlbmQiOiJkamFuZ28uY29udHJpYi5hdXRoLmJhY2tlbmRzLk1vZGVsQmFja2VuZCIsIl9hdXRoX3VzZXJfaWQiOjJ9	2015-01-29 09:49:39.122+01
rb4wcqb5iw7xbem59nd4b40vuakwnh4m	ZmYxODIwZDg2NWVhNDU4MWFiZGQwNzYzMDcwMDkxN2UzODdhYTUxZjp7fQ==	2015-01-23 09:47:13.978258+01
yc72ufo7urh7si2lg2f7i9xerygpit6l	ZmYxODIwZDg2NWVhNDU4MWFiZGQwNzYzMDcwMDkxN2UzODdhYTUxZjp7fQ==	2015-01-24 13:59:11.982977+01
t7isodhzwganm0f8yccc3k2en07j5gwk	ZmYxODIwZDg2NWVhNDU4MWFiZGQwNzYzMDcwMDkxN2UzODdhYTUxZjp7fQ==	2015-01-07 12:44:53.097316+01
c2q59mt797eoc70cuxtvg1bcqwim18dj	MGM3ZjMwODBiZDc2NzcyMDE0ZjFjYzg4MjkxNGYxNWIxNTJlYTkyOTp7Il9hdXRoX3VzZXJfaGFzaCI6ImQxZjlmOTIyNmY1NWQyMzRmYTYxNTk5ZGQwMzg0ZjAwNzYwZmY5OGEiLCJfYXV0aF91c2VyX2JhY2tlbmQiOiJkamFuZ28uY29udHJpYi5hdXRoLmJhY2tlbmRzLk1vZGVsQmFja2VuZCIsIl9hdXRoX3VzZXJfaWQiOjJ9	2015-02-13 12:22:49.606959+01
wnhi1z2lp4qzbt90y0jvy6368p77et86	MGM3ZjMwODBiZDc2NzcyMDE0ZjFjYzg4MjkxNGYxNWIxNTJlYTkyOTp7Il9hdXRoX3VzZXJfaGFzaCI6ImQxZjlmOTIyNmY1NWQyMzRmYTYxNTk5ZGQwMzg0ZjAwNzYwZmY5OGEiLCJfYXV0aF91c2VyX2JhY2tlbmQiOiJkamFuZ28uY29udHJpYi5hdXRoLmJhY2tlbmRzLk1vZGVsQmFja2VuZCIsIl9hdXRoX3VzZXJfaWQiOjJ9	2015-01-26 08:51:58.109+01
jmez7wbsifeqsdq9fk1tbu01lwwz0s22	MGM3ZjMwODBiZDc2NzcyMDE0ZjFjYzg4MjkxNGYxNWIxNTJlYTkyOTp7Il9hdXRoX3VzZXJfaGFzaCI6ImQxZjlmOTIyNmY1NWQyMzRmYTYxNTk5ZGQwMzg0ZjAwNzYwZmY5OGEiLCJfYXV0aF91c2VyX2JhY2tlbmQiOiJkamFuZ28uY29udHJpYi5hdXRoLmJhY2tlbmRzLk1vZGVsQmFja2VuZCIsIl9hdXRoX3VzZXJfaWQiOjJ9	2015-02-04 12:38:16.095+01
180l7isifg13c3elnr4atki1gzt4mzof	ZTY0YmUyY2ZiMjQ3MDg2M2NkMDY3YWY5NWQ0OGVmNjVjYzUyMDNiZjp7Il9hdXRoX3VzZXJfaGFzaCI6IjQyZDg4NGNjMTViOWFjMDRiMGIxZGRkNjE4NDgwNGMwYmNhYTRiMjAiLCJfYXV0aF91c2VyX2JhY2tlbmQiOiJkamFuZ28uY29udHJpYi5hdXRoLmJhY2tlbmRzLk1vZGVsQmFja2VuZCIsIl9hdXRoX3VzZXJfaWQiOjEyfQ==	2015-02-02 23:52:09.996037+01
hj40dlapvtt3il349wcxo7sja0fo4zxv	ZmYxODIwZDg2NWVhNDU4MWFiZGQwNzYzMDcwMDkxN2UzODdhYTUxZjp7fQ==	2015-01-07 12:48:46.586469+01
4on0poxa3u5n5l4vhsma5gljmy7wu0nn	NmZkZGE3YzM1NGE1MTMxYTQwYWVjMmY5NWI2Y2FkNDg2NTQ3MTI5Njp7Il9hdXRoX3VzZXJfaGFzaCI6IjcyOTcwMmZhOWM1ZTFhZDUwMjJjNTlmODFkYzhhNzViMmNjNTZjYTEiLCJfYXV0aF91c2VyX2JhY2tlbmQiOiJkamFuZ28uY29udHJpYi5hdXRoLmJhY2tlbmRzLk1vZGVsQmFja2VuZCIsIl9hdXRoX3VzZXJfaWQiOjE2fQ==	2015-02-10 04:36:06.903988+01
e8u4trm6qhblmglem6zkifzd8ttwrxf3	ZmYxODIwZDg2NWVhNDU4MWFiZGQwNzYzMDcwMDkxN2UzODdhYTUxZjp7fQ==	2015-01-08 08:12:43.843009+01
tb87eaq550j1l7ljzuizx852o7w29bx3	ZmYxODIwZDg2NWVhNDU4MWFiZGQwNzYzMDcwMDkxN2UzODdhYTUxZjp7fQ==	2015-01-08 08:41:19.649709+01
ylhrwa1tabhxpocup3jsbnrzixe1cuc9	ZmYxODIwZDg2NWVhNDU4MWFiZGQwNzYzMDcwMDkxN2UzODdhYTUxZjp7fQ==	2015-01-08 10:10:29.386183+01
ojstihjro3gymm6kr7zmf4q7abz7xc47	MGM3ZjMwODBiZDc2NzcyMDE0ZjFjYzg4MjkxNGYxNWIxNTJlYTkyOTp7Il9hdXRoX3VzZXJfaGFzaCI6ImQxZjlmOTIyNmY1NWQyMzRmYTYxNTk5ZGQwMzg0ZjAwNzYwZmY5OGEiLCJfYXV0aF91c2VyX2JhY2tlbmQiOiJkamFuZ28uY29udHJpYi5hdXRoLmJhY2tlbmRzLk1vZGVsQmFja2VuZCIsIl9hdXRoX3VzZXJfaWQiOjJ9	2015-01-08 15:42:14.502148+01
1nng6gjj6b0kwyxkel0kswaepskbprs1	ZmYxODIwZDg2NWVhNDU4MWFiZGQwNzYzMDcwMDkxN2UzODdhYTUxZjp7fQ==	2015-01-08 23:54:48.314865+01
8dnbsyt1utat2v33tpbna8yms2rah9zy	ZmYxODIwZDg2NWVhNDU4MWFiZGQwNzYzMDcwMDkxN2UzODdhYTUxZjp7fQ==	2015-01-09 01:22:24.879028+01
jjraojvvtt9jbclcymohjsiz8ksallyl	ZmYxODIwZDg2NWVhNDU4MWFiZGQwNzYzMDcwMDkxN2UzODdhYTUxZjp7fQ==	2015-01-09 01:29:50.504987+01
y9jdb3envq3lkfkamq0um6yk6zmlv2bb	ZmYxODIwZDg2NWVhNDU4MWFiZGQwNzYzMDcwMDkxN2UzODdhYTUxZjp7fQ==	2015-01-09 01:39:54.096661+01
o1jjp8colyc8nr7vuyi1cjnbjrw9nsaf	ZmYxODIwZDg2NWVhNDU4MWFiZGQwNzYzMDcwMDkxN2UzODdhYTUxZjp7fQ==	2015-01-09 02:37:45.338229+01
p0itp3nu85ihx198nqmnvdybqrokkbqp	MWRkZWRiOWY0ZTgzNWY0ZjM3NmIwMWNhMDMwYTIyMjJjZTEzNDNlYzp7Il9hdXRoX3VzZXJfaGFzaCI6ImZjYzY1NzA0YzZiNmRkMGU3NjI4ZDgwZjdiMDNjYWYxYmYwNWJjMzYiLCJfYXV0aF91c2VyX2JhY2tlbmQiOiJkamFuZ28uY29udHJpYi5hdXRoLmJhY2tlbmRzLk1vZGVsQmFja2VuZCIsIl9hdXRoX3VzZXJfaWQiOjN9	2015-01-09 02:40:19.218705+01
onh2p64it8cdg1k45sn3lx53dkstdtle	MmY2ZmU1M2Y0NDFlMmExYWMxZGJkM2FjNjgwYmY4MmMxNjM0NjE4NTp7Il9hdXRoX3VzZXJfaGFzaCI6IjRjMGY2ZjgyMmIzOGNhNThiYjg4OWQ1ZTE1YjI1NGZhYjViYjU4YzEiLCJfbWVzc2FnZXMiOiJbW1wiX19qc29uX21lc3NhZ2VcIiwwLDI1LFwiVGhlIHJlcXVlc3QgXFxcIlJlcXVlc3Qgb2JqZWN0XFxcIiB3YXMgY2hhbmdlZCBzdWNjZXNzZnVsbHkuXCJdLFtcIl9fanNvbl9tZXNzYWdlXCIsMCwyNSxcIlRoZSByZXF1ZXN0IFxcXCJSZXF1ZXN0IG9iamVjdFxcXCIgd2FzIGNoYW5nZWQgc3VjY2Vzc2Z1bGx5LlwiXSxbXCJfX2pzb25fbWVzc2FnZVwiLDAsMjUsXCJUaGUgcmVxdWVzdCBcXFwiUmVxdWVzdCBvYmplY3RcXFwiIHdhcyBjaGFuZ2VkIHN1Y2Nlc3NmdWxseS5cIl0sW1wiX19qc29uX21lc3NhZ2VcIiwwLDI1LFwiU3VjY2Vzc2Z1bGx5IGRlbGV0ZWQgMSByZXF1ZXN0LlwiXSxbXCJfX2pzb25fbWVzc2FnZVwiLDAsMjUsXCJTdWNjZXNzZnVsbHkgZGVsZXRlZCA1IHJlcXVlc3RzLlwiXSxbXCJfX2pzb25fbWVzc2FnZVwiLDAsMjUsXCJTdWNjZXNzZnVsbHkgZGVsZXRlZCAxIHJlcXVlc3QuXCJdLFtcIl9fanNvbl9tZXNzYWdlXCIsMCwyNSxcIlN1Y2Nlc3NmdWxseSBkZWxldGVkIDEgbXkgdXNlci5cIl0sW1wiX19qc29uX21lc3NhZ2VcIiwwLDI1LFwiVGhlIG15IHVzZXIgXFxcInJlbmVAZ3JhcGhpY3Nmb3JjYXJzLmNvbS5hdVxcXCIgd2FzIGFkZGVkIHN1Y2Nlc3NmdWxseS4gWW91IG1heSBlZGl0IGl0IGFnYWluIGJlbG93LlwiXSxbXCJfX2pzb25fbWVzc2FnZVwiLDAsMjUsXCJUaGUgbXkgdXNlciBcXFwicmVuZUBncmFwaGljc2ZvcmNhcnMuY29tLmF1XFxcIiB3YXMgY2hhbmdlZCBzdWNjZXNzZnVsbHkuXCJdLFtcIl9fanNvbl9tZXNzYWdlXCIsMCwyNSxcIlRoZSBteSB1c2VyIFxcXCJ2aW5rbHBldHJAZ21haWwuY29tXFxcIiB3YXMgYWRkZWQgc3VjY2Vzc2Z1bGx5LiBZb3UgbWF5IGVkaXQgaXQgYWdhaW4gYmVsb3cuXCJdLFtcIl9fanNvbl9tZXNzYWdlXCIsMCwyNSxcIlRoZSBteSB1c2VyIFxcXCJ2aW5rbHBldHJAZ21haWwuY29tXFxcIiB3YXMgY2hhbmdlZCBzdWNjZXNzZnVsbHkuXCJdLFtcIl9fanNvbl9tZXNzYWdlXCIsMCwyNSxcIlN1Y2Nlc3NmdWxseSBkZWxldGVkIDIgY29tcGFueSBjYXRlZ29yeXMuXCJdLFtcIl9fanNvbl9tZXNzYWdlXCIsMCwyNSxcIlRoZSBjb21wYW55IGNhdGVnb3J5IFxcXCJDYXIgZGVhbGVyXFxcIiB3YXMgY2hhbmdlZCBzdWNjZXNzZnVsbHkuXCJdXSIsIl9hdXRoX3VzZXJfaWQiOjYsIl9hdXRoX3VzZXJfYmFja2VuZCI6ImRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kIn0=	2015-02-13 02:54:52.797565+01
1nxgdggift7fuqbpz5rit2518ujbvnxr	MmMzOWQ5ZmFhOTY0NTkyMTZiYWQyNjczOTIwYWZkODUyMDQ3NDM1Mzp7Il9hdXRoX3VzZXJfaGFzaCI6ImQwOGU2OGI4ZTcyYTMwMWNmYjczODY3NmI0MjYzNGU4NDhlOTYxYzEiLCJfYXV0aF91c2VyX2JhY2tlbmQiOiJkamFuZ28uY29udHJpYi5hdXRoLmJhY2tlbmRzLk1vZGVsQmFja2VuZCIsIl9hdXRoX3VzZXJfaWQiOjExfQ==	2015-02-11 02:12:14.268794+01
4hzw2l9ue4m8m30vfsj6vlxqdi0viw5z	ZGJmYjhkZDdmYmUwZjNmNzE4NzNhNGNiYmNhYmU2YWNlOTIyOWEyMjp7Il9hdXRoX3VzZXJfaGFzaCI6IjZmNmZiNmU0MGQ0MTI2NmM5Mzk4OWViY2M4MTQ5MmQ0NGYxYTEzYzAiLCJfYXV0aF91c2VyX2JhY2tlbmQiOiJkamFuZ28uY29udHJpYi5hdXRoLmJhY2tlbmRzLk1vZGVsQmFja2VuZCIsIl9hdXRoX3VzZXJfaWQiOjEwfQ==	2015-01-29 10:58:38.169525+01
6nqus55a47iafi982equl8rb9a0vm57t	MGM3ZjMwODBiZDc2NzcyMDE0ZjFjYzg4MjkxNGYxNWIxNTJlYTkyOTp7Il9hdXRoX3VzZXJfaGFzaCI6ImQxZjlmOTIyNmY1NWQyMzRmYTYxNTk5ZGQwMzg0ZjAwNzYwZmY5OGEiLCJfYXV0aF91c2VyX2JhY2tlbmQiOiJkamFuZ28uY29udHJpYi5hdXRoLmJhY2tlbmRzLk1vZGVsQmFja2VuZCIsIl9hdXRoX3VzZXJfaWQiOjJ9	2015-01-26 14:20:55.549655+01
cw66tuqwkc2d3xwc6pqjfl6i3wfgnyrm	MDU0NDg1MGY3Mzg3YjllMTZkOTUyNGFjNjliMjNmZmFmNDFlMThjNzp7Il9hdXRoX3VzZXJfaGFzaCI6IjRjMGY2ZjgyMmIzOGNhNThiYjg4OWQ1ZTE1YjI1NGZhYjViYjU4YzEiLCJfYXV0aF91c2VyX2JhY2tlbmQiOiJkamFuZ28uY29udHJpYi5hdXRoLmJhY2tlbmRzLk1vZGVsQmFja2VuZCIsIl9hdXRoX3VzZXJfaWQiOjZ9	2015-01-12 11:01:17.75797+01
qwsy4ad2ojdn3n3avlx92j7vn1q5bfo5	MGM3ZjMwODBiZDc2NzcyMDE0ZjFjYzg4MjkxNGYxNWIxNTJlYTkyOTp7Il9hdXRoX3VzZXJfaGFzaCI6ImQxZjlmOTIyNmY1NWQyMzRmYTYxNTk5ZGQwMzg0ZjAwNzYwZmY5OGEiLCJfYXV0aF91c2VyX2JhY2tlbmQiOiJkamFuZ28uY29udHJpYi5hdXRoLmJhY2tlbmRzLk1vZGVsQmFja2VuZCIsIl9hdXRoX3VzZXJfaWQiOjJ9	2015-02-13 12:26:20.53205+01
br8o1sqvq7ds9604y9mgussclgbr9yy8	ZmYxODIwZDg2NWVhNDU4MWFiZGQwNzYzMDcwMDkxN2UzODdhYTUxZjp7fQ==	2015-01-23 09:47:22.156912+01
pjqh977ip1b5h4axyfhgk3kqbafewlt0	MDI5MDQ4NTE0NTBkNWY4OWYzOWJlZjlmZTI4NjAzMTgzOGU1OTQ4OTp7Il9hdXRoX3VzZXJfaGFzaCI6Ijg0MjM1Y2Q0MmY5OWFiNzg2NmFiNjc4Yjg2ODY0NmFiODBjNjM3MGMiLCJfYXV0aF91c2VyX2JhY2tlbmQiOiJkamFuZ28uY29udHJpYi5hdXRoLmJhY2tlbmRzLk1vZGVsQmFja2VuZCIsIl9hdXRoX3VzZXJfaWQiOjV9	2015-02-01 14:36:16.412192+01
q0v2fmtxvxyxp5e5vwjunhj611awy8ly	MGM3ZjMwODBiZDc2NzcyMDE0ZjFjYzg4MjkxNGYxNWIxNTJlYTkyOTp7Il9hdXRoX3VzZXJfaGFzaCI6ImQxZjlmOTIyNmY1NWQyMzRmYTYxNTk5ZGQwMzg0ZjAwNzYwZmY5OGEiLCJfYXV0aF91c2VyX2JhY2tlbmQiOiJkamFuZ28uY29udHJpYi5hdXRoLmJhY2tlbmRzLk1vZGVsQmFja2VuZCIsIl9hdXRoX3VzZXJfaWQiOjJ9	2015-02-11 08:38:39.820227+01
1e6f6g41p752h3bs8egdy6neidra4nk3	MmMzOWQ5ZmFhOTY0NTkyMTZiYWQyNjczOTIwYWZkODUyMDQ3NDM1Mzp7Il9hdXRoX3VzZXJfaGFzaCI6ImQwOGU2OGI4ZTcyYTMwMWNmYjczODY3NmI0MjYzNGU4NDhlOTYxYzEiLCJfYXV0aF91c2VyX2JhY2tlbmQiOiJkamFuZ28uY29udHJpYi5hdXRoLmJhY2tlbmRzLk1vZGVsQmFja2VuZCIsIl9hdXRoX3VzZXJfaWQiOjExfQ==	2015-02-12 00:00:11.669811+01
iw5nou30jbeh1fjb9pdg06x6intjhq6x	MDU0NDg1MGY3Mzg3YjllMTZkOTUyNGFjNjliMjNmZmFmNDFlMThjNzp7Il9hdXRoX3VzZXJfaGFzaCI6IjRjMGY2ZjgyMmIzOGNhNThiYjg4OWQ1ZTE1YjI1NGZhYjViYjU4YzEiLCJfYXV0aF91c2VyX2JhY2tlbmQiOiJkamFuZ28uY29udHJpYi5hdXRoLmJhY2tlbmRzLk1vZGVsQmFja2VuZCIsIl9hdXRoX3VzZXJfaWQiOjZ9	2015-02-03 12:50:27.13104+01
rvizou58kn8kdarbva0w6n0vrk1ptvap	ZWFkZWQ5YWRlZDhjODhiOGFlMmQ2MDcyYjk1MDBlM2JiNzI5ZDBlYTp7Il9hdXRoX3VzZXJfaGFzaCI6IjE3MjZiMTljNTBlYjU4ZmM1OTFiNzUxZGM4ZWMxYmM2MGEzN2MyYzkiLCJfYXV0aF91c2VyX2JhY2tlbmQiOiJkamFuZ28uY29udHJpYi5hdXRoLmJhY2tlbmRzLk1vZGVsQmFja2VuZCIsIl9hdXRoX3VzZXJfaWQiOjE4fQ==	2015-02-13 02:56:01.304508+01
ugekozgvn9suxvzbc3k72u2wjwihkc6s	ZmYxODIwZDg2NWVhNDU4MWFiZGQwNzYzMDcwMDkxN2UzODdhYTUxZjp7fQ==	2015-02-06 02:52:28.697055+01
n7i2a2l7toa7gxmy3ufrhew9zel39jz6	MDU0NDg1MGY3Mzg3YjllMTZkOTUyNGFjNjliMjNmZmFmNDFlMThjNzp7Il9hdXRoX3VzZXJfaGFzaCI6IjRjMGY2ZjgyMmIzOGNhNThiYjg4OWQ1ZTE1YjI1NGZhYjViYjU4YzEiLCJfYXV0aF91c2VyX2JhY2tlbmQiOiJkamFuZ28uY29udHJpYi5hdXRoLmJhY2tlbmRzLk1vZGVsQmFja2VuZCIsIl9hdXRoX3VzZXJfaWQiOjZ9	2015-01-13 00:46:19.177656+01
c4upzvad4dm729ahl3fskbuufrenyway	MGM3ZjMwODBiZDc2NzcyMDE0ZjFjYzg4MjkxNGYxNWIxNTJlYTkyOTp7Il9hdXRoX3VzZXJfaGFzaCI6ImQxZjlmOTIyNmY1NWQyMzRmYTYxNTk5ZGQwMzg0ZjAwNzYwZmY5OGEiLCJfYXV0aF91c2VyX2JhY2tlbmQiOiJkamFuZ28uY29udHJpYi5hdXRoLmJhY2tlbmRzLk1vZGVsQmFja2VuZCIsIl9hdXRoX3VzZXJfaWQiOjJ9	2015-02-12 23:39:33.464503+01
8m0bb51w5q1p3zhw74cg9jc3a0cvbqfu	NzM2ZTJhNzE4NGFjY2U5MjNhNmRhNTY1ZThkZmYzMDc2MGQ4YzE2ZDp7Il9hdXRoX3VzZXJfYmFja2VuZCI6ImRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kIiwiX2F1dGhfdXNlcl9oYXNoIjoiZDFmOWY5MjI2ZjU1ZDIzNGZhNjE1OTlkZDAzODRmMDA3NjBmZjk4YSIsIl9hdXRoX3VzZXJfaWQiOjIsIl9tZXNzYWdlcyI6IltbXCJfX2pzb25fbWVzc2FnZVwiLDAsMjUsXCJUaGUgcmVxdWVzdCBcXFwiUmVxdWVzdCBvYmplY3RcXFwiIHdhcyBjaGFuZ2VkIHN1Y2Nlc3NmdWxseS5cIl0sW1wiX19qc29uX21lc3NhZ2VcIiwwLDI1LFwiVGhlIHJlcXVlc3QgXFxcIlJlcXVlc3Qgb2JqZWN0XFxcIiB3YXMgY2hhbmdlZCBzdWNjZXNzZnVsbHkuXCJdLFtcIl9fanNvbl9tZXNzYWdlXCIsMCwyNSxcIlRoZSByZXF1ZXN0IFxcXCJSZXF1ZXN0IG9iamVjdFxcXCIgd2FzIGNoYW5nZWQgc3VjY2Vzc2Z1bGx5LlwiXSxbXCJfX2pzb25fbWVzc2FnZVwiLDAsMjUsXCJUaGUgcmVxdWVzdCBcXFwiUmVxdWVzdCBvYmplY3RcXFwiIHdhcyBjaGFuZ2VkIHN1Y2Nlc3NmdWxseS5cIl0sW1wiX19qc29uX21lc3NhZ2VcIiwwLDI1LFwiVGhlIHJlcXVlc3QgXFxcIlJlcXVlc3Qgb2JqZWN0XFxcIiB3YXMgY2hhbmdlZCBzdWNjZXNzZnVsbHkuXCJdLFtcIl9fanNvbl9tZXNzYWdlXCIsMCwyNSxcIlRoZSByZXF1ZXN0IFxcXCJSZXF1ZXN0IG9iamVjdFxcXCIgd2FzIGNoYW5nZWQgc3VjY2Vzc2Z1bGx5LlwiXSxbXCJfX2pzb25fbWVzc2FnZVwiLDAsMjUsXCJTdWNjZXNzZnVsbHkgZGVsZXRlZCAyIHJlcXVlc3RzLlwiXSxbXCJfX2pzb25fbWVzc2FnZVwiLDAsMjUsXCJUaGUgcmVxdWVzdCBcXFwiUmVxdWVzdCBvYmplY3RcXFwiIHdhcyBjaGFuZ2VkIHN1Y2Nlc3NmdWxseS5cIl0sW1wiX19qc29uX21lc3NhZ2VcIiwwLDI1LFwiVGhlIHNldHRpbmdzIGNvbnRlbnQgXFxcIlNldHRpbmdzQ29udGVudCBvYmplY3RcXFwiIHdhcyBhZGRlZCBzdWNjZXNzZnVsbHkuXCJdLFtcIl9fanNvbl9tZXNzYWdlXCIsMCwyNSxcIlRoZSBzZXR0aW5ncyBjb250ZW50IFxcXCJTZXR0aW5nc0NvbnRlbnQgb2JqZWN0XFxcIiB3YXMgYWRkZWQgc3VjY2Vzc2Z1bGx5LlwiXSxbXCJfX2pzb25fbWVzc2FnZVwiLDAsMjUsXCJUaGUgc2V0dGluZ3MgY29udGVudCBcXFwiU2V0dGluZ3NDb250ZW50IG9iamVjdFxcXCIgd2FzIGFkZGVkIHN1Y2Nlc3NmdWxseS5cIl0sW1wiX19qc29uX21lc3NhZ2VcIiwwLDI1LFwiVGhlIHJlcXVlc3QgXFxcIlJlcXVlc3Qgb2JqZWN0XFxcIiB3YXMgY2hhbmdlZCBzdWNjZXNzZnVsbHkuXCJdLFtcIl9fanNvbl9tZXNzYWdlXCIsMCwyNSxcIlRoZSBncm91cCBcXFwiR2VuZXJhbCBtYW5hZ2VyXFxcIiB3YXMgY2hhbmdlZCBzdWNjZXNzZnVsbHkuXCJdLFtcIl9fanNvbl9tZXNzYWdlXCIsMCwyNSxcIlRoZSBteSB1c2VyIFxcXCJyZXZhc3RhbmlzbGF2QGdtYWlsLmNvbVxcXCIgd2FzIGNoYW5nZWQgc3VjY2Vzc2Z1bGx5LlwiXSxbXCJfX2pzb25fbWVzc2FnZVwiLDAsMjUsXCJUaGUgZ3JvdXAgXFxcIkFkbWluXFxcIiB3YXMgYWRkZWQgc3VjY2Vzc2Z1bGx5LlwiXSxbXCJfX2pzb25fbWVzc2FnZVwiLDAsMjUsXCJUaGUgbXkgdXNlciBcXFwicmV2YXN0YW5pc2xhdkBnbWFpbC5jb21cXFwiIHdhcyBjaGFuZ2VkIHN1Y2Nlc3NmdWxseS5cIl0sW1wiX19qc29uX21lc3NhZ2VcIiwwLDI1LFwiVGhlIHJlcXVlc3QgXFxcIlJlcXVlc3Qgb2JqZWN0XFxcIiB3YXMgY2hhbmdlZCBzdWNjZXNzZnVsbHkuXCJdLFtcIl9fanNvbl9tZXNzYWdlXCIsMCwyNSxcIlRoZSByZXF1ZXN0IFxcXCJSZXF1ZXN0IG9iamVjdFxcXCIgd2FzIGNoYW5nZWQgc3VjY2Vzc2Z1bGx5LlwiXSxbXCJfX2pzb25fbWVzc2FnZVwiLDAsMjUsXCJUaGUgcmVxdWVzdCBcXFwiUmVxdWVzdCBvYmplY3RcXFwiIHdhcyBjaGFuZ2VkIHN1Y2Nlc3NmdWxseS4gWW91IG1heSBlZGl0IGl0IGFnYWluIGJlbG93LlwiXSxbXCJfX2pzb25fbWVzc2FnZVwiLDAsMjUsXCJUaGUgcmVxdWVzdCBcXFwiUmVxdWVzdCBvYmplY3RcXFwiIHdhcyBjaGFuZ2VkIHN1Y2Nlc3NmdWxseS4gWW91IG1heSBlZGl0IGl0IGFnYWluIGJlbG93LlwiXSxbXCJfX2pzb25fbWVzc2FnZVwiLDAsMjUsXCJUaGUgcmVxdWVzdCBcXFwiUmVxdWVzdCBvYmplY3RcXFwiIHdhcyBjaGFuZ2VkIHN1Y2Nlc3NmdWxseS5cIl0sW1wiX19qc29uX21lc3NhZ2VcIiwwLDI1LFwiVGhlIHJlcXVlc3QgXFxcIlJlcXVlc3Qgb2JqZWN0XFxcIiB3YXMgY2hhbmdlZCBzdWNjZXNzZnVsbHkuXCJdLFtcIl9fanNvbl9tZXNzYWdlXCIsMCwyNSxcIlN1Y2Nlc3NmdWxseSBkZWxldGVkIDEgcmVxdWVzdC5cIl0sW1wiX19qc29uX21lc3NhZ2VcIiwwLDI1LFwiVGhlIHJlcXVlc3QgXFxcIlJlcXVlc3Qgb2JqZWN0XFxcIiB3YXMgY2hhbmdlZCBzdWNjZXNzZnVsbHkuXCJdLFtcIl9fanNvbl9tZXNzYWdlXCIsMCwyNSxcIlRoZSByZXF1ZXN0IFxcXCJSZXF1ZXN0IG9iamVjdFxcXCIgd2FzIGNoYW5nZWQgc3VjY2Vzc2Z1bGx5LlwiXSxbXCJfX2pzb25fbWVzc2FnZVwiLDAsMjUsXCJUaGUgcmVxdWVzdCBcXFwiUmVxdWVzdCBvYmplY3RcXFwiIHdhcyBjaGFuZ2VkIHN1Y2Nlc3NmdWxseS5cIl0sW1wiX19qc29uX21lc3NhZ2VcIiwwLDI1LFwiVGhlIHJlcXVlc3QgXFxcIlJlcXVlc3Qgb2JqZWN0XFxcIiB3YXMgY2hhbmdlZCBzdWNjZXNzZnVsbHkuXCJdLFtcIl9fanNvbl9tZXNzYWdlXCIsMCwyNSxcIlRoZSByZXF1ZXN0IFxcXCJSZXF1ZXN0IG9iamVjdFxcXCIgd2FzIGNoYW5nZWQgc3VjY2Vzc2Z1bGx5LlwiXSxbXCJfX2pzb25fbWVzc2FnZVwiLDAsMjUsXCJUaGUgcmVxdWVzdCBcXFwiUmVxdWVzdCBvYmplY3RcXFwiIHdhcyBjaGFuZ2VkIHN1Y2Nlc3NmdWxseS5cIl0sW1wiX19qc29uX21lc3NhZ2VcIiwwLDI1LFwiVGhlIHJlcXVlc3QgXFxcIlJlcXVlc3Qgb2JqZWN0XFxcIiB3YXMgY2hhbmdlZCBzdWNjZXNzZnVsbHkuXCJdLFtcIl9fanNvbl9tZXNzYWdlXCIsMCwyNSxcIlRoZSByZXF1ZXN0IFxcXCJSZXF1ZXN0IG9iamVjdFxcXCIgd2FzIGNoYW5nZWQgc3VjY2Vzc2Z1bGx5LlwiXSxbXCJfX2pzb25fbWVzc2FnZVwiLDAsMjUsXCJUaGUgcmVxdWVzdCBcXFwiUmVxdWVzdCBvYmplY3RcXFwiIHdhcyBjaGFuZ2VkIHN1Y2Nlc3NmdWxseS5cIl0sW1wiX19qc29uX21lc3NhZ2VcIiwwLDI1LFwiVGhlIHJlcXVlc3QgXFxcIlJlcXVlc3Qgb2JqZWN0XFxcIiB3YXMgY2hhbmdlZCBzdWNjZXNzZnVsbHkuIFlvdSBtYXkgZWRpdCBpdCBhZ2FpbiBiZWxvdy5cIl0sW1wiX19qc29uX21lc3NhZ2VcIiwwLDI1LFwiVGhlIHJlcXVlc3QgXFxcIlJlcXVlc3Qgb2JqZWN0XFxcIiB3YXMgY2hhbmdlZCBzdWNjZXNzZnVsbHkuXCJdLFtcIl9fanNvbl9tZXNzYWdlXCIsMCwyNSxcIlRoZSByZXF1ZXN0IFxcXCJSZXF1ZXN0IG9iamVjdFxcXCIgd2FzIGNoYW5nZWQgc3VjY2Vzc2Z1bGx5LlwiXSxbXCJfX2pzb25fbWVzc2FnZVwiLDAsMjUsXCJTdWNjZXNzZnVsbHkgZGVsZXRlZCAxIGNvbXBhbnkuXCJdXSJ9	2015-02-04 09:51:25.617983+01
d3zpenu9gjwaw9osiq27z254s05y17cj	MmMzOWQ5ZmFhOTY0NTkyMTZiYWQyNjczOTIwYWZkODUyMDQ3NDM1Mzp7Il9hdXRoX3VzZXJfaGFzaCI6ImQwOGU2OGI4ZTcyYTMwMWNmYjczODY3NmI0MjYzNGU4NDhlOTYxYzEiLCJfYXV0aF91c2VyX2JhY2tlbmQiOiJkamFuZ28uY29udHJpYi5hdXRoLmJhY2tlbmRzLk1vZGVsQmFja2VuZCIsIl9hdXRoX3VzZXJfaWQiOjExfQ==	2015-02-06 01:59:15.466804+01
cknl99b7j7clim2vzu9bjs3hf14hjyco	MDU0NDg1MGY3Mzg3YjllMTZkOTUyNGFjNjliMjNmZmFmNDFlMThjNzp7Il9hdXRoX3VzZXJfaGFzaCI6IjRjMGY2ZjgyMmIzOGNhNThiYjg4OWQ1ZTE1YjI1NGZhYjViYjU4YzEiLCJfYXV0aF91c2VyX2JhY2tlbmQiOiJkamFuZ28uY29udHJpYi5hdXRoLmJhY2tlbmRzLk1vZGVsQmFja2VuZCIsIl9hdXRoX3VzZXJfaWQiOjZ9	2015-01-29 00:20:40.718886+01
7bxr32vks0gbgxo4268ya8qfsrzsojg2	MGM3ZjMwODBiZDc2NzcyMDE0ZjFjYzg4MjkxNGYxNWIxNTJlYTkyOTp7Il9hdXRoX3VzZXJfaGFzaCI6ImQxZjlmOTIyNmY1NWQyMzRmYTYxNTk5ZGQwMzg0ZjAwNzYwZmY5OGEiLCJfYXV0aF91c2VyX2JhY2tlbmQiOiJkamFuZ28uY29udHJpYi5hdXRoLmJhY2tlbmRzLk1vZGVsQmFja2VuZCIsIl9hdXRoX3VzZXJfaWQiOjJ9	2015-01-24 22:19:15.558906+01
lvvjepbkz0yomo2x2opqdty8g51s20hq	MmMzOWQ5ZmFhOTY0NTkyMTZiYWQyNjczOTIwYWZkODUyMDQ3NDM1Mzp7Il9hdXRoX3VzZXJfaGFzaCI6ImQwOGU2OGI4ZTcyYTMwMWNmYjczODY3NmI0MjYzNGU4NDhlOTYxYzEiLCJfYXV0aF91c2VyX2JhY2tlbmQiOiJkamFuZ28uY29udHJpYi5hdXRoLmJhY2tlbmRzLk1vZGVsQmFja2VuZCIsIl9hdXRoX3VzZXJfaWQiOjExfQ==	2015-02-03 13:47:43.037678+01
3jk18l5qbaxhden2hrh6tqsula0p2sfn	ZmYxODIwZDg2NWVhNDU4MWFiZGQwNzYzMDcwMDkxN2UzODdhYTUxZjp7fQ==	2015-02-11 14:55:34.442738+01
x3zd0a0asfp73qgcadt0rfjfki9vvjbu	ZmYxODIwZDg2NWVhNDU4MWFiZGQwNzYzMDcwMDkxN2UzODdhYTUxZjp7fQ==	2015-02-13 06:36:46.321109+01
w5crjgq5g78wq9o9ttaq0598nasbp6f3	ZmYxODIwZDg2NWVhNDU4MWFiZGQwNzYzMDcwMDkxN2UzODdhYTUxZjp7fQ==	2015-02-12 06:44:48.177689+01
pfhcbt2ffdncjm465z8lqkr16etsvzx6	MGM3ZjMwODBiZDc2NzcyMDE0ZjFjYzg4MjkxNGYxNWIxNTJlYTkyOTp7Il9hdXRoX3VzZXJfaGFzaCI6ImQxZjlmOTIyNmY1NWQyMzRmYTYxNTk5ZGQwMzg0ZjAwNzYwZmY5OGEiLCJfYXV0aF91c2VyX2JhY2tlbmQiOiJkamFuZ28uY29udHJpYi5hdXRoLmJhY2tlbmRzLk1vZGVsQmFja2VuZCIsIl9hdXRoX3VzZXJfaWQiOjJ9	2015-01-28 18:09:23.74661+01
fn92yhpvcis16w0ytht01xiuky26mnny	MGM3ZjMwODBiZDc2NzcyMDE0ZjFjYzg4MjkxNGYxNWIxNTJlYTkyOTp7Il9hdXRoX3VzZXJfaGFzaCI6ImQxZjlmOTIyNmY1NWQyMzRmYTYxNTk5ZGQwMzg0ZjAwNzYwZmY5OGEiLCJfYXV0aF91c2VyX2JhY2tlbmQiOiJkamFuZ28uY29udHJpYi5hdXRoLmJhY2tlbmRzLk1vZGVsQmFja2VuZCIsIl9hdXRoX3VzZXJfaWQiOjJ9	2015-02-02 10:59:26.471357+01
8spw31m9ar3y1rmsjlh4z2dqjhldw70x	ZTY0YmUyY2ZiMjQ3MDg2M2NkMDY3YWY5NWQ0OGVmNjVjYzUyMDNiZjp7Il9hdXRoX3VzZXJfaGFzaCI6IjQyZDg4NGNjMTViOWFjMDRiMGIxZGRkNjE4NDgwNGMwYmNhYTRiMjAiLCJfYXV0aF91c2VyX2JhY2tlbmQiOiJkamFuZ28uY29udHJpYi5hdXRoLmJhY2tlbmRzLk1vZGVsQmFja2VuZCIsIl9hdXRoX3VzZXJfaWQiOjEyfQ==	2015-02-06 02:54:02.204739+01
rpkbxsaukhxdp6me2rq21bnli5u5563a	MGM3ZjMwODBiZDc2NzcyMDE0ZjFjYzg4MjkxNGYxNWIxNTJlYTkyOTp7Il9hdXRoX3VzZXJfaGFzaCI6ImQxZjlmOTIyNmY1NWQyMzRmYTYxNTk5ZGQwMzg0ZjAwNzYwZmY5OGEiLCJfYXV0aF91c2VyX2JhY2tlbmQiOiJkamFuZ28uY29udHJpYi5hdXRoLmJhY2tlbmRzLk1vZGVsQmFja2VuZCIsIl9hdXRoX3VzZXJfaWQiOjJ9	2015-02-06 14:23:10.91334+01
un29bp449fz808zbqag0i8dx3swvqh4g	MGM3ZjMwODBiZDc2NzcyMDE0ZjFjYzg4MjkxNGYxNWIxNTJlYTkyOTp7Il9hdXRoX3VzZXJfaGFzaCI6ImQxZjlmOTIyNmY1NWQyMzRmYTYxNTk5ZGQwMzg0ZjAwNzYwZmY5OGEiLCJfYXV0aF91c2VyX2JhY2tlbmQiOiJkamFuZ28uY29udHJpYi5hdXRoLmJhY2tlbmRzLk1vZGVsQmFja2VuZCIsIl9hdXRoX3VzZXJfaWQiOjJ9	2015-02-11 14:55:44.031021+01
e2k0pukpsfe2a41pdmhxo4o26zxng526	MGM3ZjMwODBiZDc2NzcyMDE0ZjFjYzg4MjkxNGYxNWIxNTJlYTkyOTp7Il9hdXRoX3VzZXJfaGFzaCI6ImQxZjlmOTIyNmY1NWQyMzRmYTYxNTk5ZGQwMzg0ZjAwNzYwZmY5OGEiLCJfYXV0aF91c2VyX2JhY2tlbmQiOiJkamFuZ28uY29udHJpYi5hdXRoLmJhY2tlbmRzLk1vZGVsQmFja2VuZCIsIl9hdXRoX3VzZXJfaWQiOjJ9	2015-02-13 09:11:26.982618+01
ryoavhd97sg9dau2kc2xg46q809dzdu8	YzNiZjQ2ZWFiMmU3ZDdmZjcyYjA0NmFmOGYyZDlkZjBkNTNkZDU5OTp7Il9hdXRoX3VzZXJfYmFja2VuZCI6ImRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kIiwiX2F1dGhfdXNlcl9oYXNoIjoiZDFmOWY5MjI2ZjU1ZDIzNGZhNjE1OTlkZDAzODRmMDA3NjBmZjk4YSIsIl9hdXRoX3VzZXJfaWQiOjIsIl9tZXNzYWdlcyI6IltbXCJfX2pzb25fbWVzc2FnZVwiLDAsMjUsXCJUaGUgcmVxdWVzdCBcXFwiUmVxdWVzdCBvYmplY3RcXFwiIHdhcyBjaGFuZ2VkIHN1Y2Nlc3NmdWxseS5cIl0sW1wiX19qc29uX21lc3NhZ2VcIiwwLDI1LFwiVGhlIHJlcXVlc3QgXFxcIlJlcXVlc3Qgb2JqZWN0XFxcIiB3YXMgY2hhbmdlZCBzdWNjZXNzZnVsbHkuXCJdLFtcIl9fanNvbl9tZXNzYWdlXCIsMCwyNSxcIlRoZSByZXF1ZXN0IFxcXCJSZXF1ZXN0IG9iamVjdFxcXCIgd2FzIGNoYW5nZWQgc3VjY2Vzc2Z1bGx5LlwiXSxbXCJfX2pzb25fbWVzc2FnZVwiLDAsMjUsXCJUaGUgcmVxdWVzdCBcXFwiUmVxdWVzdCBvYmplY3RcXFwiIHdhcyBjaGFuZ2VkIHN1Y2Nlc3NmdWxseS5cIl1dIn0=	2015-01-25 13:48:46.913492+01
wgw17b2clvebc039aklhmba7jl2hrmtg	MDU0NDg1MGY3Mzg3YjllMTZkOTUyNGFjNjliMjNmZmFmNDFlMThjNzp7Il9hdXRoX3VzZXJfaGFzaCI6IjRjMGY2ZjgyMmIzOGNhNThiYjg4OWQ1ZTE1YjI1NGZhYjViYjU4YzEiLCJfYXV0aF91c2VyX2JhY2tlbmQiOiJkamFuZ28uY29udHJpYi5hdXRoLmJhY2tlbmRzLk1vZGVsQmFja2VuZCIsIl9hdXRoX3VzZXJfaWQiOjZ9	2015-01-29 00:47:00.405447+01
ji20zzh0diyxyvs2cg2h2ehfffixzfln	ODg1MTJhMjRlNTJmNzlhZmE1N2FhMjI2OTcwZDAxMGRkZTg5YTE3Nzp7Il9hdXRoX3VzZXJfaGFzaCI6ImQxZjlmOTIyNmY1NWQyMzRmYTYxNTk5ZGQwMzg0ZjAwNzYwZmY5OGEiLCJfYXV0aF91c2VyX2lkIjoyLCJfYXV0aF91c2VyX2JhY2tlbmQiOiJkamFuZ28uY29udHJpYi5hdXRoLmJhY2tlbmRzLk1vZGVsQmFja2VuZCJ9	2015-01-29 16:22:32.13312+01
45ts5sw7dzk74g7igqjhqnapcpax723r	MGM3ZjMwODBiZDc2NzcyMDE0ZjFjYzg4MjkxNGYxNWIxNTJlYTkyOTp7Il9hdXRoX3VzZXJfaGFzaCI6ImQxZjlmOTIyNmY1NWQyMzRmYTYxNTk5ZGQwMzg0ZjAwNzYwZmY5OGEiLCJfYXV0aF91c2VyX2JhY2tlbmQiOiJkamFuZ28uY29udHJpYi5hdXRoLmJhY2tlbmRzLk1vZGVsQmFja2VuZCIsIl9hdXRoX3VzZXJfaWQiOjJ9	2015-02-04 12:31:59.981757+01
617i7nozl4au6wuhwv7uc45vyiq5vku6	MGM3ZjMwODBiZDc2NzcyMDE0ZjFjYzg4MjkxNGYxNWIxNTJlYTkyOTp7Il9hdXRoX3VzZXJfaGFzaCI6ImQxZjlmOTIyNmY1NWQyMzRmYTYxNTk5ZGQwMzg0ZjAwNzYwZmY5OGEiLCJfYXV0aF91c2VyX2JhY2tlbmQiOiJkamFuZ28uY29udHJpYi5hdXRoLmJhY2tlbmRzLk1vZGVsQmFja2VuZCIsIl9hdXRoX3VzZXJfaWQiOjJ9	2015-02-11 14:57:37.958244+01
wlmf69pw55bhygqch1vtmqr062paurdp	MmMzOWQ5ZmFhOTY0NTkyMTZiYWQyNjczOTIwYWZkODUyMDQ3NDM1Mzp7Il9hdXRoX3VzZXJfaGFzaCI6ImQwOGU2OGI4ZTcyYTMwMWNmYjczODY3NmI0MjYzNGU4NDhlOTYxYzEiLCJfYXV0aF91c2VyX2JhY2tlbmQiOiJkamFuZ28uY29udHJpYi5hdXRoLmJhY2tlbmRzLk1vZGVsQmFja2VuZCIsIl9hdXRoX3VzZXJfaWQiOjExfQ==	2015-02-08 23:33:14.007344+01
a7da0uf5z8sparrohuekzy0zzf8a0cey	ZjJjNzExODZiZjc1ZWQ2NjVjYTNiNWQ1YzgwNGM5MWI3Y2NhZmI3MDp7Il9hdXRoX3VzZXJfaGFzaCI6ImQxZjlmOTIyNmY1NWQyMzRmYTYxNTk5ZGQwMzg0ZjAwNzYwZmY5OGEiLCJfbWVzc2FnZXMiOiJbW1wiX19qc29uX21lc3NhZ2VcIiwwLDI1LFwiVGhlIHJlcXVlc3QgXFxcIlJlcXVlc3Qgb2JqZWN0XFxcIiB3YXMgY2hhbmdlZCBzdWNjZXNzZnVsbHkuXCJdLFtcIl9fanNvbl9tZXNzYWdlXCIsMCwyNSxcIlRoZSByZXF1ZXN0IFxcXCJSZXF1ZXN0IG9iamVjdFxcXCIgd2FzIGNoYW5nZWQgc3VjY2Vzc2Z1bGx5LlwiXSxbXCJfX2pzb25fbWVzc2FnZVwiLDAsMjUsXCJUaGUgY29tcGFueSBcXFwieW91XFxcIiB3YXMgY2hhbmdlZCBzdWNjZXNzZnVsbHkuXCJdLFtcIl9fanNvbl9tZXNzYWdlXCIsMCwyNSxcIlRoZSBteSB1c2VyIFxcXCJmdW5reS5naW5AeWFuZGV4LnJ1XFxcIiB3YXMgY2hhbmdlZCBzdWNjZXNzZnVsbHkuXCJdLFtcIl9fanNvbl9tZXNzYWdlXCIsMCwyNSxcIlRoZSBteSB1c2VyIFxcXCJtYXhAZ3JhcGhpY3Nmb3JjYXJzLmNvbS5hdVxcXCIgd2FzIGNoYW5nZWQgc3VjY2Vzc2Z1bGx5LlwiXSxbXCJfX2pzb25fbWVzc2FnZVwiLDAsMjUsXCJUaGUgbXkgdXNlciBcXFwiZ2luLnJhZEBnbWFpbC5jb21cXFwiIHdhcyBjaGFuZ2VkIHN1Y2Nlc3NmdWxseS5cIl0sW1wiX19qc29uX21lc3NhZ2VcIiwwLDI1LFwiVGhlIHJlcXVlc3QgXFxcIlJlcXVlc3Qgb2JqZWN0XFxcIiB3YXMgY2hhbmdlZCBzdWNjZXNzZnVsbHkuXCJdLFtcIl9fanNvbl9tZXNzYWdlXCIsMCwyNSxcIlRoZSByZXF1ZXN0IFxcXCJSZXF1ZXN0IG9iamVjdFxcXCIgd2FzIGNoYW5nZWQgc3VjY2Vzc2Z1bGx5LlwiXSxbXCJfX2pzb25fbWVzc2FnZVwiLDAsMjUsXCJUaGUgbXkgdXNlciBcXFwibWF4QGdyYXBoaWNzZm9yY2Fycy5jb20uYXVcXFwiIHdhcyBjaGFuZ2VkIHN1Y2Nlc3NmdWxseS5cIl0sW1wiX19qc29uX21lc3NhZ2VcIiwwLDI1LFwiVGhlIHJlcXVlc3QgXFxcIlJlcXVlc3Qgb2JqZWN0XFxcIiB3YXMgY2hhbmdlZCBzdWNjZXNzZnVsbHkuXCJdLFtcIl9fanNvbl9tZXNzYWdlXCIsMCwyNSxcIlRoZSByZXF1ZXN0IFxcXCJSZXF1ZXN0IG9iamVjdFxcXCIgd2FzIGNoYW5nZWQgc3VjY2Vzc2Z1bGx5LiBZb3UgbWF5IGVkaXQgaXQgYWdhaW4gYmVsb3cuXCJdXSIsIl9hdXRoX3VzZXJfaWQiOjIsIl9hdXRoX3VzZXJfYmFja2VuZCI6ImRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kIn0=	2015-02-10 22:05:57.663588+01
\.


--
-- Data for Name: office_attachment; Type: TABLE DATA; Schema: public; Owner: stas
--

COPY office_attachment (id, attachment_file, request_id) FROM stdin;
70	uploads/Falling_leaf.jpg	178
71	uploads/Falling_leaf1.jpg	178
75	uploads/Custom_Graphics_GilbertRoach_Isuzu_MuX.pdf	181
76	uploads/photo_3.JPG	181
77	uploads/photo_1.JPG	181
90	uploads/024.JPG	197
91	uploads/025.JPG	197
92	uploads/026.JPG	197
93	uploads/027.JPG	197
94	uploads/March_2013_012.JPG	197
95	uploads/March_2013_015.JPG	197
96	uploads/March_2013_016.JPG	197
97	uploads/espit_oA7H1MW.png	206
98	uploads/icon_fI0LhFv.png	206
99	uploads/IMAG0684.jpg	207
100	uploads/IMAG0685.jpg	207
101	uploads/IMAG0434.jpg	208
102	uploads/CAR_Signage_example.pdf	210
103	uploads/Blue_ute_1.jpg	212
104	uploads/Blue_ute_2.jpg	212
105	uploads/Blue_ute_3.jpg	212
106	uploads/Blue_ute_4.jpg	212
107	uploads/Total_Concept_VW_Caddy_Van_2012.pdf	214
111	uploads/IMG_0341.JPG	225
112	uploads/Custom_Graphics1305_Sydney_Sitelines_JAN28.pdf	226
113	uploads/Custom_Graphics_1241_RWulfse_Toyota_Yaris_JAN22.pdf	227
114	uploads/Audi.jpg	229
115	uploads/184581328155392876_20150130120809389.pdf	231
\.


--
-- Name: office_attachment_id_seq; Type: SEQUENCE SET; Schema: public; Owner: stas
--

SELECT pg_catalog.setval('office_attachment_id_seq', 115, true);


--
-- Data for Name: office_company; Type: TABLE DATA; Schema: public; Owner: stas
--

COPY office_company (id, name, email, phone, category_id, city, website, address, state, post_code, description) FROM stdin;
31	Popology	hello@popology.com.au	1300880746	24	Sydney	www.popology.com.au	Suite 202, 166 Glebe Point Rd Glebe NSW 2037	NSW	2037	Design company , do a lot of  exhibition. suitable for T-board .
34	Sydney Prestige		95503765	28	Sydney	sydneyprestigeautobody.com.au	184 Victoria Rd Marrickville	NSW	2204	Friends of Ilya and Max, Tonny and Chris are the owners.
33	Cochlear implant		+61294286555	27	Sydney	cochlear.com	1 University ave, Macquarie University	NSW	2109	Selling hearing implants etc
117	Rolling Stock	rossgenua@gmail.com	-	56	Sydney	rollingstock.net.au	-	NSW	-	New company for wheels resaling
118	Zombie Cafe	info@zombiecafe.com.au	-	57	Sydney	zombiecafe.com.au	-	NSW		One van business, van selling coffee. Do not have a lot of money.
59	Infinity		0296999179	37	Sydney	infinityproperty.com.au	38, 112-122 Mcevoy st Alexandria	NSW	2015	Real estate , use to work with Signarama, have a flit of 5 cars. 
60	Bingo group		1300424646	38	Sydney	bingogroup.com.au	81 Egerton St Silverwater	NSW		
61	GoGet			39	Sydney	goget.com.au	Glebe	NSW	2037	One of the biggest Car share companies in Australia . Needs a lot of stickers.
120	Esprit Martial Arts	info@espritmartialarts.com.au	8964 9195	68	Sydney	espritmartialarts.com.au	137 Victoria Rd, Marrickville	NSW	2204	Esprit Martial Arts teaches martial arts designed for both adults and children of all ages.
121	Redy2go	info@redy2go.com.au	1300 246 669	69	Sydney	redy2go.com.au	Unit 7 - 25 Ossary Street Mascot	NSW	2020	 Airport Shuttle  Service
122	SAAP Auto	info@saapauto.com.au	95189333	36	Sydney	www.saapauto.com.au	60 Parramatta Rd., Gleb	NSW	2037	Boutique dealership passionate about cars and committed to ensuring complete customer satisfaction - finding the right car for you, at the right price
30	Custom graphics	info@graphicsforcars.com.au		23	Sydney	www.graphicsforcars.com.au	86c Dunning ave Rosebery	NSW	2018	Our bright and shiny company
40	Holy Cow	olivia@holycow.com.au	0292124676	24	Sydeny	holycow.com.au	4/104 Commonwealth St. Surry Hills	NSW	2010	Design company , we sent promo letter , call for an appointment and sent email. 
41	Hooch Creative	hi@hoochcreative.com.au	1300139439	24	Sydney	hoochcreative.com.au	Level 3 Suit 45 330 Wattle st Ultimo	NSW	2007	Design company in the center of Sydney , promo email sent.
39	Regyp	natasha@regyp.com.au	1300473497	31	Sydney	www.regyp.com.au	P O Box 3454 Tamarama	NSW	2026	Recycling and Gypsum Sales
42	Squeeze Creative		0283992824	24	Sydney	squeezecreative.com.au	Studio 112/59 Marlborough St Surry hills	NSW	2010	Received our promo mail , put our information in database. call when have something for us. 
43	Orion Creative		0288502215	24	Sydney	orioncreative.com.au	302, 2-8 Brookhollow ave Baulkham Hills	NSW	2153	No need. Have their own wrapping division. 
45	Bench creative		0280210149	24	Sydney	benchcreative.com.au	302/3 Gladstone St. Newtown 	NSW	2042	Will let us know when will be any orders. Put us in database.
46	Emedia Creative 		1300619437	24	Sydney		91 Watkin St. Newtown 	NSW	2042	Add us on their database, will contact us as soon as they will have anything for us. Contact person Christie.
123	Felipe Santos	guyminha@gmail.com	-	57	-	-	-	-	-	Tradie
124	Gil Douglass	gilian@oxleyhomecare.com.au	99862266	70	Sydbey	www.oxleyhomecare.com.au	19/7 Narabang Way, Belrose	NSW	2085	Small car signage, on both sides and back
125	Halfpriced Grannyflats	info@halfpricedgrannyflats.com.au	98229989	60	Sydney	halfpricedgrannyflats.com.au	4/55-57 Newton rd. Wetherill Park	NSW	2164	Builder
126	Ragdoll Surfboards	ragdollsurfboards@yahoo.com	-	56	-	-	-	NSW	-	Retailer for Surf Board
127	1800EzyShades & 1800ShadeMaster	info@1800ezyshades.com.au	1800399742337	56	Sydney	1800ezyshades.com.au	1/470 Parramatta Road	NSW	2135	Shades Retailer
128	Balcroy Homes	info@balcroy.com.au	0418618292	60	Sydney	balcroy.com.au	Po Box 46 Casula Mall	NSW	2170	Builder & constructtion
129	Rosie Meehan	info@buildridge.com.au	98075307	71		buildridge.com.au				Buildridge Constructions is an established progressive company specializing in architecturally designed homes, development projects and alterations & additions.
106	Buildridge Construction	info@buildridge.com.au	98075307	60	Sydney	http://buildridge.com.au/	PO Box 816 Drummoyne	NSW	1470	Building and Construction
130	Garden Clinic	kent@gardenclinic.com	1300133100	\N	Sydney	gardenclinic.com	83 Beecroft Road, Beecroft	NSW	2119	Garden Solutions
131	NSW State Fleet - Government Services	david.worrell@finance.nsw.gov.au	1800801523	72	Sydney	statefleet.nsw.gov.au	Level 12, 2-24 Rawson Place 	NSW	2000	NSW State Fleet
44	Juuce creative	dave@juuce.com	0299765777	24	Sydney	juuce.com.au	1	NSW	2000	David is a contact person,  promo email sent.
132	Time Realty	leanne@timerealty.com.au	97121188	37	Sydbey	timerealty.com.au	161 Great North Road, Five Dock	NSW	2046	Sale, purchase, leasing and renting of property
65	PROvinyl	info@provinyl.com.au		41	Sydney	provinyl.com.au	86 Dunning ave Rosebery	NSW	2018	Shit COmPANY , DO NOT DEAL WITH IT 
71	Exotic Graphix			45	Melbuorn		2	VIC	3201	Big Signage company
57	Gilbert and Roach	Chris.Bloor@gandr.com.au	0288251014	36	Sydney	gandr.com.au	8	NSW	2148	Big, really big, Division of Autopool pty. ltd.  Trucks ,vans , utes , 4wd .. and more .. one big dealership.  have a chat with Chris Bloor - nice guy , really interested, and quite active. Need to provide quotation for existing vehicle for further cooperation.   
83	Flora and Fauna	info@floraandfauna.com.au	96533625	56	Sydney	floraandfauna.com.au	Flora	NSW	2159	Flora and fauna boutique shop.
104	Gold car wash	danny@graphicsforcars.com.au	-	58	Sydney	-	O'riordan street 	NSW	2015	car wash company constant customer we did for them 2x cars 
101	Total Concept Projects 	sales@totalconceptprojects.com	2 8094 9919	57	Sydeny	totalconceptprojects.com	Unit 26, 4a Bachell Avenue Lidcombe 2141, New South Wales	NSW	2141	Audio and visual system installation.
107	Right Carpet Cleaning	info@rightcarpetcleaning.com.au	1800937247	57	Sydney	rightcarpetcleaning.com.au	1807/348-354 sussex street 	NSW	-	
108	Rayzor	ray@rayzor.net.au	98693664 	47	Sydney	rayzor.net.au	RAYZOR  EPPING, NSW 	NSW	-	Web and Graphic Design Company
109	Blake	blake@kickstartbusinesses.com	0488376999	61	-	kickstartbusinesses.com	-	-	-	Accountant for Footzy
110	Garden Clinic	info@gardenclinic.com	1300133100	62	Sydney	gardenclinic.com	83 Beecroft Road, Beecroft	NSW	2119	Gardening Information
134	Camperdown Fitness	sales@camperdown-fitness.com.au	(02) 8594 2900	73	Sydney 	http://camperdown-fitness.com.au	166-172 Parramatta Rd, Camperdown 	NSW	2058	Fitness guys very good friends of Danny, \r\ndirector Paul \r\nTrainer Alex have BMW M1 we did it has couple of Hummers and mercedes sl for now. \r\n\r\n
135	Pet Carriers	info@boatbuddies.com.au	0403 279 635	76	Sydney 	boatbuddies.com.au	Rushcutters Bay 	NSW	2010	Ken is director of this company he is open to new idea.
136	Sydney Sitelines	info@sydneysitelines.com.au	98164598	60	Sydney	sydneysitelines.com.au	59 Westminster Road, Gladesville	NSW	2111	manage and co-ordinate councils, tenants, body corporate and agents and provide all the specialist trades required to complete your project.
137	Phonesales.com.au	support@phonesales.com	0407021176	78	Sydney	phonesales.com.au	-	NSW	-	Online Phone Sales
138	ChemDry	Info@chemdry.com.au	1800 243 637 	79	Sydney	chemdry.com.au	Randwick	NSW	-	Cleaning Company for Residential and commercial
144	Toyota Sydney		0283031961	80	Sydney	sidneycitytoyota.com.au	824	NSW	2017	
145	Designed Group		047 - 744 - 7555	60	Sydney	designedgroup.com.au	-	NSW	-	Building & construction
146	Paul	paul.art@gmail.com	0412224651	81	Sydney	-	-	NSW	2018	Graphics
147	wqe	ceo@4-com.pro	wqe	55	qwe	qwe	qweqwe	qwe	qwe	qweqweqw
148	qweqwe		qwe	82		wqe				
\.


--
-- Name: office_company_id_seq; Type: SEQUENCE SET; Schema: public; Owner: stas
--

SELECT pg_catalog.setval('office_company_id_seq', 148, true);


--
-- Data for Name: office_companycategory; Type: TABLE DATA; Schema: public; Owner: stas
--

COPY office_companycategory (id, title) FROM stdin;
23	Printing company
24	Design
27	Healthcare
28	Auto Workshop
31	Recycling & Gypsum Sales
36	Car dealers
37	Realestate
38	Waste management 
39	Car rentals
41	Car Wraps
45	Vehicle signage
47	Designs
48	Printing company22
49	Designdsd
50	Printing company
52	Design
54	Beauty 
55	Boutique
56	Retail shop
57	Tradies
58	Car wash
59	Financial 
60	Builder
61	Accountant
62	Gardening
64	fddfgdfg
65	car dealers
66	Sign Maker
68	Martial Art
69	Taxi
70	Nursing Home
71	Construction
72	Government
73	Fitness
76	Boats
77	Online Sales
78	Online Phone Sales
79	Cleaning
80	Car dealer
81	Employee
82	qwe
\.


--
-- Name: office_companycategory_id_seq; Type: SEQUENCE SET; Schema: public; Owner: stas
--

SELECT pg_catalog.setval('office_companycategory_id_seq', 82, true);


--
-- Data for Name: office_contactperson; Type: TABLE DATA; Schema: public; Owner: stas
--

COPY office_contactperson (id, company_id, name, email, phone, "position") FROM stdin;
60	101				
61	101	Anthony Musico	antohny_m1996@hotmail.com	0434736493	Manager
62	104				
63	104	Mahmood	goldcarwashcafe@yahoo.com	96999007	manager
67	106				
68	106	Rosie Meehan	rosie@buildridge.com.au	0410663090	Secretary
69	107				
70	107	Sam Puthan	info@rightcarpetcleaning.com.au	0401883333	Owner
71	108				
72	108	Ray Sharpe	ray@rayzor.net.au	0418861547	Owner
73	109				
74	109	Blake	blake@kickstartbusinesses.com		Owner
75	110				
76	101	ddd	danny@graphicsforcars.com.au	23423423234	ddd
80	117	Ross Genua	rossgenua@gmail.com	0421337677	Manager
81	118				
82	120				
83	120	John Cooksey	john@espiritmartialarts.com.au	-	Owner
84	121				
85	121	Peter Ray	pbray@redy2go.com.au	-	Driver
86	122				
87	122	Christian	christian@saapauto.com.au	0405817775	Manager
88	123				
26	30	Danny Vinkl	danny@graphicsforcars.com.au		CEO
89	123	Felipe	guyminha@gmail.com	-	Owner
29	39	Natasha	natasha@regyp.com.au	0416005865	Manager
30	40	Olivia			Manager
31	42	Jen			
90	124	Gil Douglass	gilian@oxleyhomecare.com.au	99862266	Manager
91	125				
92	125	Slaven Markovic	slaven@halfpricedgrannyflats.com.au	0402698 015	Director
93	126				
94	126	Simon Sheppard 	ragdollsurfboards@yahoo.com	-	Owner
95	127				
96	128				
39	44	George Ihring	gi@juuce.com		Creative Director
97	127	Paul Tobin	paul@1800ezyshades.com.au	0498 119452	Manager
98	129				
99	128	Don Elder	Info@balcroy.com.au	0418618292	Director
100	101	Anthony Musico	anthony_m1996@hotmail.com	0434736493	Manager
101	130				
46	57	Chris Bloor 	chris.bloor@gandr.com.au	0400401806	sales manager
102	127	Kent Ross	kent@gardenclinic.com	0407454227	Managing Director
103	131				
104	127	David Worrell	david.worrell@finance.nsw.gov.au	1800801523	Repairs Controller
50	71	Nick Caniniti	nick@exoticgraphix.com.au	0397750534	Owner
105	132				
52	65	Mark	info@provinyl.com.au		Manager
106	134	Alex Love 	bondistrengthco@gmail.com	+61410757620	Trainer 
107	135	Ken Heikkinen	ken@boatbuddies.com.au	+61433440349	CEO
108	136				
109	136	Warwick Knight	sydneysitelines@optusnet.com.au	0418686601	Owner
110	137				
59	83	Tom Abraham	tom@floraandfauna.com.au		Owner
111	137	Rich Wulfse	wulfse@gmail.com	0407021176	Owner
112	138				
113	138	James McKeon	jamesmckeon1978@yahoo.com.au	0410603168	Manager
114	144				
115	144	Naomi Menon		0415889366	Marketing manager
116	145				
117	145	PETER ADAMS	peter@designedcommercial.com.au	0477447555	Owner
118	146				
119	146	Paul	paul.art@gmail.com	0412224651	Manager
120	147	qweqwe	ceo@4-com.pro	qwe	wqe
121	148				
122	148				
\.


--
-- Name: office_contactperson_id_seq; Type: SEQUENCE SET; Schema: public; Owner: stas
--

SELECT pg_catalog.setval('office_contactperson_id_seq', 122, true);


--
-- Data for Name: office_customertype; Type: TABLE DATA; Schema: public; Owner: stas
--

COPY office_customertype (id, title, index_sort, material, tisk) FROM stdin;
1	GROUP 1 	1	1.80000000000000004	3
2	GROUP 2 	2	1.69999999999999996	2.70000000000000018
3	GROUP 3 	3	1.60000000000000009	2.5
4	GROUP 4 	4	1.5	2.20000000000000018
5	GROUP 5	5	1.19999999999999996	2
6	GROUP 6	6	1.10000000000000009	1.80000000000000004
7	GROUP 7	7	1.10000000000000009	1.60000000000000009
8	GROUP 8	8	1.10000000000000009	1.39999999999999991
\.


--
-- Name: office_customertype_id_seq; Type: SEQUENCE SET; Schema: public; Owner: stas
--

SELECT pg_catalog.setval('office_customertype_id_seq', 8, true);


--
-- Data for Name: office_lamination; Type: TABLE DATA; Schema: public; Owner: stas
--

COPY office_lamination (id, title, index_sort, value) FROM stdin;
1	NO	1	0
6	SMGL-/DOL2000/	6	7
7	SMGL-/DOL2100/	7	7
8	SMGL-/DOL 3000 overlaminate/	8	4
9	SMML137-/DOL 3100 overlaminate/	9	3
10	One Way Lam 	10	29
3	CWML152-/DOL 1480/	3	18
4	CWGL137-/DOL 1360/	4	16
5	CWML137-/DOL 1380/	5	16
2	CWGL152-/DOL 1460/	2	18
\.


--
-- Name: office_lamination_id_seq; Type: SEQUENCE SET; Schema: public; Owner: stas
--

SELECT pg_catalog.setval('office_lamination_id_seq', 10, true);


--
-- Data for Name: office_material; Type: TABLE DATA; Schema: public; Owner: stas
--

COPY office_material (id, title, index_sort, value) FROM stdin;
4	NO	1	0
6	MPI 2105EA	3	9
7	MPI 2000GW	4	8
8	MPI 2120MW	5	9
9	MPI 2040GC	6	7
10	MPI 3000PP	7	4
11	MPI 3041PP	8	5
12	SMBG440-/MPI4130/	9	3
13	SMBM440-/MPI4230/	10	3
14	SMBM680-/MPI4330/	11	6
15	SMP-/IP 2405/	12	3
16	SMP-/IP 2408/	13	4
17	One Way Vision Auto View	14	26
18	MPI 2611	15	14
19	EasyPrint 1702	16	37
20	15 AUD	18	15
21	20 AUD	19	20
22	25 AUD	20	25
23	30 AUD	21	30
24	35 AUD	22	35
25	40 AUD	23	40
26	45 AUD	24	45
27	50 AUD	25	50
5	CWF1-\\MPI 1005SC\\	2	20
\.


--
-- Name: office_material_id_seq; Type: SEQUENCE SET; Schema: public; Owner: stas
--

SELECT pg_catalog.setval('office_material_id_seq', 27, true);


--
-- Data for Name: office_myuser; Type: TABLE DATA; Schema: public; Owner: stas
--

COPY office_myuser (id, password, last_login, email, date_of_birth, is_active, is_admin, name, phone, group_id) FROM stdin;
1	pbkdf2_sha256$12000$BmRBfEe4CZ1P$kjGelcQSQw1ScwMwK297/sa5WJBJbbsmNC1sK4+Vf7E=	2014-12-23 12:13:03.128674+01	asdasds@mail.ru	1990-02-17	t	t	stas revas	0990817424	\N
4	pbkdf2_sha256$12000$2slr8cVJuyUI$+34FPGZFivyqx343/Yq+SvIhV25HwQDx6UXtu7l+WIE=	2014-12-26 01:04:22.922556+01	sales@4-com.pro	2014-12-26	t	f	stas revas	0990817424	\N
7	pbkdf2_sha256$12000$UYV1LRH0LKXl$5r8jLTuE7x1FlLWs6hDZikQcGsZMAH0ywNIGDViF9vg=	2015-01-09 09:47:14.333685+01	reva@revasik.com	2015-01-09	t	f	stas revas	0990817424	\N
14	pbkdf2_sha256$12000$mg9L0K304Gs8$CtLpeL6rEVADuifQ4bD7aHWhrfYssCDnp0WiZ8oGBdI=	2015-01-26 23:52:51.965113+01	lukas@graphicsforcars.com.au	2015-01-12	t	f	Lukas	0487145759	6
18	pbkdf2_sha256$12000$haccCdRgB5en$t7ZX4jWnmKGDVwxlSsX86ZS64RK8+UWWEJkRaMfmFNc=	2015-01-30 02:56:01.302239+01	vinklpetr@gmail.com	2015-01-20	t	f	Peter	0293137056	2
5	pbkdf2_sha256$12000$mCRA8uOudXvn$mdj/io858smUJKqcRDDUyREL+JZScb+0Nx2GEmLrbEM=	2015-01-21 10:51:46.160819+01	gin.rad@gmail.com	2014-12-26	t	t	Roman Radin	0667554639	7
15	pbkdf2_sha256$12000$CWXIFcC6JIFx$lE8abky5ancaLJ1a0b3WI5VqHIDbSswK24i7Epjn9Z4=	2015-01-26 23:55:05.431079+01	george@graphicsforcars.com.au	2015-01-19	t	f	George	0424648940	5
10	pbkdf2_sha256$12000$bVQrONwuPmOB$JGv0ZIMI9CdsrTuWzYMUr6Pd4ACvXJ+AStRKYgFyuzo=	2015-01-15 10:58:38.164329+01	funky.gin@yandex.ru	2009-01-13	t	f	funky	gin	7
3	pbkdf2_sha256$12000$VKOHZQH7e0rT$cqNElFrUDwaMvf02NIot7cmHRZelzZedKOLn3GwLLNc=	2015-01-10 17:04:13.894984+01	ceo@4-com.pro	1989-09-21	t	t	stas revas	0990817424	2
2	pbkdf2_sha256$12000$PYqfi6Ndm39p$SvalhO3F/Cp3Svvb68fxD7cNEEVV+2lX3oXo9Si2FM8=	2015-01-30 12:26:20.530018+01	revastanislav@gmail.com	1990-02-17	t	t	stas revas	0990817424	7
16	pbkdf2_sha256$12000$dEuua3AJJfVP$Oy4b5X+sw9a/A8v936r36l2Uflwq+eormPbUGHJSvwc=	2015-01-27 04:36:06.90129+01	meegan@graphicsforcars.com.au	2015-01-14	t	f	Meegan	0424648940	4
11	pbkdf2_sha256$12000$zpbtNRpLTwVS$QGWi+T7JwOW4mgQdEJiHyuvib1wbfB+v7Ca/uxfvPyU=	2015-01-29 00:00:11.666659+01	danny@graphicsforcars.com.au	1987-08-10	t	t	Danny	Vinkl	7
12	pbkdf2_sha256$12000$1Frq0TSG5oJn$X1eQP8TMQ2aEHNHF/0OVRY0+UF5a1fb5V5p5R0P8rFg=	2015-01-29 23:25:38.663995+01	paul@graphicsforcars.com.au	2015-01-14	t	t	Paul Pullukaran	969 709 53	7
6	pbkdf2_sha256$12000$u3N1aU38xWcz$pyU12JET7XrPPU/RHKki45/Tm/UNqJ5ItExexg7cmEk=	2015-01-30 00:18:12.512816+01	max@graphicsforcars.com.au	2014-12-29	t	t	Max	0990817424	7
17	pbkdf2_sha256$12000$eSBjVKwE2gEk$LP6lTWwZOb0xRag0UrNf40qYSBqvhs6wbUiRKgxEzPM=	2015-01-30 01:36:42.55997+01	rene@graphicsforcars.com.au	2015-01-15	t	f	Rene	CustomGraphics	2
\.


--
-- Name: office_myuser_id_seq; Type: SEQUENCE SET; Schema: public; Owner: stas
--

SELECT pg_catalog.setval('office_myuser_id_seq', 18, true);


--
-- Data for Name: office_organizer; Type: TABLE DATA; Schema: public; Owner: stas
--

COPY office_organizer (id, text, date, user_id) FROM stdin;
52	have to go to van man 	2015-01-23 13:00:00+01	11
53	Hooch creative, meeting. 3 Suite 45 330 Wattle st	2015-01-22 09:00:00+01	6
54	Hooch creative, meeting. 3 Suite 45 330 Wattle st	2015-01-23 09:00:00+01	6
55	Juuce creative . 12 pm meeting	2015-01-23 13:00:00+01	6
56	Quotation on Gilbert and roach	2015-01-23 10:00:00+01	6
57	Paul call to Budget to Rebeca Greenwood marketing manager 	2015-01-23 11:00:00+01	6
58	Call to arrange the meeting - GoGet	2015-01-26 10:00:00+01	6
\.


--
-- Name: office_organizer_id_seq; Type: SEQUENCE SET; Schema: public; Owner: stas
--

SELECT pg_catalog.setval('office_organizer_id_seq', 58, true);


--
-- Data for Name: office_plot; Type: TABLE DATA; Schema: public; Owner: stas
--

COPY office_plot (id, title, index_sort, value) FROM stdin;
1	YES	1	1
2	NO	2	0
\.


--
-- Name: office_plot_id_seq; Type: SEQUENCE SET; Schema: public; Owner: stas
--

SELECT pg_catalog.setval('office_plot_id_seq', 2, true);


--
-- Data for Name: office_plotpricesam; Type: TABLE DATA; Schema: public; Owner: stas
--

COPY office_plotpricesam (id, title, index_sort, value) FROM stdin;
1	0	1	0
2	5	5	5
3	10	10	10
4	15	15	15
5	20	20	20
6	25	25	25
7	30	30	30
8	35	35	35
9	40	40	40
10	45	45	45
11	50	50	50
12	55	55	55
13	60	60	60
14	65	65	65
15	70	70	70
16	75	75	75
17	80	80	80
\.


--
-- Name: office_plotpricesam_id_seq; Type: SEQUENCE SET; Schema: public; Owner: stas
--

SELECT pg_catalog.setval('office_plotpricesam_id_seq', 17, true);


--
-- Data for Name: office_print; Type: TABLE DATA; Schema: public; Owner: stas
--

COPY office_print (id, title, index_sort, value) FROM stdin;
2	NO	2	0
1	YES	1	12
\.


--
-- Name: office_print_id_seq; Type: SEQUENCE SET; Schema: public; Owner: stas
--

SELECT pg_catalog.setval('office_print_id_seq', 2, true);


--
-- Data for Name: office_request; Type: TABLE DATA; Schema: public; Owner: stas
--

COPY office_request (id, contact_person_id, company_id, order_details, price, payment, sales_manager_id, office_manager_id, graphics_user_id, graphics_description, graphics_date_start, graphics_date_end, installation_user_id, installation_description, installation_date_start, installation_date_end, product_user_id, product_description, product_date_start, product_date_end, status, sending_date, calculation_json, graphics_min, installation_min, product_min, graphics_finish, installation_finish, product_finish, quotation_declined, sent_invoice) FROM stdin;
185	30	40	cvbcfb	\N	f	5	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	3	2015-01-26 09:32:49.674493+01	\N	\N	\N	\N	f	f	f	wefsdfgsfsdf	f
192	68	106	Vehicles sign wrapped \r\n2x Ford Rangers and 1x Isuzu Tipper.\r\n\r\nDesign Needed	\N	f	12	\N	\N		\N	\N	\N		\N	\N	\N		\N	\N	1	2015-01-27 04:18:14+01	{"sam":{"total_price":"5689.80","expences":6258.780000000001},"note":"zvzxvzxvzxvzxv","position":[{"description":"sdfsfwefwefwef","material":"MPI 2105EA","lamination":"CWML152-/DOL 1480/","print":"YES","plot":"YES","qty":"3","x":"4","y":"5","total_m2":"60.00","costumer_type":"GROUP 1 ","material_price":"9.0","lamination_price":"18.0","print_price":"12.0","plot_price":"600.00","mat.ad.value":"1.8","tisk":"3.0","total_price":"5676.00","graphics_time":"3","production_time":"4","item_price":"5689.80","graphics_price":"4.80","production_price":"4.00","installation_price":"5.00"}]}	\N	\N	\N	f	f	f		f
202	26	45	rrrrrrrrrrrrrrrrrr	\N	f	2	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	3	2015-01-28 08:39:10.054332+01	{"sam":{"total_price":"2181.00","expences":"720.00","price_incl_gstl":2399.1},"note":"ffjfjghghjghj","position":[{"description":"jhhgjghjghj","material":"NO","lamination":"NO","print":"YES","plot":"YES","plot_price_sqm":"0","qty":"3","x":"4","y":"5","total_m2":"60.00","costumer_type":"GROUP 1 ","material_price":"0.0","lamination_price":"0.0","print_price":"12.0","plot_price":"0.00","mat_ad_value":"1.8","tisk":"3.0","total_price":"2160.00","graphics_time":"5","production_time":"6","installation_time":"7","item_price":"2181.00","graphics_price":"8.00","production_price":"6.00","installation_price":"7.00"}]}	\N	\N	\N	f	f	f	sdfdsfsdfsdf	f
201	52	65	blue chrome lamination 	\N	f	11	\N	\N		\N	\N	11		2015-01-28 02:18:00+01	2015-01-28 02:18:00+01	\N		\N	\N	8	2015-01-28 02:13:10+01	{"sam":{"total_price":"252.46","expences":"160.38","price_incl_gstl":277.706},"note":"chrome lamination ","position":[{"description":"chrome lamination","material":"NO","lamination":"CWGL152-/DOL 1460/","print":"NO","plot":"NO","plot_price_sqm":"0","qty":"1","x":"6.5","y":"1.37","total_m2":"8.91","costumer_type":"GROUP 5","material_price":"0.0","lamination_price":"18.0","print_price":"0.0","plot_price":"0.00","mat_ad_value":"1.2","tisk":"2.0","total_price":"192.46","graphics_time":"","production_time":"","installation_time":"60","item_price":"252.46","graphics_price":"0","production_price":"0","installation_price":"60.00"}]}	\N	40	\N	f	f	f		t
198	76	101	door sticker 	\N	t	11	\N	11	None	2015-01-27 08:32:00+01	2015-01-27 08:32:00+01	11	None	2015-01-27 08:32:00+01	2015-01-27 08:32:00+01	11	None	2015-01-27 08:32:00+01	2015-01-27 08:32:00+01	9	2015-01-27 08:30:49.221385+01	{"sam":{"total_price":"100.00","expences":110},"note":"sticker on the door ","position":[{"description":"door sticker ","material":"CWF1-\\\\MPI 1005SC\\\\","lamination":"CWGL152-/DOL 1460/","print":"YES","plot":"NO","qty":"1","x":"1.37","y":"1","total_m2":"1.37","item_price":"100.00","graphics_price":"0","production_price":"0","installation_price":"4.65"}]}	0	0	0	f	f	f	\N	t
227	111	137	Vinyl Stickers on both side doors and on top of the rear view window	\N	f	12	\N	16	pls do the design asap 	2015-01-30 02:27:00+01	2015-01-30 06:27:00+01	\N	instalation til 31.1.2015	2015-01-31 02:27:00+01	2015-01-31 04:27:00+01	\N	nedd to be print and cut 	2015-01-30 06:27:00+01	2015-02-02 01:27:00+01	6	2015-01-30 02:07:16.806175+01	{"sam":{"total_price":"675.41","expences":"320.58","price_incl_gstl":742.951},"note":"2x sides stickers print and cut and one way vision on the back. ","position":[{"description":"one way vision ","material":"One Way Vision Auto View","lamination":"One Way Lam ","print":"YES","plot":"NO","plot_price_sqm":"0","qty":"1","x":"1.37","y":"2","total_m2":"2.74","costumer_type":"GROUP 4 ","material_price":"26.0","lamination_price":"29.0","print_price":"12.0","plot_price":"0.00","mat_ad_value":"1.5","tisk":"2.2","total_price":"298.39","graphics_time":"","production_time":"","installation_time":"50","item_price":"348.39","graphics_price":"0","production_price":"0","installation_price":"50.00"},{"description":"stickers on the sides ","material":"CWF1-\\\\MPI 1005SC\\\\","lamination":"CWGL152-/DOL 1460/","print":"YES","plot":"YES","plot_price_sqm":"25","qty":"1","x":"1.37","y":"2","total_m2":"2.74","costumer_type":"GROUP 4 ","material_price":"20.0","lamination_price":"18.0","print_price":"12.0","plot_price":"68.50","mat_ad_value":"1.5","tisk":"2.2","total_price":"297.02","graphics_time":"","production_time":"","installation_time":"30","item_price":"327.02","graphics_price":"0","production_price":"0","installation_price":"30.00"}]}	240	120	240	f	f	f	\N	f
178	39	44	Quotation to beat. it is a sample quatation for design company , i need it before i'll go there. All information please see below:\r\n\r\nThe ute has, the two sides of the cab + 3 panel on the tray\r\nThe box trailer has 4 panels. 7x5 feet. 900mm height.\r\n\r\nQuote with  supplying the boards. \r\n\r\nImportant just print quotation do not send it.	\N	f	6	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	2	2015-01-22 05:32:19.655447+01	{"sam":{"total_price":"2165.10","expences":"2381.61","price_incl_gstl":2381.6099999999997},"note":"","position":[{"description":"  alu pannels ","material":"50 AUD","lamination":"NO","print":"NO","plot":"NO","plot_price_sqm":"","qty":"1","x":"1","y":"14.5","total_m2":"14.50","costumer_type":"","material_price":"","lamination_price":"","print_price":"","plot_price":"","mat_ad_value":"","tisk":"","total_price":"","graphics_time":"","production_time":"","installation_time":"","item_price":"1037.50","graphics_price":"0","production_price":"0","installation_price":"240.00"},{"description":"  stickers for alu pannels ","material":"MPI 2105EA","lamination":"SMGL-/DOL2000/","print":"YES","plot":"NO","plot_price_sqm":"","qty":"1","x":"1","y":"14.5","total_m2":"14.50","costumer_type":"","material_price":"","lamination_price":"","print_price":"","plot_price":"","mat_ad_value":"","tisk":"","total_price":"","graphics_time":"","production_time":"","installation_time":"","item_price":"688.40","graphics_price":"0","production_price":"0","installation_price":"120.00"},{"description":"  both sides hilux signage ","material":"CWF1-\\\\MPI 1005SC\\\\","lamination":"CWGL152-/DOL 1460/","print":"YES","plot":"YES","plot_price_sqm":"","qty":"1","x":"1.37","y":"2","total_m2":"2.74","costumer_type":"","material_price":"","lamination_price":"","print_price":"","plot_price":"","mat_ad_value":"","tisk":"","total_price":"","graphics_time":"","production_time":"","installation_time":"","item_price":"439.20","graphics_price":"0","production_price":"0","installation_price":"180.00"}]}	\N	\N	\N	f	f	f	\N	f
203	30	45	ukguig	\N	f	2	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	3	2015-01-28 10:14:37.061039+01	{"sam":{"total_price":"0","expences":"0","price_incl_gstl":0},"note":"","position":[{"description":"","material":"NO","lamination":"NO","print":"YES","plot":"YES","plot_price_sqm":"0","qty":"","x":"","y":"","total_m2":"","costumer_type":"GROUP 1 ","material_price":"0.0","lamination_price":"0.0","print_price":"12.0","plot_price":"","mat_ad_value":"1.8","tisk":"3.0","total_price":"","graphics_time":"","production_time":"","installation_time":"","item_price":"","graphics_price":"0","production_price":"0","installation_price":"0"}]}	\N	\N	\N	f	f	f	hjrfjkryj	f
189	60	101	Visuals for two sides of the Caddy, One-Way Vision for the Back\r\n\r\nStickers on the Door	\N	f	12	\N	11	done	2015-01-27 04:57:00+01	2015-01-27 03:57:00+01	11	danny go ahed 	2015-01-27 04:37:00+01	2015-01-27 04:57:00+01	11	instal	2015-01-27 03:37:00+01	2015-01-27 04:37:00+01	3	2015-01-27 02:18:27.063455+01	{"sam":{"total_price":"2167.80","expences":2384.5800000000004},"note":"","position":[{"description":"drgergergergerg","material":"MPI 2105EA","lamination":"CWML152-/DOL 1480/","print":"YES","plot":"YES","qty":"2","x":"3","y":"4","total_m2":"24.00","item_price":"2167.80","graphics_price":"6.40","production_price":"5.00","installation_price":"6.00"}]}	20	20	60	f	f	f	done	t
181	46	57	Need quotation according attached pictures.  Have to use two different vinyls ,  white , and transparent. Doors wraped with solid material , and sides of the artwork wrapped with see through material. \r\nThis is sample of existing wrap , provided to us as  test of out price, so please pay more attention to get best results \r\n\r\nDo not send quotation , print it 	\N	f	6	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	3	2015-01-22 23:27:36.497088+01	{"sam":{"total_price":"1214.92","expences":1336.412},"note":"Isuzu partial signage according the artwork.","position":[{"description":"doors signage ","material":"CWF1-\\\\MPI 1005SC\\\\","lamination":"CWGL152-/DOL 1460/","print":"YES","plot":"NO","qty":"1","x":"1.37","y":"5","total_m2":"6.85","item_price":"794.29","graphics_price":"0","production_price":"0","installation_price":"360.00"},{"description":"gradient on sedes ","material":"15 AUD","lamination":"CWGL152-/DOL 1460/","print":"YES","plot":"NO","qty":"1","x":"1.37","y":"2","total_m2":"2.74","item_price":"358.65","graphics_price":"0","production_price":"0","installation_price":"200.00"},{"description":"back signage ","material":"25 AUD","lamination":"NO","print":"NO","plot":"YES","qty":"1","x":"0.62","y":"1.6","total_m2":"0.99","item_price":"61.98","graphics_price":"0","production_price":"0","installation_price":"10.00"}]}	\N	\N	\N	f	f	f	expensiv	f
226	109	136	signage to my 2014  VW Transporter, Vinyl on Side and One-way vision at the back	\N	f	12	\N	16	None	2015-01-30 02:09:00+01	2015-01-30 04:09:00+01	\N	need to be install till 31/1/2015. \n	2015-01-31 02:09:00+01	2015-01-31 06:09:00+01	\N	None	2015-01-30 02:09:00+01	2015-01-30 06:09:00+01	6	2015-01-30 01:59:33.290134+01	{"sam":{"total_price":"850.11","expences":"370.99","price_incl_gstl":935.121},"note":"VW Transporter vehicle signage both sides and one way according the art work.\\n","position":[{"description":"one way vision ","material":"One Way Vision Auto View","lamination":"One Way Lam ","print":"YES","plot":"NO","plot_price_sqm":"0","qty":"1","x":"1.37","y":"1.8","total_m2":"2.47","costumer_type":"GROUP 6","material_price":"26.0","lamination_price":"29.0","print_price":"12.0","plot_price":"0.00","mat_ad_value":"1.1","tisk":"1.8","total_price":"202.79","graphics_time":"","production_time":"","installation_time":"50","item_price":"252.79","graphics_price":"0","production_price":"0","installation_price":"50.00"},{"description":"vehicle signage ","material":"CWF1-\\\\MPI 1005SC\\\\","lamination":"CWGL152-/DOL 1460/","print":"YES","plot":"YES","plot_price_sqm":"25","qty":"1","x":"1.37","y":"3","total_m2":"4.11","costumer_type":"GROUP 6","material_price":"20.0","lamination_price":"18.0","print_price":"12.0","plot_price":"102.75","mat_ad_value":"1.1","tisk":"1.8","total_price":"363.32","graphics_time":"","production_time":"","installation_time":"234","item_price":"597.32","graphics_price":"0","production_price":"0","installation_price":"234.00"}]}	120	240	240	f	f	f	\N	f
211	94	126	id like my logo to go from the bottom of passenger/drivers door on an angle going up to the top of my back window, i drive a subaru forester 01 model...it doesn't need to have the character on it , just the wording.	\N	f	12	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	2	2015-01-29 01:55:33.494971+01	{"sam":{"total_price":"521.08","expences":"150.50","price_incl_gstl":573.1880000000001},"note":"Vehicle signage : from the bottom of passenger/drivers door on an angle going up to the top of my back window.  LOGO ","position":[{"description":"vehicle signage.","material":"CWF1-\\\\MPI 1005SC\\\\","lamination":"CWGL152-/DOL 1460/","print":"YES","plot":"YES","plot_price_sqm":"25","qty":"1","x":"1.37","y":"2.2","total_m2":"3.01","costumer_type":"GROUP 6","material_price":"20.0","lamination_price":"18.0","print_price":"12.0","plot_price":"75.25","mat_ad_value":"1.1","tisk":"1.8","total_price":"266.08","graphics_time":"","production_time":"","installation_time":"255","item_price":"521.08","graphics_price":"0","production_price":"0","installation_price":"255.00"}]}	\N	\N	\N	f	f	f	\N	f
228	113	138	Printed Graphics on Front Side and Back of a Hundai I-Load	\N	f	12	\N	\N	design have to be done 4.2.2015	2015-02-05 02:33:00+01	2015-02-03 02:33:00+01	\N	customer will bring the car 9.2.2015	2015-02-09 02:32:00+01	2015-02-09 02:33:00+01	\N	None	2015-02-05 02:32:00+01	2015-02-05 02:33:00+01	5	2015-01-30 02:21:06.744442+01	{"sam":{"total_price":"1964.87","expences":"997.16","price_incl_gstl":2161.357},"note":"iload partial wrap +back one way vision ","position":[{"description":"one way vision ","material":"One Way Vision Auto View","lamination":"One Way Lam ","print":"YES","plot":"NO","plot_price_sqm":"0","qty":"1","x":"1.37","y":"4","total_m2":"5.48","costumer_type":"GROUP 6","material_price":"26.0","lamination_price":"29.0","print_price":"12.0","plot_price":"0.00","mat_ad_value":"1.1","tisk":"1.8","total_price":"449.91","graphics_time":"","production_time":"","installation_time":"","item_price":"449.91","graphics_price":"0","production_price":"0","installation_price":"0"},{"description":"car signage ","material":"CWF1-\\\\MPI 1005SC\\\\","lamination":"CWGL152-/DOL 1460/","print":"YES","plot":"NO","plot_price_sqm":"0","qty":"1","x":"1.37","y":"9.2","total_m2":"12.60","costumer_type":"GROUP 5","material_price":"20.0","lamination_price":"18.0","print_price":"12.0","plot_price":"0.00","mat_ad_value":"1.2","tisk":"2.0","total_price":"876.96","graphics_time":"","production_time":"","installation_time":"638","item_price":"1514.96","graphics_price":"0","production_price":"0","installation_price":"638.00"}]}	1	1	1	f	f	f	\N	f
209	89	123	One Way Vision for Holden Captiva PLUS 2 side door signs	\N	f	12	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	2	2015-01-29 00:56:58.828919+01	{"sam":{"total_price":"505.49","expences":"235.73","price_incl_gstl":556.039},"note":"One Way Vision for Holden Captiva PLUS 2 side door signs\\n","position":[{"description":"   one way vision ","material":"One Way Vision Auto View","lamination":"One Way Lam ","print":"YES","plot":"NO","plot_price_sqm":"0","qty":"1","x":"1.37","y":"1.6","total_m2":"2.19","costumer_type":"GROUP 5","material_price":"26.0","lamination_price":"29.0","print_price":"12.0","plot_price":"0.00","mat_ad_value":"1.2","tisk":"2.0","total_price":"197.10","graphics_time":"","production_time":"","installation_time":"50","item_price":"247.10","graphics_price":"0","production_price":"0","installation_price":"50.00"},{"description":"   doors signage ","material":"CWF1-\\\\MPI 1005SC\\\\","lamination":"CWGL152-/DOL 1460/","print":"YES","plot":"YES","plot_price_sqm":"25","qty":"1","x":"1.3","y":"1.37","total_m2":"1.78","costumer_type":"GROUP 5","material_price":"20.0","lamination_price":"18.0","print_price":"12.0","plot_price":"44.50","mat_ad_value":"1.2","tisk":"2.0","total_price":"168.39","graphics_time":"","production_time":"","installation_time":"90","item_price":"258.39","graphics_price":"0","production_price":"0","installation_price":"90.00"}]}	\N	\N	\N	f	f	f	\N	f
218	106	134	Audi a5 full wrap change the colour. 	\N	f	11	\N	\N		\N	\N	\N		\N	\N	\N		\N	\N	2	2015-01-29 23:35:03+01	{"sam":{"total_price":"3300.00","expences":"0.00","price_incl_gstl":3630},"note":"Audi a5 full wrap change the colour.","position":[{"description":" a5 full wrap","material":"NO","lamination":"NO","print":"YES","plot":"YES","plot_price_sqm":"","qty":"1","x":"","y":"","total_m2":"0.00","costumer_type":"GROUP 1 ","material_price":"0.0","lamination_price":"0.0","print_price":"12.0","plot_price":"0.00","mat_ad_value":"","tisk":"3.0","total_price":"0.00","graphics_time":"","production_time":"","installation_time":"3300","item_price":"3300.00","graphics_price":"0","production_price":"0","installation_price":"3300.00"}]}	\N	\N	\N	f	f	f		f
193	52	65	Sticker for Avantodor\r\n\r\n415mm x  30mm	\N	f	12	\N	16	None	2015-01-27 06:35:00+01	2015-01-27 06:55:00+01	11	None	2015-01-27 06:55:00+01	2015-01-27 06:56:00+01	11	None	2015-01-27 06:55:00+01	2015-01-27 06:55:00+01	8	2015-01-27 05:10:43.147469+01	{"sam":{"total_price":"66.75","expences":73.425},"note":"AVENTADOR custom  stickers  and design ","position":[{"description":"2x custom stickers ","material":"25 AUD","lamination":"NO","print":"NO","plot":"YES","qty":"1","x":"0.5","y":"0.5","total_m2":"0.25","item_price":"66.75","graphics_price":"48.00","production_price":"5.00","installation_price":"0"}]}	20	1	20	f	f	f	\N	t
233	46	57	need new cheaper quotation based 181. 	\N	f	11	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	2	2015-01-30 04:48:42.563236+01	{"sam":{"total_price":"1029.65","expences":"488.30","price_incl_gstl":1132.615},"note":"Isuzu partial signage according the artwork.","position":[{"description":"doors signage","material":"CWF1-\\\\MPI 1005SC\\\\","lamination":"CWGL152-/DOL 1460/","print":"YES","plot":"YES","plot_price_sqm":"0","qty":"1","x":"1.37","y":"5","total_m2":"6.85","costumer_type":"GROUP 8","material_price":"20.0","lamination_price":"18.0","print_price":"12.0","plot_price":"0.00","mat_ad_value":"1.1","tisk":"1.4","total_price":"401.41","graphics_time":"","production_time":"","installation_time":"300","item_price":"701.41","graphics_price":"0","production_price":"0","installation_price":"300.00"},{"description":"gradient on sedes ","material":"15 AUD","lamination":"CWGL152-/DOL 1460/","print":"YES","plot":"YES","plot_price_sqm":"0","qty":"1","x":"1.37","y":"2","total_m2":"2.74","costumer_type":"GROUP 8","material_price":"15.0","lamination_price":"18.0","print_price":"12.0","plot_price":"0.00","mat_ad_value":"1.1","tisk":"1.4","total_price":"145.49","graphics_time":"","production_time":"","installation_time":"140","item_price":"285.49","graphics_price":"0","production_price":"0","installation_price":"140.00"},{"description":"back signage ","material":"25 AUD","lamination":"NO","print":"NO","plot":"YES","plot_price_sqm":"20","qty":"1","x":"0.6","y":"1.5","total_m2":"0.90","costumer_type":"GROUP 8","material_price":"25.0","lamination_price":"0.0","print_price":"0.0","plot_price":"18.00","mat_ad_value":"1.1","tisk":"1.4","total_price":"42.75","graphics_time":"","production_time":"","installation_time":"","item_price":"42.75","graphics_price":"0","production_price":"0","installation_price":"0.00"}]}	\N	\N	\N	f	f	f	\N	f
210	92	125	I am after a quote on signage for a small fleet of three utes: \r\n\r\n2 x Two door great wall V240 (logo across single door on both side)(Patios Logo)\r\n\r\n1 x Four door great wall V240 (duel cab) (logo across front and rear doors on both sides) (Grannyflats Logo)\r\n\r\n2 x rear windscreen (patios logo)\r\n\r\n1 x rear windscreen (grannyflats logo)\r\n\r\nInstallation will most likely need to be done in Wetherill Park NSW 2164	\N	f	12	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	2	2015-01-29 01:47:43.055684+01	{"sam":{"total_price":"1313.11","expences":"490.11","price_incl_gstl":1444.4209999999998},"note":"3x ute signage according the artwork + 3x one way vision + installation at your work shop.","position":[{"description":"  print a logos ","material":"CWF1-\\\\MPI 1005SC\\\\","lamination":"CWGL152-/DOL 1460/","print":"YES","plot":"NO","plot_price_sqm":"0","qty":"1","x":"1.37","y":"4","total_m2":"5.48","costumer_type":"GROUP 5","material_price":"20.0","lamination_price":"18.0","print_price":"12.0","plot_price":"0.00","mat_ad_value":"1.2","tisk":"2.0","total_price":"381.41","graphics_time":"","production_time":"","installation_time":"","item_price":"381.41","graphics_price":"0","production_price":"0","installation_price":"0"},{"description":"  ploting the lettering ","material":"25 AUD","lamination":"NO","print":"NO","plot":"YES","plot_price_sqm":"25","qty":"1","x":"1.2","y":"2","total_m2":"2.40","costumer_type":"GROUP 5","material_price":"25.0","lamination_price":"0.0","print_price":"0.0","plot_price":"60.00","mat_ad_value":"1.2","tisk":"2.0","total_price":"132.00","graphics_time":"","production_time":"","installation_time":"","item_price":"132.00","graphics_price":"0","production_price":"0","installation_price":"0"},{"description":"  installation ","material":"NO","lamination":"NO","print":"YES","plot":"YES","plot_price_sqm":"0","qty":"1","x":"","y":"","total_m2":"0.00","costumer_type":"GROUP 1 ","material_price":"0.0","lamination_price":"0.0","print_price":"12.0","plot_price":"0.00","mat_ad_value":"1.8","tisk":"3.0","total_price":"0.00","graphics_time":"","production_time":"","installation_time":"360","item_price":"360.00","graphics_price":"0","production_price":"0","installation_price":"360.00"},{"description":"  delivery","material":"NO","lamination":"NO","print":"YES","plot":"YES","plot_price_sqm":"0","qty":"1","x":"","y":"","total_m2":"0.00","costumer_type":"GROUP 1 ","material_price":"0.0","lamination_price":"0.0","print_price":"12.0","plot_price":"0.00","mat_ad_value":"1.8","tisk":"3.0","total_price":"0.00","graphics_time":"","production_time":"","installation_time":"130","item_price":"130.00","graphics_price":"0","production_price":"0","installation_price":"130.00"},{"description":"  one way vision","material":"One Way Vision Auto View","lamination":"One Way Lam ","print":"YES","plot":"NO","plot_price_sqm":"0","qty":"1","x":"1.37","y":"1.7","total_m2":"2.33","costumer_type":"GROUP 5","material_price":"26.0","lamination_price":"29.0","print_price":"12.0","plot_price":"0.00","mat_ad_value":"1.2","tisk":"2.0","total_price":"209.70","graphics_time":"","production_time":"","installation_time":"100","item_price":"309.70","graphics_price":"0","production_price":"0","installation_price":"100.00"}]}	\N	\N	\N	f	f	f	\N	f
194	70	107	Caddy Door Signage and One-Way Vision for the back.	\N	f	12	\N	\N		\N	\N	\N		\N	\N	\N		\N	\N	2	2015-01-27 05:23:18+01	{"sam":{"total_price":"457.44","expences":"247.49","price_incl_gstl":503.18399999999997},"note":"Caddy doors signage and rear one way vision.","position":[{"description":"doors signage ","material":"CWF1-\\\\MPI 1005SC\\\\","lamination":"CWGL152-/DOL 1460/","print":"YES","plot":"YES","plot_price_sqm":"25","qty":"1","x":"1.37","y":"1.2","total_m2":"1.64","costumer_type":"GROUP 5","material_price":"20.0","lamination_price":"18.0","print_price":"12.0","plot_price":"41.00","mat_ad_value":"1.2","tisk":"2.0","total_price":"155.14","graphics_time":"","production_time":"","installation_time":"57","item_price":"212.14","graphics_price":"0","production_price":"0","installation_price":"57.00"},{"description":"one way vision ","material":"One Way Vision Auto View","lamination":"One Way Lam ","print":"YES","plot":"NO","plot_price_sqm":"0","qty":"1","x":"1.8","y":"1.37","total_m2":"2.47","costumer_type":"GROUP 5","material_price":"26.0","lamination_price":"29.0","print_price":"12.0","plot_price":"0.00","mat_ad_value":"1.2","tisk":"2.0","total_price":"222.30","graphics_time":"","production_time":"","installation_time":"23","item_price":"245.30","graphics_price":"0","production_price":"0","installation_price":"23.00"}]}	\N	\N	\N	f	f	f		f
205	74	45	dsfdsfg	\N	f	2	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	3	2015-01-28 15:02:04.65743+01	{"sam":{"total_price":"672895.80","expences":"234876.00","price_incl_gstl":740185.38},"note":"","position":[{"description":"dfgdfgdf","material":"MPI 2105EA","lamination":"CWGL137-/DOL 1360/","print":"YES","plot":"YES","plot_price_sqm":"25","qty":"12","x":"23","y":"23","total_m2":"6348.00","costumer_type":"GROUP 1 ","material_price":"9.0","lamination_price":"16.0","print_price":"12.0","plot_price":"158700.00","mat_ad_value":"1.8","tisk":"3.0","total_price":"672888.00","graphics_time":"3","production_time":"2","installation_time":"1","item_price":"672895.80","graphics_price":"4.80","production_price":"2.00","installation_price":"1.00"}]}	\N	\N	\N	f	f	f	dfgvdfgbdf	f
212	97	127	We have 3 vehicles that we want exactly replicated like the blue ute.	\N	f	12	\N	\N		\N	\N	\N		\N	\N	\N		\N	\N	2	2015-01-29 02:20:54+01	{"sam":{"total_price":"1924.71","expences":"557.73","price_incl_gstl":2117.181},"note":"3x ute signage according the artwork. In case that you will approved this quotation we need to do all 3 utes together. Approximate time for installation is 1 day.","position":[{"description":"one way for 3 vehicles ","material":"One Way Vision Auto View","lamination":"One Way Lam ","print":"YES","plot":"NO","plot_price_sqm":"0","qty":"1","x":"1.37","y":"1.6","total_m2":"2.19","costumer_type":"GROUP 5","material_price":"26.0","lamination_price":"29.0","print_price":"12.0","plot_price":"0.00","mat_ad_value":"1.2","tisk":"2.0","total_price":"197.10","graphics_time":"","production_time":"","installation_time":"150","item_price":"347.10","graphics_price":"0","production_price":"0","installation_price":"150.00"},{"description":"3x vehicle signage ","material":"CWF1-\\\\MPI 1005SC\\\\","lamination":"CWGL152-/DOL 1460/","print":"YES","plot":"YES","plot_price_sqm":"25","qty":"1","x":"1.37","y":"6","total_m2":"8.22","costumer_type":"GROUP 5","material_price":"20.0","lamination_price":"18.0","print_price":"12.0","plot_price":"205.50","mat_ad_value":"1.2","tisk":"2.0","total_price":"777.61","graphics_time":"","production_time":"","installation_time":"800","item_price":"1577.61","graphics_price":"0","production_price":"0","installation_price":"800.00"}]}	\N	\N	\N	f	f	f		f
195	72	108	Toyota Half Wrap with One-Way Vision	\N	f	12	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	2	2015-01-27 05:47:06.322661+01	{"sam":{"total_price":"1424.57","expences":"754.52","price_incl_gstl":1567.027},"note":"Toyota half wrap by artwork + one way vision on the back window","position":[{"description":"toyota half wrap ","material":"CWF1-\\\\MPI 1005SC\\\\","lamination":"CWGL152-/DOL 1460/","print":"YES","plot":"NO","plot_price_sqm":"0","qty":"1","x":"1.37","y":"9","total_m2":"12.33","costumer_type":"GROUP 5","material_price":"20.0","lamination_price":"18.0","print_price":"12.0","plot_price":"0.00","mat_ad_value":"1.2","tisk":"2.0","total_price":"858.17","graphics_time":"","production_time":"","installation_time":"347","item_price":"1205.17","graphics_price":"0","production_price":"0","installation_price":"347.00"},{"description":"one way vision ","material":"One Way Vision Auto View","lamination":"One Way Lam ","print":"YES","plot":"NO","plot_price_sqm":"0","qty":"1","x":"1.37","y":"1.5","total_m2":"2.06","costumer_type":"GROUP 5","material_price":"26.0","lamination_price":"29.0","print_price":"12.0","plot_price":"0.00","mat_ad_value":"1.2","tisk":"2.0","total_price":"185.40","graphics_time":"","production_time":"","installation_time":"34","item_price":"219.40","graphics_price":"0","production_price":"0","installation_price":"34.00"}]}	\N	\N	\N	f	f	f	\N	f
206	83	120	Vinyl Branding on 2 sides and Back with One-Way Vision. Toyota Kluger 2005	\N	f	12	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	2	2015-01-28 23:04:14.285306+01	{"sam":{"total_price":"1018.23","expences":"420.73","price_incl_gstl":1120.053},"note":"toyota kluger 2005 signage according the artwork ","position":[{"description":"toyota kluger 2005 signage ","material":"CWF1-\\\\MPI 1005SC\\\\","lamination":"CWGL152-/DOL 1460/","print":"YES","plot":"YES","plot_price_sqm":"25","qty":"1","x":"1.37","y":"4","total_m2":"5.48","costumer_type":"GROUP 6","material_price":"20.0","lamination_price":"18.0","print_price":"12.0","plot_price":"137.00","mat_ad_value":"1.1","tisk":"1.8","total_price":"484.43","graphics_time":"15","production_time":"60","installation_time":"220","item_price":"788.43","graphics_price":"24.00","production_price":"60.00","installation_price":"220.00"},{"description":"one way vision ","material":"One Way Vision Auto View","lamination":"One Way Lam ","print":"YES","plot":"NO","plot_price_sqm":"0","qty":"1","x":"1.37","y":"1.6","total_m2":"2.19","costumer_type":"GROUP 6","material_price":"26.0","lamination_price":"29.0","print_price":"12.0","plot_price":"0.00","mat_ad_value":"1.1","tisk":"1.8","total_price":"179.80","graphics_time":"","production_time":"","installation_time":"50","item_price":"229.80","graphics_price":"0","production_price":"0","installation_price":"50.00"}]}	\N	\N	\N	f	f	f	\N	f
224	107	135	need 1 tel number put to his shop signage 	\N	f	11	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	2	2015-01-30 01:51:05.358619+01	{"sam":{"total_price":"102.80","expences":"24.00"},"note":"","position":[{"description":"cutting black vinyl ","material":"25 AUD","lamination":"NO","print":"NO","plot":"YES","qty":"1","x":"1.2","y":"0.8","total_m2":"0.96","costumer_type":"GROUP 5","material_price":"25.0","lamination_price":"0.0","print_price":"0.0","plot_price":"24.00","mat.ad.value":"1.2","tisk":"2.0","total_price":"52.80","graphics_time":"","production_time":"","installation_time":"50","item_price":"102.80","graphics_price":"0","production_price":"0","installation_price":"50.00"}]}	\N	\N	\N	f	f	f	\N	f
230	115	144	t-board 7500x2900	\N	f	11	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	2	2015-01-30 02:59:49.579753+01	{"sam":{"total_price":"2739.00","expences":"0.00","price_incl_gstl":3012.9},"note":" T-Board Sign Measurement Width of 7500 mm and a height of  2900 mm.  \\n\\nFabricated from aluminium channels with grooves for replaceable skin.\\n\\nThe Sign comes complete with installation.\\n \\n","position":[{"description":"T-Board ","material":"NO","lamination":"NO","print":"YES","plot":"YES","plot_price_sqm":"0","qty":"1","x":"","y":"","total_m2":"0.00","costumer_type":"GROUP 1 ","material_price":"0.0","lamination_price":"0.0","print_price":"12.0","plot_price":"0.00","mat_ad_value":"1.8","tisk":"3.0","total_price":"0.00","graphics_time":"","production_time":"","installation_time":"2499","item_price":"2499.00","graphics_price":"0","production_price":"0","installation_price":"2499.00"},{"description":"instalation ","material":"NO","lamination":"NO","print":"YES","plot":"YES","plot_price_sqm":"0","qty":"1","x":"","y":"","total_m2":"0.00","costumer_type":"GROUP 1 ","material_price":"0.0","lamination_price":"0.0","print_price":"12.0","plot_price":"0.00","mat_ad_value":"1.8","tisk":"3.0","total_price":"0.00","graphics_time":"","production_time":"","installation_time":"240","item_price":"240.00","graphics_price":"0","production_price":"0","installation_price":"240.00"}]}	\N	\N	\N	f	f	f	\N	f
213	99	128	The two utes I have, are \r\n1 x 1999 Ford Courier extra cab ute with Aluminium tray\r\n1 x 2012 Mazda BT50 dual Cab Ute with standard tray\r\nCould you give me a price for the doors and tailgate please.	\N	f	12	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	2	2015-01-29 05:52:25.349514+01	{"sam":{"total_price":"781.81","expences":"171.50","price_incl_gstl":859.991},"note":"ute and mazda vehicle signage doors and back .","position":[{"description":"signage","material":"CWF1-\\\\MPI 1005SC\\\\","lamination":"CWGL152-/DOL 1460/","print":"YES","plot":"YES","plot_price_sqm":"25","qty":"1","x":"2.5","y":"1.37","total_m2":"3.43","costumer_type":"GROUP 4 ","material_price":"20.0","lamination_price":"18.0","print_price":"12.0","plot_price":"85.75","mat_ad_value":"1.5","tisk":"2.2","total_price":"371.81","graphics_time":"","production_time":"","installation_time":"410","item_price":"781.81","graphics_price":"0","production_price":"0","installation_price":"410.00"}]}	\N	\N	\N	f	f	f	\N	f
229	52	65	Print & Cut Logos for Audi A1 2013	\N	f	12	\N	\N	None	2015-02-03 03:07:00+01	2015-02-02 11:48:00+01	\N	None	2015-01-30 20:47:00+01	2015-02-03 03:07:00+01	\N	None	2015-02-03 03:07:00+01	2015-02-03 03:06:00+01	8	2015-01-30 02:46:25.292564+01	{"sam":{"total_price":"408.81","expences":"205.50"},"note":"Print & Cut Logos for ford.","position":[{"description":"stickers ","material":"CWF1-\\\\MPI 1005SC\\\\","lamination":"CWGL152-/DOL 1460/","print":"YES","plot":"YES","qty":"1","x":"1.37","y":"3","total_m2":"4.11","costumer_type":"GROUP 5","material_price":"20.0","lamination_price":"18.0","print_price":"12.0","plot_price":"102.75","mat.ad.value":"1.2","tisk":"2.0","total_price":"388.81","graphics_time":"","production_time":"20","installation_time":"","item_price":"408.81","graphics_price":"0","production_price":"20.00","installation_price":"0"}]}	20	20	20	f	f	f	\N	f
190	63	104	rear door redo according the artwork.	\N	f	6	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	3	2015-01-27 03:45:38.418642+01	{"sam":{"total_price":"332.0984","expences":"116.45"},"note":"nissan cube redo rear door according the artwork print and instalation. ","position":[{"description":"redo rear door","material":"CWF1-\\\\MPI 1005SC\\\\","lamination":"CWGL152-/DOL 1460/","print":"YES","plot":"NO","qty":"1","x":"1.37","y":"1.7","total_m2":"2.329","item_price":"332.0984","graphics_price":"0","production_price":"0","installation_price":"170"}]}	\N	\N	\N	f	f	f	going out of system 	f
196	52	65	Gold laminated 	\N	f	11	\N	11	None	2015-01-27 06:32:00+01	2015-01-27 07:02:00+01	11	None	2015-01-27 07:32:00+01	2015-01-27 07:32:00+01	11	None	2015-01-27 07:02:00+01	2015-01-27 07:32:00+01	8	2015-01-27 06:09:19.477309+01	{"sam":{"total_price":"115.66","expences":"54.72"},"note":"lam. gold material ","position":[{"description":"gold lamination ","material":"NO","lamination":"CWML152-/DOL 1480/","print":"NO","plot":"NO","qty":"1","x":"2","y":"1.52","total_m2":"3.04","item_price":"115.66","graphics_price":"0","production_price":"0","installation_price":"50.00"}]}	30	0	30	f	f	f	\N	t
183	52	65	Plot vinyl as per agreement with Paul. Ask Paul for more info.	\N	f	12	\N	11	None	2015-01-26 23:41:00+01	2015-01-26 23:41:00+01	11	None	2015-01-26 23:41:00+01	2015-01-26 23:41:00+01	11	None	2015-01-26 23:41:00+01	2015-01-26 23:41:00+01	3	2015-01-23 03:49:00.585875+01	{"sam":{"total_price":"2170.20","expences":"720.00","price_incl_gstl":2387.22},"note":"","position":[{"description":"ertertertert","material":"NO","lamination":"NO","print":"YES","plot":"YES","plot_price_sqm":"0","qty":"3","x":"4","y":"5","total_m2":"60.00","costumer_type":"GROUP 1 ","material_price":"0.0","lamination_price":"0.0","print_price":"12.0","plot_price":"0.00","mat_ad_value":"1.8","tisk":"3.0","total_price":"2160.00","graphics_time":"2","production_time":"3","installation_time":"4","item_price":"2170.20","graphics_price":"3.20","production_price":"3.00","installation_price":"4.00"}]}	0	0	0	f	f	f	done	t
207	85	121	Repair of Damaged Vinyl on Top of the vehicle and Back Corner as indicated on the Photograph	\N	f	12	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	2	2015-01-28 23:23:07.64779+01	{"sam":{"total_price":"701.20","expences":"336.00","price_incl_gstl":771.32},"note":"Reparation of the smashed parts.","position":[{"description":"black parts redo","material":"40 AUD","lamination":"CWGL137-/DOL 1360/","print":"NO","plot":"NO","plot_price_sqm":"0","qty":"1","x":"1.5","y":"2","total_m2":"3.00","costumer_type":"GROUP 5","material_price":"40.0","lamination_price":"16.0","print_price":"0.0","plot_price":"0.00","mat_ad_value":"1.2","tisk":"2.0","total_price":"201.60","graphics_time":"","production_time":"","installation_time":"298","item_price":"499.60","graphics_price":"0","production_price":"0","installation_price":"298.00"},{"description":"green parts redo","material":"40 AUD","lamination":"CWGL137-/DOL 1360/","print":"NO","plot":"NO","plot_price_sqm":"0","qty":"1","x":"1.5","y":"2","total_m2":"3.00","costumer_type":"GROUP 5","material_price":"40.0","lamination_price":"16.0","print_price":"0.0","plot_price":"0.00","mat_ad_value":"1.2","tisk":"2.0","total_price":"201.60","graphics_time":"","production_time":"","installation_time":"","item_price":"201.60","graphics_price":"0","production_price":"0","installation_price":"0"}]}	\N	\N	\N	f	f	f	\N	f
225	107	135	boat wrap 	\N	f	11	\N	16	Meegan pls create just telephone number (02) 9700 7223 prepare to cut its for pet carriers Ken last time we forget to put the number there on the shop signage so we will just cut it and ill install that by tomorrow measurements are 900 mm thanks 	2015-01-30 01:58:00+01	2015-01-30 02:18:00+01	\N	instalation in mascot 16 Beresford street	2015-01-31 01:58:00+01	2015-01-31 03:58:00+01	\N	None	2015-01-31 01:58:00+01	2015-01-31 02:18:00+01	6	2015-01-30 01:52:52.518063+01	{"sam":{"total_price":"845.36","expences":"380.00"},"note":"boat full wrap and design.","position":[{"description":"boat full wrap and design ","material":"CWF1-\\\\MPI 1005SC\\\\","lamination":"CWGL152-/DOL 1460/","print":"YES","plot":"NO","qty":"1","x":"1.52","y":"5","total_m2":"7.60","costumer_type":"GROUP 8","material_price":"20.0","lamination_price":"18.0","print_price":"12.0","plot_price":"0.00","mat.ad.value":"1.1","tisk":"1.4","total_price":"445.36","graphics_time":"","production_time":"","installation_time":"400","item_price":"845.36","graphics_price":"0","production_price":"0","installation_price":"400.00"}]}	20	120	20	f	f	f	\N	f
231	117	145	supply and placement of company logos and website onto two hilux’s and a van	\N	f	12	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	2	2015-01-30 03:58:17.290347+01	{"sam":{"total_price":"359.20","expences":"137.00"},"note":"Toyota Hilux vehicle signage according the artwork. \\n","position":[{"description":"vehicle signage ","material":"CWF1-\\\\MPI 1005SC\\\\","lamination":"CWGL152-/DOL 1460/","print":"YES","plot":"YES","qty":"1","x":"1.37","y":"2","total_m2":"2.74","costumer_type":"GROUP 5","material_price":"20.0","lamination_price":"18.0","print_price":"12.0","plot_price":"68.50","mat.ad.value":"1.2","tisk":"2.0","total_price":"259.20","graphics_time":"","production_time":"","installation_time":"100","item_price":"359.20","graphics_price":"0","production_price":"0","installation_price":"100.00"}]}	\N	\N	\N	f	f	f	\N	f
214	100	101	Viny Branding on 2 side and the back with one-way vision\r\n\r\nSmaller model caddy but the same design as attached.	\N	f	12	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	2	2015-01-29 06:04:17.916385+01	{"sam":{"total_price":"712.00","expences":"0.00","price_incl_gstl":783.2},"note":"vehicle signage caddy according the artwork. ","position":[{"description":"caddy ","material":"NO","lamination":"NO","print":"YES","plot":"YES","plot_price_sqm":"0","qty":"1","x":"","y":"","total_m2":"0.00","costumer_type":"GROUP 1 ","material_price":"0.0","lamination_price":"0.0","print_price":"12.0","plot_price":"0.00","mat_ad_value":"1.8","tisk":"3.0","total_price":"0.00","graphics_time":"","production_time":"","installation_time":"712","item_price":"712.00","graphics_price":"0","production_price":"0","installation_price":"712.00"}]}	\N	\N	\N	f	f	f	\N	f
157	29	39	TRUCK WRAP- 3M Controltac 180c\r\n \r\n-2 x 7900mm x 1550mm trailer sides to have digitally printed vehicle wrap applied.\r\n-media: 3M Controltac 180c\r\n-full colour high resolution digitally printed graphics.\r\n-includes UV/scratch resistant gloss lamination.\r\n-fully installed\r\n-Supply and install – we have the artwork\r\n-to be done in February\r\n \r\n \r\nCONTAINER SIGNAGE- Installation only\r\n \r\n2 x  6000m long X 1800mm high (can vary in height to match sheet but not below 1750mm) existing panels to have digitally printed prints applied.\r\n- prints to be supplied by the client.\r\n- Prints will require trimming onsite, as original print sizes were 10,017mm long x 1735mm high.\r\n- For this one we will need a Alupanel sheet as it’s going on to a container.  You may have other suggestions though.\r\n- ready now, container on-site at Kurnell.	\N	t	12	\N	2		2015-01-20 13:01:00+01	2015-01-20 16:01:00+01	10		2015-01-22 15:00:00+01	2015-01-22 16:00:00+01	\N		2015-01-21 11:00:00+01	2015-01-21 13:00:00+01	9	2015-01-19 23:57:18+01	{"sam":{"total_price":"5482.61","expences":"1200.80"},"note":"","position":[{"description":"truck wrap ","material":"CWF1-\\\\MPI 1005SC\\\\","lamination":"CWGL152-/DOL 1460/","print":"YES","plot":"NO","qty":"2","x":"7.9","y":"1.52","total_m2":"24.016000000000002","item_price":"2482.61","graphics_price":"0","production_price":"0","installation_price":"960.00"},{"description":"container alu pannels ","material":"NO","lamination":"NO","print":"YES","plot":"YES","qty":"1","x":"","y":"","total_m2":"0","item_price":"1900.00","graphics_price":"0","production_price":"0","installation_price":"1900.00"},{"description":"install customer stickers ","material":"NO","lamination":"NO","print":"YES","plot":"YES","qty":"1","x":"","y":"","total_m2":"0","item_price":"1100.00","graphics_price":"0","production_price":"0","installation_price":"1100.00"}]}	200	60	\N	f	f	f		t
215	29	39	RUCK WRAP- 3M Controltac 180c\r\n \r\n-2 x 7900mm x 1550mm trailer sides to have digitally printed vehicle wrap applied.\r\n-media: 3M Controltac 180c\r\n-full colour high resolution digitally printed graphics.\r\n-includes UV/scratch resistant gloss lamination.\r\n-fully installed\r\n-Supply and install – we have the artwork\r\n-to be done in February\r\n \r\n \r\nCONTAINER SIGNAGE- Installation only\r\n \r\n2 x  6000m long X 1800mm high (can vary in height to match sheet but not below 1750mm) existing panels to have digitally printed prints applied.\r\n- prints to be supplied by the client.\r\n- Prints will require trimming onsite, as original print sizes were 10,017mm long x 1735mm high.\r\n- For this one we will need a Alupanel sheet as it’s going on to a container.  You may have other suggestions though.\r\n- ready now, container on-site at Kurnell.	\N	f	12	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	2	2015-01-29 06:07:55.672194+01	{"sam":{"total_price":"5450.87","expences":"1201.00","price_incl_gstl":5995.957},"note":"","position":[{"description":"truck wrap","material":"CWF1-\\\\MPI 1005SC\\\\","lamination":"CWGL152-/DOL 1460/","print":"YES","plot":"NO","plot_price_sqm":"0","qty":"2","x":"7.9","y":"1.52","total_m2":"24.02","costumer_type":"GROUP 6","material_price":"20.0","lamination_price":"18.0","print_price":"12.0","plot_price":"0.00","mat_ad_value":"1.1","tisk":"1.8","total_price":"1522.87","graphics_time":"","production_time":"","installation_time":"928","item_price":"2450.87","graphics_price":"0","production_price":"0","installation_price":"928.00"},{"description":"container alu pannels","material":"NO","lamination":"NO","print":"YES","plot":"YES","plot_price_sqm":"0","qty":"1","x":"","y":"","total_m2":"0.00","costumer_type":"GROUP 1 ","material_price":"0.0","lamination_price":"0.0","print_price":"12.0","plot_price":"0.00","mat_ad_value":"1.8","tisk":"3.0","total_price":"0.00","graphics_time":"","production_time":"","installation_time":"1900","item_price":"1900.00","graphics_price":"0","production_price":"0","installation_price":"1900.00"},{"description":"install customers stickers ","material":"NO","lamination":"NO","print":"YES","plot":"YES","plot_price_sqm":"0","qty":"1","x":"","y":"","total_m2":"0.00","costumer_type":"GROUP 1 ","material_price":"0.0","lamination_price":"0.0","print_price":"12.0","plot_price":"0.00","mat_ad_value":"1.8","tisk":"3.0","total_price":"0.00","graphics_time":"","production_time":"","installation_time":"1100","item_price":"1100.00","graphics_price":"0","production_price":"0","installation_price":"1100.00"}]}	\N	\N	\N	f	f	f	\N	f
208	87	122	Branding Land Rover with Vinyl Graphics.\r\n\r\nSides and Front only	\N	f	12	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	2	2015-01-29 00:30:21.949597+01	{"sam":{"total_price":"796.00","expences":"180.00","price_incl_gstl":875.6},"note":"Land Rover vehicle signage according the artwork.","position":[{"description":"Land rover signage ","material":"25 AUD","lamination":"NO","print":"NO","plot":"YES","plot_price_sqm":"25","qty":"1","x":"1.2","y":"6","total_m2":"7.20","costumer_type":"GROUP 5","material_price":"25.0","lamination_price":"0.0","print_price":"0.0","plot_price":"180.00","mat_ad_value":"1.2","tisk":"2.0","total_price":"396.00","graphics_time":"","production_time":"","installation_time":"400","item_price":"796.00","graphics_price":"0","production_price":"0","installation_price":"400.00"}]}	\N	\N	\N	f	f	f	\N	f
197	74	109	I am looking at mainly having the Z 10 as a feature of the design with Kawasaki included on other parts of the bike.\r\nThe tank will need a complete cover of some description to cover up the poor paint bubbling problem. The rear may also feature or incorporate the design.\r\nI am expecting the bike to be mainly black with the Kawasaki green  and some silver mixed in to break up the colours.	\N	f	12	\N	\N		\N	\N	\N		\N	\N	\N		\N	\N	2	2015-01-27 07:13:51+01	{"sam":{"total_price":"1073.19","expences":"205.50","price_incl_gstl":1180.509},"note":"motor bike wrap ","position":[{"description":"motor bike wrap ","material":"CWF1-\\\\MPI 1005SC\\\\","lamination":"CWGL152-/DOL 1460/","print":"YES","plot":"NO","plot_price_sqm":"0","qty":"1","x":"1.37","y":"3","total_m2":"4.11","costumer_type":"GROUP 3 ","material_price":"20.0","lamination_price":"18.0","print_price":"12.0","plot_price":"0.00","mat_ad_value":"1.6","tisk":"2.5","total_price":"373.19","graphics_time":"","production_time":"","installation_time":"700","item_price":"1073.19","graphics_price":"0","production_price":"0","installation_price":"700.00"}]}	\N	\N	\N	f	f	f		f
232	119	146	Posters 11 Nos. A2 Size as perviously done only half the quantity required.	\N	f	12	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	2	2015-01-30 04:36:57.582644+01	{"sam":{"total_price":"220.00","expences":"0.00"},"note":"11 x posters and lamination ","position":[{"description":"","material":"NO","lamination":"NO","print":"YES","plot":"YES","qty":"1","x":"","y":"","total_m2":"0.00","costumer_type":"GROUP 1 ","material_price":"0.0","lamination_price":"0.0","print_price":"12.0","plot_price":"0.00","mat.ad.value":"1.8","tisk":"3.0","total_price":"0.00","graphics_time":"","production_time":"","installation_time":"220","item_price":"220.00","graphics_price":"0","production_price":"0","installation_price":"220.00"}]}	\N	\N	\N	f	f	f	\N	f
\.


--
-- Name: office_request_id_seq; Type: SEQUENCE SET; Schema: public; Owner: stas
--

SELECT pg_catalog.setval('office_request_id_seq', 233, true);


--
-- Data for Name: office_settingscontent; Type: TABLE DATA; Schema: public; Owner: stas
--

COPY office_settingscontent (id, title, key, image, is_image, text, is_text) FROM stdin;
3	production_coefficient	production_coefficient		f	1	t
2	installation_coefficient	installation_coefficient		f	1	t
1	graphic_coefficient	graphic_coefficient		f	1.6	t
\.


--
-- Name: office_settingscontent_id_seq; Type: SEQUENCE SET; Schema: public; Owner: stas
--

SELECT pg_catalog.setval('office_settingscontent_id_seq', 3, true);


--
-- Name: auth_group_name_key; Type: CONSTRAINT; Schema: public; Owner: stas; Tablespace: 
--

ALTER TABLE ONLY auth_group
    ADD CONSTRAINT auth_group_name_key UNIQUE (name);


--
-- Name: auth_group_permissions_group_id_permission_id_key; Type: CONSTRAINT; Schema: public; Owner: stas; Tablespace: 
--

ALTER TABLE ONLY auth_group_permissions
    ADD CONSTRAINT auth_group_permissions_group_id_permission_id_key UNIQUE (group_id, permission_id);


--
-- Name: auth_group_permissions_pkey; Type: CONSTRAINT; Schema: public; Owner: stas; Tablespace: 
--

ALTER TABLE ONLY auth_group_permissions
    ADD CONSTRAINT auth_group_permissions_pkey PRIMARY KEY (id);


--
-- Name: auth_group_pkey; Type: CONSTRAINT; Schema: public; Owner: stas; Tablespace: 
--

ALTER TABLE ONLY auth_group
    ADD CONSTRAINT auth_group_pkey PRIMARY KEY (id);


--
-- Name: auth_permission_content_type_id_codename_key; Type: CONSTRAINT; Schema: public; Owner: stas; Tablespace: 
--

ALTER TABLE ONLY auth_permission
    ADD CONSTRAINT auth_permission_content_type_id_codename_key UNIQUE (content_type_id, codename);


--
-- Name: auth_permission_pkey; Type: CONSTRAINT; Schema: public; Owner: stas; Tablespace: 
--

ALTER TABLE ONLY auth_permission
    ADD CONSTRAINT auth_permission_pkey PRIMARY KEY (id);


--
-- Name: django_admin_log_pkey; Type: CONSTRAINT; Schema: public; Owner: stas; Tablespace: 
--

ALTER TABLE ONLY django_admin_log
    ADD CONSTRAINT django_admin_log_pkey PRIMARY KEY (id);


--
-- Name: django_content_type_app_label_3ec8c61c_uniq; Type: CONSTRAINT; Schema: public; Owner: stas; Tablespace: 
--

ALTER TABLE ONLY django_content_type
    ADD CONSTRAINT django_content_type_app_label_3ec8c61c_uniq UNIQUE (app_label, model);


--
-- Name: django_content_type_pkey; Type: CONSTRAINT; Schema: public; Owner: stas; Tablespace: 
--

ALTER TABLE ONLY django_content_type
    ADD CONSTRAINT django_content_type_pkey PRIMARY KEY (id);


--
-- Name: django_migrations_pkey; Type: CONSTRAINT; Schema: public; Owner: stas; Tablespace: 
--

ALTER TABLE ONLY django_migrations
    ADD CONSTRAINT django_migrations_pkey PRIMARY KEY (id);


--
-- Name: django_session_pkey; Type: CONSTRAINT; Schema: public; Owner: stas; Tablespace: 
--

ALTER TABLE ONLY django_session
    ADD CONSTRAINT django_session_pkey PRIMARY KEY (session_key);


--
-- Name: office_attachment_pkey; Type: CONSTRAINT; Schema: public; Owner: stas; Tablespace: 
--

ALTER TABLE ONLY office_attachment
    ADD CONSTRAINT office_attachment_pkey PRIMARY KEY (id);


--
-- Name: office_company_pkey; Type: CONSTRAINT; Schema: public; Owner: stas; Tablespace: 
--

ALTER TABLE ONLY office_company
    ADD CONSTRAINT office_company_pkey PRIMARY KEY (id);


--
-- Name: office_companycategory_pkey; Type: CONSTRAINT; Schema: public; Owner: stas; Tablespace: 
--

ALTER TABLE ONLY office_companycategory
    ADD CONSTRAINT office_companycategory_pkey PRIMARY KEY (id);


--
-- Name: office_contactperson_pkey; Type: CONSTRAINT; Schema: public; Owner: stas; Tablespace: 
--

ALTER TABLE ONLY office_contactperson
    ADD CONSTRAINT office_contactperson_pkey PRIMARY KEY (id);


--
-- Name: office_customertype_pkey; Type: CONSTRAINT; Schema: public; Owner: stas; Tablespace: 
--

ALTER TABLE ONLY office_customertype
    ADD CONSTRAINT office_customertype_pkey PRIMARY KEY (id);


--
-- Name: office_lamination_pkey; Type: CONSTRAINT; Schema: public; Owner: stas; Tablespace: 
--

ALTER TABLE ONLY office_lamination
    ADD CONSTRAINT office_lamination_pkey PRIMARY KEY (id);


--
-- Name: office_material_pkey; Type: CONSTRAINT; Schema: public; Owner: stas; Tablespace: 
--

ALTER TABLE ONLY office_material
    ADD CONSTRAINT office_material_pkey PRIMARY KEY (id);


--
-- Name: office_myuser_email_key; Type: CONSTRAINT; Schema: public; Owner: stas; Tablespace: 
--

ALTER TABLE ONLY office_myuser
    ADD CONSTRAINT office_myuser_email_key UNIQUE (email);


--
-- Name: office_myuser_pkey; Type: CONSTRAINT; Schema: public; Owner: stas; Tablespace: 
--

ALTER TABLE ONLY office_myuser
    ADD CONSTRAINT office_myuser_pkey PRIMARY KEY (id);


--
-- Name: office_organizer_pkey; Type: CONSTRAINT; Schema: public; Owner: stas; Tablespace: 
--

ALTER TABLE ONLY office_organizer
    ADD CONSTRAINT office_organizer_pkey PRIMARY KEY (id);


--
-- Name: office_plot_pkey; Type: CONSTRAINT; Schema: public; Owner: stas; Tablespace: 
--

ALTER TABLE ONLY office_plot
    ADD CONSTRAINT office_plot_pkey PRIMARY KEY (id);


--
-- Name: office_plotpricesam_pkey; Type: CONSTRAINT; Schema: public; Owner: stas; Tablespace: 
--

ALTER TABLE ONLY office_plotpricesam
    ADD CONSTRAINT office_plotpricesam_pkey PRIMARY KEY (id);


--
-- Name: office_print_pkey; Type: CONSTRAINT; Schema: public; Owner: stas; Tablespace: 
--

ALTER TABLE ONLY office_print
    ADD CONSTRAINT office_print_pkey PRIMARY KEY (id);


--
-- Name: office_request_pkey; Type: CONSTRAINT; Schema: public; Owner: stas; Tablespace: 
--

ALTER TABLE ONLY office_request
    ADD CONSTRAINT office_request_pkey PRIMARY KEY (id);


--
-- Name: office_settingscontent_pkey; Type: CONSTRAINT; Schema: public; Owner: stas; Tablespace: 
--

ALTER TABLE ONLY office_settingscontent
    ADD CONSTRAINT office_settingscontent_pkey PRIMARY KEY (id);


--
-- Name: auth_group_permissions_0e939a4f; Type: INDEX; Schema: public; Owner: stas; Tablespace: 
--

CREATE INDEX auth_group_permissions_0e939a4f ON auth_group_permissions USING btree (group_id);


--
-- Name: auth_group_permissions_8373b171; Type: INDEX; Schema: public; Owner: stas; Tablespace: 
--

CREATE INDEX auth_group_permissions_8373b171 ON auth_group_permissions USING btree (permission_id);


--
-- Name: auth_permission_417f1b1c; Type: INDEX; Schema: public; Owner: stas; Tablespace: 
--

CREATE INDEX auth_permission_417f1b1c ON auth_permission USING btree (content_type_id);


--
-- Name: django_admin_log_417f1b1c; Type: INDEX; Schema: public; Owner: stas; Tablespace: 
--

CREATE INDEX django_admin_log_417f1b1c ON django_admin_log USING btree (content_type_id);


--
-- Name: django_admin_log_e8701ad4; Type: INDEX; Schema: public; Owner: stas; Tablespace: 
--

CREATE INDEX django_admin_log_e8701ad4 ON django_admin_log USING btree (user_id);


--
-- Name: django_session_de54fa62; Type: INDEX; Schema: public; Owner: stas; Tablespace: 
--

CREATE INDEX django_session_de54fa62 ON django_session USING btree (expire_date);


--
-- Name: office_attachment_f68d2c36; Type: INDEX; Schema: public; Owner: stas; Tablespace: 
--

CREATE INDEX office_attachment_f68d2c36 ON office_attachment USING btree (request_id);


--
-- Name: office_company_category_id; Type: INDEX; Schema: public; Owner: stas; Tablespace: 
--

CREATE INDEX office_company_category_id ON office_company USING btree (category_id);


--
-- Name: office_contactperson_company_id; Type: INDEX; Schema: public; Owner: stas; Tablespace: 
--

CREATE INDEX office_contactperson_company_id ON office_contactperson USING btree (company_id);


--
-- Name: office_myuser_0e939a4f; Type: INDEX; Schema: public; Owner: stas; Tablespace: 
--

CREATE INDEX office_myuser_0e939a4f ON office_myuser USING btree (group_id);


--
-- Name: office_myuser_email_like; Type: INDEX; Schema: public; Owner: stas; Tablespace: 
--

CREATE INDEX office_myuser_email_like ON office_myuser USING btree (email varchar_pattern_ops);


--
-- Name: office_organizer_e8701ad4; Type: INDEX; Schema: public; Owner: stas; Tablespace: 
--

CREATE INDEX office_organizer_e8701ad4 ON office_organizer USING btree (user_id);


--
-- Name: office_request_company_id; Type: INDEX; Schema: public; Owner: stas; Tablespace: 
--

CREATE INDEX office_request_company_id ON office_request USING btree (company_id);


--
-- Name: office_request_contact_person_id; Type: INDEX; Schema: public; Owner: stas; Tablespace: 
--

CREATE INDEX office_request_contact_person_id ON office_request USING btree (contact_person_id);


--
-- Name: office_request_graphics_user_id; Type: INDEX; Schema: public; Owner: stas; Tablespace: 
--

CREATE INDEX office_request_graphics_user_id ON office_request USING btree (graphics_user_id);


--
-- Name: office_request_installation_user_id; Type: INDEX; Schema: public; Owner: stas; Tablespace: 
--

CREATE INDEX office_request_installation_user_id ON office_request USING btree (installation_user_id);


--
-- Name: office_request_office_manager_id; Type: INDEX; Schema: public; Owner: stas; Tablespace: 
--

CREATE INDEX office_request_office_manager_id ON office_request USING btree (office_manager_id);


--
-- Name: office_request_product_user_id; Type: INDEX; Schema: public; Owner: stas; Tablespace: 
--

CREATE INDEX office_request_product_user_id ON office_request USING btree (product_user_id);


--
-- Name: office_request_sales_manager_id; Type: INDEX; Schema: public; Owner: stas; Tablespace: 
--

CREATE INDEX office_request_sales_manager_id ON office_request USING btree (sales_manager_id);


--
-- Name: office_settingscontent_3c6e0b8a; Type: INDEX; Schema: public; Owner: stas; Tablespace: 
--

CREATE INDEX office_settingscontent_3c6e0b8a ON office_settingscontent USING btree (key);


--
-- Name: auth_group_permiss_permission_id_23962d04_fk_auth_permission_id; Type: FK CONSTRAINT; Schema: public; Owner: stas
--

ALTER TABLE ONLY auth_group_permissions
    ADD CONSTRAINT auth_group_permiss_permission_id_23962d04_fk_auth_permission_id FOREIGN KEY (permission_id) REFERENCES auth_permission(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: auth_group_permissions_group_id_58c48ba9_fk_auth_group_id; Type: FK CONSTRAINT; Schema: public; Owner: stas
--

ALTER TABLE ONLY auth_group_permissions
    ADD CONSTRAINT auth_group_permissions_group_id_58c48ba9_fk_auth_group_id FOREIGN KEY (group_id) REFERENCES auth_group(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: auth_permiss_content_type_id_51277a81_fk_django_content_type_id; Type: FK CONSTRAINT; Schema: public; Owner: stas
--

ALTER TABLE ONLY auth_permission
    ADD CONSTRAINT auth_permiss_content_type_id_51277a81_fk_django_content_type_id FOREIGN KEY (content_type_id) REFERENCES django_content_type(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: django_admin_content_type_id_5151027a_fk_django_content_type_id; Type: FK CONSTRAINT; Schema: public; Owner: stas
--

ALTER TABLE ONLY django_admin_log
    ADD CONSTRAINT django_admin_content_type_id_5151027a_fk_django_content_type_id FOREIGN KEY (content_type_id) REFERENCES django_content_type(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: django_admin_log_user_id_1c5f563_fk_office_myuser_id; Type: FK CONSTRAINT; Schema: public; Owner: stas
--

ALTER TABLE ONLY django_admin_log
    ADD CONSTRAINT django_admin_log_user_id_1c5f563_fk_office_myuser_id FOREIGN KEY (user_id) REFERENCES office_myuser(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: office_attachment_request_id_2ebf2047_fk_office_request_id; Type: FK CONSTRAINT; Schema: public; Owner: stas
--

ALTER TABLE ONLY office_attachment
    ADD CONSTRAINT office_attachment_request_id_2ebf2047_fk_office_request_id FOREIGN KEY (request_id) REFERENCES office_request(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: office_compan_category_id_7b80930c_fk_office_companycategory_id; Type: FK CONSTRAINT; Schema: public; Owner: stas
--

ALTER TABLE ONLY office_company
    ADD CONSTRAINT office_compan_category_id_7b80930c_fk_office_companycategory_id FOREIGN KEY (category_id) REFERENCES office_companycategory(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: office_contactperson_company_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: stas
--

ALTER TABLE ONLY office_contactperson
    ADD CONSTRAINT office_contactperson_company_id_fkey FOREIGN KEY (company_id) REFERENCES office_company(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: office_myuser_group_id_126a1459_fk_auth_group_id; Type: FK CONSTRAINT; Schema: public; Owner: stas
--

ALTER TABLE ONLY office_myuser
    ADD CONSTRAINT office_myuser_group_id_126a1459_fk_auth_group_id FOREIGN KEY (group_id) REFERENCES auth_group(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: office_organizer_user_id_3181d2af_fk_office_myuser_id; Type: FK CONSTRAINT; Schema: public; Owner: stas
--

ALTER TABLE ONLY office_organizer
    ADD CONSTRAINT office_organizer_user_id_3181d2af_fk_office_myuser_id FOREIGN KEY (user_id) REFERENCES office_myuser(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: office_re_contact_person_id_2b1283a4_fk_office_contactperson_id; Type: FK CONSTRAINT; Schema: public; Owner: stas
--

ALTER TABLE ONLY office_request
    ADD CONSTRAINT office_re_contact_person_id_2b1283a4_fk_office_contactperson_id FOREIGN KEY (contact_person_id) REFERENCES office_contactperson(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: office_reques_installation_user_id_2fd6af65_fk_office_myuser_id; Type: FK CONSTRAINT; Schema: public; Owner: stas
--

ALTER TABLE ONLY office_request
    ADD CONSTRAINT office_reques_installation_user_id_2fd6af65_fk_office_myuser_id FOREIGN KEY (installation_user_id) REFERENCES office_myuser(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: office_request_company_id_71a89e4c_fk_office_company_id; Type: FK CONSTRAINT; Schema: public; Owner: stas
--

ALTER TABLE ONLY office_request
    ADD CONSTRAINT office_request_company_id_71a89e4c_fk_office_company_id FOREIGN KEY (company_id) REFERENCES office_company(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: office_request_graphics_user_id_309694a4_fk_office_myuser_id; Type: FK CONSTRAINT; Schema: public; Owner: stas
--

ALTER TABLE ONLY office_request
    ADD CONSTRAINT office_request_graphics_user_id_309694a4_fk_office_myuser_id FOREIGN KEY (graphics_user_id) REFERENCES office_myuser(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: office_request_office_manager_id_3d1b4044_fk_office_myuser_id; Type: FK CONSTRAINT; Schema: public; Owner: stas
--

ALTER TABLE ONLY office_request
    ADD CONSTRAINT office_request_office_manager_id_3d1b4044_fk_office_myuser_id FOREIGN KEY (office_manager_id) REFERENCES office_myuser(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: office_request_product_user_id_6b29a521_fk_office_myuser_id; Type: FK CONSTRAINT; Schema: public; Owner: stas
--

ALTER TABLE ONLY office_request
    ADD CONSTRAINT office_request_product_user_id_6b29a521_fk_office_myuser_id FOREIGN KEY (product_user_id) REFERENCES office_myuser(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: office_request_sales_manager_id_283ef2e9_fk_office_myuser_id; Type: FK CONSTRAINT; Schema: public; Owner: stas
--

ALTER TABLE ONLY office_request
    ADD CONSTRAINT office_request_sales_manager_id_283ef2e9_fk_office_myuser_id FOREIGN KEY (sales_manager_id) REFERENCES office_myuser(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: public; Type: ACL; Schema: -; Owner: postgres
--

REVOKE ALL ON SCHEMA public FROM PUBLIC;
REVOKE ALL ON SCHEMA public FROM postgres;
GRANT ALL ON SCHEMA public TO postgres;
GRANT ALL ON SCHEMA public TO PUBLIC;


--
-- PostgreSQL database dump complete
--

