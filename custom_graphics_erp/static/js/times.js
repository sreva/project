google.setOnLoadCallback(drawChart);

      function drawChart() {
        var container = document.getElementById('timeline');
        var chart = new google.visualization.Timeline(container);
        var dataTable = new google.visualization.DataTable();

        dataTable.addColumn({ type: 'string', id: 'President' });
        dataTable.addColumn({ type: 'date', id: 'Start' });
        dataTable.addColumn({ type: 'date', id: 'End' });
        dataTable.addRows([
          [ '#37', new Date(2015, 1, 1, 13), new Date(2015, 1, 10, 14) ],
          [ '#37', new Date(2015, 1, 11), new Date(2015, 1, 15) ],
          [ '#37', new Date(2015, 1, 16), new Date(2015, 1, 24) ],
          [ '#31', new Date(2015, 3, 29), new Date(2015, 4, 10) ],
          [ '#31', new Date(2015, 4, 11), new Date(2015, 4, 15) ],
          [ '#31', new Date(2015, 4, 16), new Date(2015, 4, 25) ],
          [ '#34', new Date(2015, 2, 3), new Date(2015, 2, 13) ],
          [ '#34', new Date(2015, 2, 14), new Date(2015, 2, 20) ]
        ]);

        chart.draw(dataTable);
      }