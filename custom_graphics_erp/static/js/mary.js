$(document).ready(function(){
//    $("#datepicker1").datepicker();
//    $("#datepicker2").datepicker();
//    $("#datepicker3").datepicker();


    function getTimeAmPm() {
    var date_o = new Date();
    var date_mins = date_o.getMinutes() < 10 ? "0" + date_o.getMinutes() : date_o.getMinutes();
    var date_hours = date_o.getHours() > 12 ? "0" + (date_o.getHours() - 12) : date_o.getHours();
    var date_am_pm = date_o.getHours() < 12 ?  " AM" : " PM";

    // handle midnight
    date_hours = date_hours === 0 ? 12 : date_hours;


    return "'" + date_hours + ":" + date_mins + " " + date_am_pm + "'";
}

$(".datepicker").datepicker({dateFormat: 'yy-mm-dd ' + getTimeAmPm()});
$(".datepicker").inputmask("9999-99-99 99:99 aa");



});

