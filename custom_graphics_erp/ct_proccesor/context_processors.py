# --*-- coding: utf-8 --*--




def menu(request):
    management_menu = ['General manager', 'Office manager', 'Admin',]
    quotation_menu = ['General manager', 'Office manager', 'Admin',]
    sales_menu = ['General manager', 'Sales manager', 'Admin',]
    graphics_menu = ['General manager', 'Graphics manager', 'Admin',]
    production_menu = ['General manager', 'Production manager', 'Admin',]
    installation_menu =  ['General manager', 'Installation manager', 'Admin',]
    archive_menu = ['General manager', 'Office manager', 'Admin',]
    path = request.META.get('PATH_INFO')
    if path.startswith('/quotation/'):
        menu_item = 'quotation'
    elif path ==('/'):
        menu_item = 'home'
    elif path.startswith('/sales/'):
        menu_item = 'sales'
    elif path.startswith('/management/'):
        menu_item = 'management'
    elif 'installation' in path:
        menu_item = 'installation'
    elif 'graphics' in path:
        menu_item = 'graphics'
    elif 'production' in path:
        menu_item = 'production'
    elif 'archive' in path:
        menu_item = 'archive'
    return locals()

