var active = parseInt($('.castom-class').index())+ 1;
$('td:nth-child(' + active + ')').addClass('activeColumDay');
$(document).ready(function(){

     $('.save-message').click(function(){
    $('.quotation-declined-form').submit();
    });
    $('.finish').click(function(){
       finish()

    });
    function getXmlHttp(){
  var xmlhttp;
  try {
    xmlhttp = new ActiveXObject("Msxml2.XMLHTTP");
  } catch (e) {
    try {
      xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
    } catch (E) {
      xmlhttp = false;
    }
  }
  if (!xmlhttp && typeof XMLHttpRequest!='undefined') {
    xmlhttp = new XMLHttpRequest();
  }
  return xmlhttp;
}
    function finish(){

        var xmlhttp = getXmlHttp();
        var pathname = window.location.pathname;
        xmlhttp.open("GET",pathname+'?close' , false);
        xmlhttp.send();
        if(xmlhttp.status == 200) {
            window.location.href = xmlhttp.responseText
}
        return false;
    }
    file_add = 2;
    changeOnClick = function(id){
        $(id).find('input[type="radio"]').on('click', function(){
            var $id = $(this).attr('id');
            $(id).find('.hidden-block').removeClass('active');
            $(id).find('[data-block='+ $id +']').addClass('active');
        });
    };
    changeOnClick('#customerForm');
    changeOnClick('#contactPersonForm');
    $('#contactPersonNew').on('click', function(){
        $('#contactPersonExist').prop('checked', false);

    });
    $('#contactPersonExist').on('click', function(){
        $('#contactPersonNew').prop('checked', false);

    });
    $('.add-file').click(function(){
        var attachment = $('.attachment').last().clone(true);
        file_add+=1;
        attachment.find('input').attr('id', 'add'+file_add);

        attachment.find('label').attr('for', 'add'+file_add);
        attachment.find('span').text('');

        $('.form-group.attach').append(attachment);
        return false;
    });
    $('#customerNew').on('click', function(){
        var $radios = $('input:radio[name=new_contact_person]');
        $('#contactPersonExist').prop('checked', false);
        $radios.prop('checked', true);
        $('#customerExist').prop('checked', false);
        $('#existing-customer').removeClass('active');
        $('#new-customer').addClass('active');
    });
    $('#customerExist').on('click', function(){
        $('#customerNew').prop('checked', false);
        var $radios = $('input:radio[name=new_contact_person]');
        $radios.prop('checked', false);
        $('#contactPersonExist').prop('checked', true);
        $('#existing-customer').addClass('active');
        $('#new-customer').removeClass('active');
    });

//    $("body").on('click', '.dropdown-menu li a' ,function(){
//        var text = $(this).text();
//        console.log(text, 'text');
////        $(this).parents(".dropdown").find('button input').val($(this).text());
////        console.log($(this).text(), 'asdasd');
////        $(this).parents(".dropdown").removeClass('open').find('.btn').attr('aria-expanded',false);
////        return false;
//    });
    $(".choice-company li a").on('click', function(){
//        var val = $(this).find('option:selected').attr('id');
//        console.log(val);
        var id = $(this).attr('id');
        var text = $(this).text();
        console.log(text, 'text');
        $(this).closest(".dropdown").find('input').val(text);
        $.ajax({
            type: "POST",
            url: "/get_contact_list/",
            dataType: 'json',
            data:{
                'csrfmiddlewaretoken': getCookie('csrftoken'),
                'id': id
            },
            success: function(data) {

                if (data.status == 'OK'){
                    $('.choice-contact-person').empty();
                    if(data.company_contact_person_list_first){
                        console.log(data.company_contact_person_list_first, 'data');
                        $('.choice-contact-person-first').val(data.company_contact_person_list_first.name);
                        $('#contactPersonExist').click()
                    }
                    else{
                        $('#contactPersonNew').click()
                    }
                    var person_list = data.company_contact_person_list;
                    for(var i = 0; i < person_list.length; i++){
                        $('.choice-contact-person').append($('<li><a href="#">'+person_list[i].name+'</a></li>'));

                    }
                }
                if (data.status == 'ERROR'){
                }
            },
            error: function(data) {}
        });
//        return false;
    });

    $('input[type=file]').change(function(){
        var $in = $(this);
        $in.parent('p').find('span').html($in.val());
    });



//   $('.autosizeDrop').autosize();

    //resize table-row
    var $heightDescription = $('.description').children('tr').height();
    var $heightPrice = $('.price').children('tr').height();
    var count = $('.description').children('tr').length;
    var height = [];
    for ( var i = 0; i < count; i++ ) {
        var $heightDescription = $('.description').children('tr').height();
        height.push($heightDescription);
        console.log(height);
    }


    $('.sub-title .glyphicon').on('click', function(){
        if (!$(this).is('.glyphicon-chevron-down')) {
            $(this).addClass('glyphicon-chevron-down');
            $(this).removeClass('glyphicon-chevron-up');
        } else {
            $(this).removeClass('glyphicon-chevron-down');
            $(this).addClass('glyphicon-chevron-up');
        }
    });


    $('.js-del-filter').click(function(){
        $(this).parent().remove();
    });

    $(".for-search").keyup(function() {
        var value = this.value;

        $(".customer-table").find(".filtr").each(function(index) {
            var id = $(this).find("td").first().text();
            $(this).toggle(id.indexOf(value) !== -1);
            if (index === 0) return;
        });
    });

    $('.js-filter').click(function(){
        var filter_list = [];
        $('.select2-search-choice').each(function(){
            var choice = $(this).find('div').text();
            filter_list.push(choice);
        });
        $('.customer-table.table-scroll.table tr').each(function(){
            $(this).show().addClass('filtr');
            if (filter_list.indexOf($(this).find('.costumer-category').html()) < 0 && filter_list.length !=0) {
                $(this).hide().removeClass('filtr');

            }
        });
    });


    $('.js-add-person').click(function(){
        var block = $(this).parent().find('.form-add-person');
        block.css('height', '380px');
       return false;
    });

    $('.js-add-customers-person').click(function(){
        var block = $(this).parent().find('.custom-add-clone').clone();
        var changeBlock = block.removeClass('custom-add-clone').addClass('custom-add-person line-for-person');
        var cloneBlock = $(this).parent().find('.list-people');
        changeBlock.appendTo(cloneBlock);

       return false;
    });

    $( ".js-modal" ).click(function() {
      $( ".modal-open" ).dialog({
        autoOpen: false,
        modal: true,
        open: function(){
            $('.ui-widget-overlay').bind('click',function(){
                $('.modal-ope').dialog('close');
            })
        }
      });
      $(".ui-dialog-titlebar-close").addClass('fa fa-times');
    });

    $( ".modal-open" ).dialog({
        modal: true,
        open: function(){
            $('.ui-widget-overlay').bind('click',function(){
                $('.modal-open').dialog('close')
            })
        },
        close: function(ev, ui) { $(this).close(function(){window.location.href='/management/'}()); }
      });


    $('.js-modal-second').click(function(){
        $('.modal-open-email').dialog({
        modal: true,
        open: function(){
            $('.ui-widget-overlay').bind('click',function(){
                $('.modal-open-email').dialog('close')
            });
        }
      });
    });
    $('.js-close-print').on('click',function(){
        $('.modal-open-print').dialog('close');
        $('.modal-open-print-custom').dialog('close');
        return false;
    });
    $('.js-yes-close').click(function(){
        setTimeout(function(){$('.modal-open-print-custom').dialog('close'); }, 100);
    });
    $( ".js-modal-print" ).click(function() {
      $( ".modal-open-print" ).dialog({
        modal: true,
        open: function(){
            $('.ui-widget-overlay').bind('click',function(){
                $('.modal-open-print').dialog('close');
            })
        }
      });
      $(".ui-dialog-titlebar-close").addClass('fa fa-times');
    });

    $( ".js-modal-print-custom" ).click(function() {
      $( ".modal-open-print-custom" ).dialog({
        modal: true,
        open: function(){
            $('.ui-widget-overlay').bind('click',function(){
                $('.modal-open-print-custom').dialog('close');
            })
        }
      });
      $(".ui-dialog-titlebar-close").addClass('fa fa-times');
    });

        //slider -- prev ----next_________________
    function getUrlParameter(sParam)
{
    var sPageURL = window.location.search.substring(1);
    var sURLVariables = sPageURL.split('&');
    for (var i = 0; i < sURLVariables.length; i++)
    {
        var sParameterName = sURLVariables[i].split('=');
        if (sParameterName[0] == sParam)
        {
            return sParameterName[1];
        }
    }
}
    $('.next-week').click(function(){

        var path = window.location.href;
        var href = window.location.pathname;

        if (path.indexOf("next") > 0){
            var next = getUrlParameter('next');
            next = parseInt(next) + 1;
           window.location.href =  href +'?next='+next
        }
      if (path.indexOf("next") < 0){

           window.location.href =  path +'?next=1'
        }
        if (path.indexOf("prev") >0){
            var prev = parseInt(getUrlParameter('prev'));
            if (prev == 1){
                window.location.href = href
                }
            if(prev !=1){
                prev -= 1
                window.location.href =  href + '?prev='+prev
            }
        }
    });
    $('.prev-week').click(function(){

        var path = window.location.href;
        var href = window.location.pathname;
        if (path.indexOf("prev") > 0){
            var prev = getUrlParameter('prev');
            prev = parseInt(prev) + 1;
           window.location.href =  href +'?prev='+prev
            }
        if (path.indexOf("prev") < 0){
           window.location.href =  path +'?prev=1'
            }
        if (path.indexOf("next") >0){
           var next = parseInt(getUrlParameter('next'));
            if (next == 1){
                window.location.href = href
                }
            if(next !=1){
                next -= 1;
                window.location.href =  href + '?next='+next
            }
        }
    });
});
function getCookie(name) {
    var cookieValue = null;
    if (document.cookie && document.cookie != '') {
        var cookies = document.cookie.split(';');
        for (var i = 0; i < cookies.length; i++) {
            var cookie = jQuery.trim(cookies[i]);
            if (cookie.substring(0, name.length + 1) == (name + '=')) {
                cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
                break;
            }
        }
    }

    return cookieValue;

}

//function firefoxFix() {
//
//    if ( /firefox/.test( window.navigator.userAgent.toLowerCase() ) ) {
//
//        var tds = document.getElementsByTagName( 'td' );
//
//        for( var index = 0; index < tds.length; index++ ) {
//            tds[index].innerHTML = '<div class="ff-fix">' + tds[index].innerHTML + '</div>';
//        };
//
//        var style = '<style>'
//            + 'td { padding: 0 !important; }'
//            + 'td:hover::before, td:hover::after { background-color: transparent !important; }'
//            + '</style>';
//        document.head.insertAdjacentHTML( 'beforeEnd', style );
//
//    };
//
//};
//
//firefoxFix();


