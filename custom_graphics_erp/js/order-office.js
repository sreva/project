$(document).ready(function(){
    $('.sent-invoice').click(function(){
        var xmlhttp = new XMLHttpRequest();
        var pathname = window.location.pathname;
        xmlhttp.open("GET",pathname+'?sent_invoice' , false);
        xmlhttp.send();
        if(xmlhttp.status == 200) {
            $(this).parent().text('YES');
            $('.modal-open-sent-invoice').dialog();
            return false;
    }
    });
    $('.min').change(function(){
        var min = $(this).val();
        var data_start = $(this).closest('tr').find('.datepicker').val();
        var this_td = $(this).closest('tr');

        console.log(data_start, min);
        if (data_start != ''){
            $.ajax({
                type: "POST",
                url: "/get_date_range/"+id+"/",
                dataType: 'json',
                data:{
                    'csrfmiddlewaretoken': getCookie('csrftoken'),
                    'date_start': data_start,
                    'min':min
                },
                success: function(data) {

                    if (data.status == 'OK'){
                       var date_end_val = data.date_end;
                       var date_end = this_td.find('.date-end', date_end_val);
                       console.log($(this).closest('tr'));
                       date_end.html(date_end_val);
                        this_td.removeClass('need-change');
                        var changes = this_td.parent().find('.need-change');
                        changes.each(function(){
                            console.log('!!!!!!');
                            $(this).find('.datepicker').val(date_end_val);
                        })
                    }
                     if (data.status == 'ERROR'){
                        console.log(this_td.find('.datepicker'));
                        this_td.find('.datepicker').addClass('input-error');

                    }
                },
                error: function(data) {}
            });
        }
    });

    $('.datepicker').change(function(){

    var date_start = $(this).val();
    var min = $(this).closest('tr').find('.min').val();
    var this_td = $(this).closest('tr');
        this_td.find('.datepicker').removeClass('input-error');
        if (min != '' && parseInt(min)){
        console.log(date_start, min);
            $.ajax({
                type: "POST",
                url: "/get_date_range/"+id+"/",
                dataType: 'json',
                data:{
                    'csrfmiddlewaretoken': getCookie('csrftoken'),
                    'date_start': date_start,
                    'min':min
                },
                success: function(data) {

                    if (data.status == 'OK'){
                        var date_end_val = data.date_end;
                        var date_end = this_td.find('.date-end', date_end_val);
                        date_end.html(date_end_val);
                        this_td.removeClass('need-change');
                        var changes = this_td.parent().find('.need-change');
                        changes.each(function(){
                            console.log('!!!!!!');
                            $(this).find('.datepicker').val(date_end_val);
                        })

                    }
                    if (data.status == 'ERROR'){
                        console.log(this_td.find('.datepicker'));
                        this_td.find('.datepicker').addClass('input-error');

                    }
                },
                error: function(data) {}
            });
        }
    });
    $('.save').click(function(){
        var design_description = $('.design-description').val(), production_description = $('.production-description').val();
        var installation_description = $('.installation-description').val();
        var pathname = window.location.pathname;
        var checked = $('.invoice-paid').is(":checked");
        console.log(checked, '------');
        $.ajax({
                type: "POST",
                url: pathname,
                dataType: 'json',
                data:{
                    'csrfmiddlewaretoken': getCookie('csrftoken'),
                    'design_description': design_description,
                    'production_description':production_description,
                    'installation_description':installation_description,
                    'design_date_start':$('.design-date-start').val(),
                    'design_date_end':$('.design-date-end').html(),
                    'design_min':$('.design-min').val(),
                    'production_date_start':$('.production-date-start').val(),
                    'production_date_end':$('.production-date-end').html(),
                    'production_min':$('.production-min').val(),
                    'installation_date_start':$('.installation-date-start').val(),
                    'installation_date_end':$('.installation-date-end').html(),
                    'installation_min':$('.installation-min').val(),
                    'payment': checked

                },
                success: function(data) {

                    if (data.status == 'OK'){
                        console.log('yoyooyoyoyoyoy');
                        $('.modal-open-add-order').dialog();
                        $('.modal-open-add-order').dialog({
                        autoOpen: false,
                        modal: true,
                        open: function(){
                            $('.ui-widget-overlay').bind('click',function(){
                                                $('.modal-open-add-order').dialog('close')
                            })
                        },
                        close: function(ev, ui) { $(this).close(function(){window.location.href='/management/'}()); }
                      });
                    return false;
                    }
                    if (data.status == 'ERROR'){
                    }
                },
                error: function(data) {}
            });
    })

});
function getCookie(name) {
                var cookieValue = null;
                if (document.cookie && document.cookie != '') {
                    var cookies = document.cookie.split(';');
                    for (var i = 0; i < cookies.length; i++) {
                        var cookie = jQuery.trim(cookies[i]);
                        if (cookie.substring(0, name.length + 1) == (name + '=')) {
                            cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
                            break;
                        }
                    }
                }

    return cookieValue;

}
