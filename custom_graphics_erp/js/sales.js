function init_add_note(date, note, cell_id)
{
    $('#id_date').val(date);
    $('#id_note').val(note.split('<br />').join('\n'));
    $('#id_cell').val(cell_id);
}

function send_note()
{
    var date = $('#id_date').val();
    var note = $('#id_note').val();
    $.ajax({
        type: "POST",
        url: "/add_update_note/",
        dataType: 'json',
        data:{
            'csrfmiddlewaretoken': getCookie('csrftoken'),
            'date': date,
            'note': note
        },
        success: function(data) {

            if (data.status == 'OK') {
                var html = '<p type="button" data-target="#newNote" data-toggle="modal" id="' + $('#id_cell').val() + '" onclick="init_add_note("' + date + '", "' + escape(note.split('\n').join('<br />')) + '", "' + $('#id_cell').val() + '")">' + note.split('\n').join(' ') + '</p> \
                <div class="tip"> \
                <p>' + note.split('\n').join('<br />') + '</p> \
                </div>';
                $('#' + $('#id_cell').val()).parent().html(html);
                $('#newNote').modal('hide');
            }
            if (data.status == 'ERROR'){
            }
        },
        error: function(data) {}
    });
}
$(document).ready(function(){
 $( "#tags" ).autocomplete({
                source: availableTags,
                change: function(event, ui, obj) {
                    // ui.content is the array that's about to be sent to the response callback.
                    var s = ui.item;
                    console.log(s);
                    if (ui.item == obj) {
                        console.log('----')
                    } else {
                        console.log('++++')
                    }
                }

            });
});