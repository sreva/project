$(document).ready(function(){



//    textareas.each(function(){
//        $(this).autosize();
//    });
    //add position table------------------------------------------------
    $('.add-position').click(function(){
        var  $table_part1 = $('.table-part1'), $table_part2 = $('.table-part2');
        var $part1 = $table_part1.find($('.part1-clone')), $part2 = $table_part2.find($('.part2-clone'));
        var last_position = 0;
        var $part1_show = $part1.clone(true), $part2_show = $part2.clone(true);

        last_position = $table_part1.find($('.part1').last()).data('position');
        var clone_part1 = $part1_show.show().attr('data-position', last_position +1).removeClass('part1-clone').addClass('part1');
        var clone_part2 = $part2_show.show().attr('data-position', last_position +1).removeClass('part2-clone').addClass('part2');
        var textareas = clone_part1.find('.autosizeDrop');

            textareas.autosize();
        $table_part1.append(clone_part1);
        $table_part2.append(clone_part2);

        return false;
    });
    $(".dropdown-menu li ").click(function(){
        $(this).parents(".dropdown").find('button').text($(this).text());
        $(this).parents(".dropdown").find('button').attr('data-value', $(this).data('value'));
        $(this).parents(".dropdown").removeClass('open').find('.btn').attr('aria-expanded',false);
        return false;
    });
    //calculation--------------------------------------------------
    $('.input-float').keypress(function(event) {
    if(event.which < 46
    || event.which > 59) {
        event.preventDefault();
    } // prevent if not number/dot

    if(event.which == 46
    && $(this).val().indexOf('.') != -1) {
        event.preventDefault();
    } // prevent if already dot
    });

    $('.total_m2_value').change(function(){
        var position_id = $(this).closest('tr').data('position');
        var values = $(this).parent().parent().find('.total_m2_value');
        var total_m2 = 1, $total_m2 = $(this).parent().parent().find('.total_m2');
        values.each(function(){
            total_m2 = total_m2 * $(this).val();
        });
        $total_m2.html(total_m2.toFixed(2));
        change(position_id);
    });
    //$('.plot_price_sam').change(function(){
    //    var position_id = $(this).closest('tr').data('position');
    //    change(position_id);
    //
    //})
    function change(position_id) {
        function plot_price_sum(position_id) {
            var $main_table = $("[data-position='" + position_id + "']");
            var plot_price = $main_table.find('.plot-price').data('value');
            var plot_price_sum = $main_table.find('.plot-price-sam').text();
            var total_m2 = $main_table.find('.total_m2').text();
            var $plot_price_all = $main_table.find($('.plot-price-all'));
            var plot_price_all = parseFloat(plot_price) * parseFloat(plot_price_sum) * parseFloat(total_m2);
            console.log(parseFloat(plot_price) , parseFloat(plot_price_sum) , parseFloat(total_m2))
            if(!isNaN(plot_price_all)){$plot_price_all.text(plot_price_all.toFixed(2));}

        }
        plot_price_sum(position_id);
        function total_price(position_id) {
            var $main_table = $("[data-position='" + position_id + "']");
            var total_m2 = $main_table.find('.total_m2').text();
            var material_price = $main_table.find('.material').text();
            var lamination_price = $main_table.find('.lamination').text();
            var print_price = $main_table.find('.print').text();
            var plot_price_all_price = $main_table.find('.plot-price-all').text();
            var costumer_type_price = $main_table.find('.costumer_type').text();
            var tisk_type_price = $main_table.find('.tisk').text();
            var total_price = parseFloat(total_m2) * parseFloat(material_price) * parseFloat(costumer_type_price) + parseFloat(total_m2) * parseFloat(lamination_price) * parseFloat(costumer_type_price) + parseFloat(total_m2) * parseFloat(print_price) * parseFloat(tisk_type_price) +parseFloat(plot_price_all_price);
            var $total_price = $main_table.find('.total-price');

            if(!isNaN(total_price)){$total_price.text(total_price.toFixed(2));}

        }
        total_price(position_id);
        function item_price(position_id) {
            console.log('dddd');
            var $main_table = $("[data-position='" + position_id + "']");
            var $main_table_part2 = $('.part2' + "[data-position='" + position_id + "']");
            var $total_price = $main_table.find('.total-price').html();
            //var graphic = $main_table.find('.graphic').val();
            //var production = $main_table.find('.production').val();
            //var installation = $main_table.find('.installation').val();
            var graphic_sam = $main_table_part2.find('.graphic_sam').html();
            var production_sam = $main_table_part2.find('.production_sam').html();
            var installation_sam = $main_table_part2.find('.installation_sam').html();
            var $item_price = $main_table_part2.find('.item-price');
            var item_price = parseFloat($total_price) + parseFloat(graphic_sam) + parseFloat(production_sam) + parseFloat(installation_sam);

            //console.log($main_table_part2);
            console.log(parseFloat($total_price) , parseFloat(graphic_sam) , parseFloat(production_sam) , parseFloat(installation_sam), '---');
            $item_price.html(item_price.toFixed(2));
            //console.log($total_price, graphic_sam, production_sam, installation_sam);

        }
        item_price(position_id);
        function items_price() {
            var item_prices = 0;
            $('.part2').each(function () {
                if ($(this).find('.item-price').html() != "") {
                    item_prices += parseFloat($(this).find('.item-price').html())
                };
            });
            $('.item-prices-sum').text(item_prices.toFixed(2))

        }
        items_price();

        function expences() {
            var positions = $('.part1');
            var sum = 0;
            positions.each(function () {
                var total_m2 = $(this).find('.total_m2').html();
                var material = $(this).find('.material').html();
                var lamination = $(this).find('.lamination').html();
                var print = $(this).find('.print').html();
                var expences = (parseFloat(material) + parseFloat(lamination) + parseFloat(print)) * total_m2;
                sum += expences;
                $('.expences').text(sum.toFixed(2));

            });

        }
            expences()
    }
    $('.step').change(function(){
        var position_id = $(this).closest('tr').data('position');
        var cof = $(this).data('cof');
        var $main_table =$("[data-position='" + position_id + "']");
        var $main_table_part2 = $('.part2'+"[data-position='" + position_id + "']");
        var $class = $(this).data('class'), value = $(this).val();
        var price = $main_table_part2.find('td.'+$class);
        var val = value*cof;
        price.html(val.toFixed(2));
        change(position_id);
    });
    $(".dropdown-menu li ").click(function(){
        var position_id = $(this).closest('tr').data('position');
        var $class = $(this).data('class'), value = $(this).data('value');
        console.log($class, 'class');
        var $obj = $(this).closest('tr').find('td.'+$class);
        if($class == "plot_price_sam" || $class == "plot"){
           change(position_id);
        }
        if($class == "plot_price_sam"){
            //plot_price_sum(position_id);
            $(this).closest('tr').find('td.tisk').html($(this).data('tisk'));
            change(position_id);
        }

        $obj.html(value);
        change(position_id);
        return false;
    });
    $('.redirect-manage').click(function(){
        window.location.href = '/management/'
    });
$('.get-json').click(function(){
    var pathname = window.location.pathname;
    var positions_part1 = $('.part1');
    var price_incl_gstl =  parseFloat($('.item-prices-sum').text()).toFixed(2)/100*10 + parseFloat($('.item-prices-sum').text());
    var sam = {
        'total_price':$('.item-prices-sum').text(),
        'expences': $('.expences').text(),
        'price_incl_gstl': price_incl_gstl
    };
    json_object = {};

    json_object['sam']=sam;
    json_object['note']=$('.note-text').val();
    var position = [];
    positions_part1.each(function(){
        var position_id = $(this).data('position');
        var position_part2 = $(".part2"+"[data-position='" + position_id + "']");
        position.push({
            'description': $(this).find('.position-description').val(),
            'material': $(this).find('.select-material').text(),
            'lamination': $(this).find('.select-lamination').text(),
            'print': $(this).find('.select-print').text(),
            'plot': $(this).find('.select-plot').text(),
            'plot_price_sqm':$(this).find('.select-plot-price-sam').text(),
            'qty':  $(this).find('.qty').val(),
            'x': $(this).find('.x').val(),
            'y': $(this).find('.y').val(),
            'total_m2': $(this).find('.total_m2').html(),
            'costumer_type': $(this).find('.select-customer-type').text(),
            'material_price': $(this).find('.material').html(),
            'lamination_price': $(this).find('.lamination').html(),
            'print_price': $(this).find('.print').html(),
            'plot_price': $(this).find('.plot-price-all').html(),
            'mat_ad_value': $(this).find('.costumer_type').html(),
            'tisk': $(this).find('.tisk').html(),
            'total_price' : $(this).find('.total-price').html(),
            'graphics_time': $(this).find('.graphic').val(),
            'production_time': $(this).find('.production').val(),
            'installation_time': $(this).find('.installation').val(),
            'item_price':position_part2.find('.item-price').html(),
            'graphics_price':position_part2.find('.graphic_sam').html(),
            'production_price':position_part2.find('.production_sam').html(),
            'installation_price':position_part2.find('.installation_sam').html()
        });
    });
    json_object['position']= position;

    $.ajax({
            type: "POST",
            url: "/save" + pathname,
            dataType: 'json',
            data:{
                'csrfmiddlewaretoken': getCookie('csrftoken'),
                'json_object': JSON.stringify(json_object)
            },
            success: function(data) {

                if (data.status == 'OK'){
                    $('.modal-open-print').dialog();

                                }
                if (data.status == 'ERROR'){
                }
            },
            error: function(data) {}
            });
});

$('.get-send-json').click(function(){
    var pathname = window.location.pathname;
    var positions_part1 = $('.part1');
    var price_incl_gstl =  parseFloat($('.item-prices-sum').text()).toFixed(2)/100*10 + parseFloat($('.item-prices-sum').text());
    var sam = {
        'total_price':$('.item-prices-sum').text(),
        'expences': $('.expences').text(),
        'price_incl_gstl': price_incl_gstl
    };
    json_object = {};

    json_object['sam']=sam;
    json_object['note']=$('.note-text').val();
    var position = [];
    positions_part1.each(function(){
        var position_id = $(this).data('position');
        var position_part2 = $(".part2"+"[data-position='" + position_id + "']");
        position.push({
            'description': $(this).find('.position-description').val(),
            'material': $(this).find('.select-material').text(),
            'lamination': $(this).find('.select-lamination').text(),
            'print': $(this).find('.select-print').text(),
            'plot': $(this).find('.select-plot').text(),
            'qty':  $(this).find('.qty').val(),
            'x': $(this).find('.x').val(),
            'y': $(this).find('.y').val(),
            'total_m2': $(this).find('.total_m2').html(),
            'costumer_type': $(this).find('.select-customer-type').text(),
            'material_price': $(this).find('.material').html(),
            'lamination_price': $(this).find('.lamination').html(),
            'print_price': $(this).find('.print').html(),
            'plot_price': $(this).find('.plot-price-all').html(),
            'mat.ad.value': $(this).find('.costumer_type').html(),
            'tisk': $(this).find('.tisk').html(),
            'total_price' : $(this).find('.total-price').html(),
            'graphics_time': $(this).find('.graphic').val(),
            'production_time': $(this).find('.production').val(),
            'installation_time': $(this).find('.installation').val(),
            'item_price':position_part2.find('.item-price').html(),
            'graphics_price':position_part2.find('.graphic_sam').html(),
            'production_price':position_part2.find('.production_sam').html(),
            'installation_price':position_part2.find('.installation_sam').html()
        });
    });
    json_object['position']= position;
    var send_email = $('.send-email').val();
    console.log(send_email, 'send_email');
    $.ajax({
            type: "POST",
            url: "/save/send" + pathname,
            dataType: 'json',
            data:{
                'csrfmiddlewaretoken': getCookie('csrftoken'),
                'json_object': JSON.stringify(json_object),
                'email':send_email
            },
            success: function(data) {

                if (data.status == 'OK'){
                   $( ".modal-open-email").dialog('close');
                    $('.modal-open-email-send').dialog();
                        $( ".modal-open-email-send" ).dialog({
                            autoOpen: false,
                            modal: true,
                            open: function(){
                                $('.ui-widget-overlay').bind('click',function(){
                                    $('#dialog').dialog('close');
                                })
                            },
                            close: function(ev, ui) { $(this).close(function(){window.location.href='/management/'}()); }} );
                                }
                if (data.status == 'ERROR'){
                }
            },
            error: function(data) {}
            });
});
});
function getCookie(name) {
                var cookieValue = null;
                if (document.cookie && document.cookie != '') {
                    var cookies = document.cookie.split(';');
                    for (var i = 0; i < cookies.length; i++) {
                        var cookie = jQuery.trim(cookies[i]);
                        if (cookie.substring(0, name.length + 1) == (name + '=')) {
                            cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
                            break;
                        }
                    }
                }

    return cookieValue;

}