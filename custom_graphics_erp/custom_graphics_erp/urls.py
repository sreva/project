

from django.conf.urls import patterns, include, url
from django.core.urlresolvers import reverse_lazy
from django.views.generic import TemplateView
from django.contrib import admin
from custom_graphics_erp import settings


urlpatterns = patterns('',
   # Examples:
  (r'^media/(?P<path>.*)$', 'django.views.static.serve', {
        'document_root': settings.MEDIA_ROOT}),
   url(r'^archive/$', 'office.views.archive', name='archive'),
   url(r'^$', 'office.views.home', name='home'),
   url(r'^quotation/(?P<id>\d+)/$', 'office.views.quotation_manager', name="quotation"),
   # url(r'^quotation/$', 'office.views.quotation_list', name="quotation-list"),
   url(r'^quotation/(?P<id>\d+)/offer/pdf/test/$', 'office.views.myview_test', name="view_pdf_test"),

   url(r'^quotation/(?P<id>\d+)/offer/pdf/$', 'office.views.myview', name="view_pdf"),
   url(r'^add-company-category/$', 'office.views.add_category', name="add-category"),

   # url(r'^quotation/(?P<id>\d+)/offer/$', 'office.views.offer', name="offer"),
   url(r'^management/(?P<id>\d+)/office/$', 'office.views.office', name="office"),
   url(r'^save/send/quotation/(?P<id>\d+)/$', 'office.views.save_send_json', name="save-sand-quotation-json"),
   url(r'^save/quotation/(?P<id>\d+)/$', 'office.views.save_json', name="save-quotation-json"),
   url(r'^admin/', include(admin.site.urls)),
   url(r'^accounts/',
       include('registration.backends.simple.urls')),
   url(r'^login/$', 'office.views.custom_login', name='login'),
   url(r'^logout/$', 'office.views.logout_view', name='logout'),
   url(r'^sales/$', 'office.views.sales', name='sales'),
   url(r'^sales/(?P<id>\d+)/$', 'office.views.sales_details', name='sales-details'),
   url(r'^management/$', 'office.views.management', name='management'),
   # url(r'^quotation-manager/(?P<id>\d+)/$', 'office.views.quotation_manager'),
   url(r'^add-request/$', 'office.views.add_request', name='add-request'),
   url(r'^graphics/$', 'office.views.graphics_list', name='graphics-list'),
   url(r'^production/$', 'office.views.production_list', name='production-list'),
   url(r'^installation/$', 'office.views.installation_list', name='installation-list'),
   url(r'^graphics/(?P<id>\d+)/$', 'office.views.order_graphics', name='graphics-request'),
   url(r'^production/(?P<id>\d+)/$', 'office.views.order_production', name='production-request'),
   url(r'^installation/(?P<id>\d+)/$', 'office.views.order_installation', name='installation-request'),

   url(r'^get_contact_list/$', 'office.views.get_contact_person_list'),
   url(r'^get_date_range/(?P<id>\d+)/$', 'office.views.get_date_range'),
   url(r'^update_status/(?P<id>\d+)/$', 'office.views.update_status', name='update-status'),
   url(r'^archive/new-costumer/$', 'office.views.new_costumer', name='new-costumer'),
   url(r'^archive/customer-card/(?P<id>\d+)/pdf/$', 'office.views.costumer_card_pdf', name='customer-card-pdf'),

   url(r'^archive/customer-card/(?P<id>\d+)/$', 'office.views.customer_card', name='customer-card'),

   url(r'^archive/customer-card/(?P<id>\d+)/add-person/$', 'office.views.add_person', name='add-person'),

   # HTML url's
   # url(r'^signin$', TemplateView.as_view(template_name='sign/signin.html')),
    # url(r'^sales$', TemplateView.as_view(template_name='sales/sales-index.html')),
    (r'^media/(?P<path>.*)$', 'django.views.static.serve', {
        'document_root': settings.MEDIA_ROOT}),




   url(r'^recovery$', TemplateView.as_view(template_name='sign/recovery.html')),
   url(r'^recovery-mail$', TemplateView.as_view(template_name='sign/recovery-mail.html')),
   url(r'^new-password$', TemplateView.as_view(template_name='sign/new-password.html')),
   url(r'^broken-link$', TemplateView.as_view(template_name='sign/broken-link.html')),
   url(r'^add-request$', TemplateView.as_view(template_name='sales/new-request.html')),
   url(r'^quotation$', TemplateView.as_view(template_name='management/quotation.html')),
   url(r'^request-details', TemplateView.as_view(template_name='sales/request_details.html')),
   url(r'^orders-list', TemplateView.as_view(template_name='management/orders-list.html')),
   url(r'^customers', TemplateView.as_view(template_name='database/customers.html')),
   url(r'^new-customer', TemplateView.as_view(template_name='database/new-customer.html')),
   url(r'^orders-full', TemplateView.as_view(template_name='management/order-full.html')),
   url(r'^orders-office', TemplateView.as_view(template_name='management/order-office.html')),
   url(r'^order-department', TemplateView.as_view(template_name='management/order-department.html')),
   url(r'^customer-card', TemplateView.as_view(template_name='database/customer-card.html')),
   url(r'^order-graph', TemplateView.as_view(template_name='graphics/order.html')),
   url(r'^order-prod', TemplateView.as_view(template_name='production/order.html')),
   url(r'^order-inst', TemplateView.as_view(template_name='installation/order.html')),
   url(r'^offer', TemplateView.as_view(template_name='offer.html')),

   url(r'^add_update_note/$', 'office.views.add_update_note'),
)
