from django import forms
from django.contrib import admin
from django.contrib.auth.models import Group
from django.contrib.auth.admin import UserAdmin
from django.contrib.auth.forms import ReadOnlyPasswordHashField

from .models import MyUser, Attachment, Company, CompanyCategory, ContactPerson, CustomerType, Lamination, Material, \
    Organizer, Plot, PlotPriceSam, Print, Request, SettingsContent


class UserCreationForm(forms.ModelForm):
    """A form for creating new users. Includes all the required
    fields, plus a repeated password."""
    password1 = forms.CharField(label='Password', widget=forms.PasswordInput)
    password2 = forms.CharField(label='Password confirmation', widget=forms.PasswordInput)

    class Meta:
        model = MyUser
        fields = ('email', 'date_of_birth')

    def clean_password2(self):
        # Check that the two password entries match
        password1 = self.cleaned_data.get("password1")
        password2 = self.cleaned_data.get("password2")
        if password1 and password2 and password1 != password2:
            raise forms.ValidationError("Passwords don't match")
        return password2

    def save(self, commit=True):
        # Save the provided password in hashed format
        user = super(UserCreationForm, self).save(commit=False)
        user.set_password(self.cleaned_data["password1"])
        if commit:
            user.save()
        return user


class UserChangeForm(forms.ModelForm):
    """A form for updating users. Includes all the fields on
    the user, but replaces the password field with admin's
    password hash display field.
    """
    password = ReadOnlyPasswordHashField()

    class Meta:
        model = MyUser
        fields = ('email', 'password', 'date_of_birth', 'is_active', 'is_admin')

    def clean_password(self):
        # Regardless of what the user provides, return the initial value.
        # This is done here, rather than on the field, because the
        # field does not have access to the initial value
        return self.initial["password"]


class MyUserAdmin(UserAdmin):
    # The forms to add and change user instances
    form = UserChangeForm
    add_form = UserCreationForm

    # The fields to be used in displaying the User model.
    # These override the definitions on the base UserAdmin
    # that reference specific fields on auth.User.
    list_display = ('email', 'date_of_birth', 'is_admin')
    list_filter = ('is_admin',)
    fieldsets = (
        (None, {'fields': ('email', 'password')}),
        ('Personal info', {'fields': ('date_of_birth', 'name', 'phone', 'group',)}),
        ('Permissions', {'fields': ('is_admin',)}),
    )
    # add_fieldsets is not a standard ModelAdmin attribute. UserAdmin
    # overrides get_fieldsets to use this attribute when creating a user.
    add_fieldsets = (
        (None, {
            'classes': ('wide',),
            'fields': ('email', 'date_of_birth', 'password1', 'password2')}
        ),
    )
    search_fields = ('email',)
    ordering = ('email',)
    filter_horizontal = ()

# Now register the new UserAdmin...
admin.site.register(MyUser, MyUserAdmin)
# ... and, since we're not using Django's built-in permissions,
# unregister the Group model from admin.
# admin.site.unregister(Group)


class AttachmentAdminInLine(admin.TabularInline):
    model = Attachment


class RequestOrder(admin.ModelAdmin):
    list_display = ['id', 'contact_person', 'company', 'sending_date', 'price', 'payment', 'sales_manager',]
    inlines = [AttachmentAdminInLine,]
admin.site.register(Request, RequestOrder)


class CompanyAdmin(admin.ModelAdmin):
    list_display = ['name', 'category', 'email', 'phone', 'city', 'description']
admin.site.register(Company, CompanyAdmin)


class CompanyCategoryAdmin(admin.ModelAdmin):
    list_display = ['title']
admin.site.register(CompanyCategory, CompanyCategoryAdmin)


class ContactPersonAdmin(admin.ModelAdmin):
    list_display = ['name', 'company', 'position', 'email', 'phone']
admin.site.register(ContactPerson, ContactPersonAdmin)


class CustomerTypeAdmin(admin.ModelAdmin):
    pass
admin.site.register(CustomerType, CustomerTypeAdmin)


class LaminationAdmin(admin.ModelAdmin):
    pass
admin.site.register(Lamination, LaminationAdmin)


class MaterialAdmin(admin.ModelAdmin):
    pass
admin.site.register(Material, MaterialAdmin)


class OrganizerAdmin(admin.ModelAdmin):
    list_display = ['date', 'text', 'user']
admin.site.register(Organizer, OrganizerAdmin)


class PlotAdmin(admin.ModelAdmin):
    pass
admin.site.register(Plot, PlotAdmin)


class PlotPriceSamAdmin(admin.ModelAdmin):
    pass
admin.site.register(PlotPriceSam, PlotPriceSamAdmin)


class PrintAdmin(admin.ModelAdmin):
    pass
admin.site.register( Print,  PrintAdmin)


class SettingsContentAdmin(admin.ModelAdmin):
    list_display = ['title',]




admin.site.register(SettingsContent, SettingsContentAdmin)