# --*-- coding: utf-8 --*--
import os
from django.core.mail import EmailMessage, EmailMultiAlternatives
from django.utils import timezone

import json
from django.db.models import Q
import datetime
from django.contrib.auth import authenticate, login, get_user_model, logout
from django.contrib.auth.decorators import login_required
from django.core.urlresolvers import reverse
from django.http import HttpResponse
from custom_graphics_erp import settings
from .models import ContactPerson, Company, CompanyCategory, Request, Attachment, Material, Lamination, Plot, Print, \
    PlotPriceSam, CustomerType, \
    Organizer, MyUser, SettingsContent
from django.http import HttpResponse
import cStringIO as StringIO
import ho.pisa as pisa
from django.template.loader import get_template, render_to_string
from django.template import Context, RequestContext
from django.http import HttpResponse
from cgi import escape
from django.shortcuts import render, redirect, resolve_url

# Create your views here.
from django.template.response import TemplateResponse
from office.forms import NewCostumerForm, NewPersonForm


def home(request):
    if not request.user.is_authenticated():
        return redirect('/login/?next=/')
    return redirect(reverse('sales'))


def logout_view(request):
    logout(request)
    return redirect('/')


def custom_login(request):
    # redirect_to = request.REQUEST.get('next', '')
    error = False
    if request.POST:
        email = request.POST['email']
        password = request.POST['password']
        user = authenticate(email=email, password=password)
        if user is not None:
            if user.is_active:
                login(request, user)
                if user.group.name == 'General manager' or user.group.name == 'Admin':
                    url = '/management/'
                elif user.group.name == 'Sales manager':
                    url = '/sales/'
                elif user.group.name == 'Graphics manager':
                    url = '/graphics/'
                elif user.group.name == 'Production manager':
                    url = '/production/'
                elif user.group.name == 'Installation manager':
                    url = '/installation/'
                elif user.group.name == 'Office manager':
                    url = '/management/'
                return redirect(url)
            else:
                error = 'Wrong login or password'
        else:
            error = 'Wrong login or password'
    return TemplateResponse(request, 'sign/signin.html', locals())


def get_contact_person_list(request):
    if request.POST:
        id = request.POST.get('id', False)
        if id:
            company = Company.objects.get(id=id)
            company_contact_person_list = company.contactperson_set.all().order_by('name').values('id', 'name')
            company_contact_person_list_first = company_contact_person_list.first()
            return HttpResponse(json.dumps({
                'status': 'OK',
                'company_contact_person_list': list(company_contact_person_list),
                'company_contact_person_list_first': company_contact_person_list_first
            }))


@login_required(login_url='/login/')
def add_request(request):
    company_list = Company.objects.all().order_by('name')
    company_list_first = company_list.first()
    try:
        contact_person_list = company_list_first.contactperson_set.all()
    except AttributeError:
        pass
    # contact_person_list_first = contact_person_list.first()
    company_category_list = CompanyCategory.objects.all()
    # company_category_list_first = company_category_list.first()
    if request.method == 'POST':
        print request.POST
        files = request.FILES.getlist('file', [])
        print files

        new_customer = request.POST.get('new_customer', False)
        new_contact_person = request.POST.get('new_contact_person', False)
        if new_customer:
            # category = request.POST['company_category_choice']
            # category = CompanyCategory.objects.get(title = category)
            company = Company.objects.create(
                name=request.POST['customer_name'],
                email=request.POST['customer_email'],
                phone=request.POST['customer_phone'],
                category=CompanyCategory.objects.get(title=request.POST['customer_category']),
                city=request.POST['customer_city'],
                website=request.POST['customer_website'],
                address=request.POST['customer_address'],
                state=request.POST['customer_state'],
                post_code=request.POST['customer_post_code'],
                description=request.POST['customer_description'],
            )
        else:
            company = Company.objects.get(name=request.POST['customer_choice'])
        if new_contact_person:
            contact_person = ContactPerson.objects.create(
                company=company,
                name=request.POST['contact_person_name'],
                email=request.POST['contact_person_email'],
                phone=request.POST['contact_person_phone'],
                position=request.POST['contact_person_position'],
            )
        else:
            if request.POST.get('choice_contact_person') != '':

                contact_person = ContactPerson.objects.get(name=request.POST['choice_contact_person'])
            else:
                contact_person = None
        req = Request.objects.create(
            company=company,
            contact_person=contact_person,
            sales_manager=request.user,
            sending_date=datetime.datetime.now(),
            order_details=request.POST['order_details']
        )
        for elem in files:
            print elem, '!!!!!!!!!!!'
            if elem != "":
                Attachment.objects.create(attachment_file=elem, request_id=req.id)

        add_request = True
        return TemplateResponse(request, 'sales/new-request.html', locals())

    return TemplateResponse(request, 'sales/new-request.html', locals())


@login_required(login_url='/login/')
def sales(request):
    # Sales block
    if request.user.group.name == "Sales manager":
        request_list = Request.objects.filter(status='2',
                                              sales_manager=MyUser.objects.get(id=request.user.id)).order_by('id')
    elif request.user.group.name == "General manager" or request.user.group.name == "Admin":
        request_list = Request.objects.filter(status='2', ).order_by('id')

    expired_list = []
    for elem in request_list:
        if elem.sending_date + datetime.timedelta(days=2) < timezone.now():
            expired_list.append(elem.id)
    print expired_list
    # else:
    # request_list = {}

    # Organizer block
    now = datetime.datetime.now()
    days = []
    for elem in range(-10, 10):
        day = now + datetime.timedelta(days=elem)
        days.append(day.strftime("%d-%m-%Y"))

    today = datetime.datetime.today()
    start_date = today - datetime.timedelta(days=today.weekday())

    if request.GET and 'next' in request.GET:
        start_date = start_date + datetime.timedelta(weeks=int(request.GET['next']))
    if request.GET and 'prev' in request.GET:
        start_date = start_date - datetime.timedelta(weeks=int(request.GET['prev']))
    days = [start_date + datetime.timedelta(days=x) for x in range(7)]

    hours = [datetime.time(8 + x) for x in range(9)]

    notes_list = list(Organizer.objects.filter(user=request.user.id, date__gte=start_date,
                                               date__lte=start_date + datetime.timedelta(days=7)))
    notes = []
    for hour in hours:
        row = []
        for day in days:
            note = filter(lambda x: x.date.day == day.day and x.date.hour == hour.hour, notes_list)
            if note:
                row.append((day, hour, note[0]))
            else:
                row.append((day, hour, None))
        notes.append(row)

    return TemplateResponse(request, 'sales/sales-index.html', locals())


def sales_details(request, id):
    r = Request.objects.get(id=id)
    if request.method == "POST":
        r.quotation_declined = request.POST.get('message', '')
        r.status = '3'
        r.save()
        return redirect('/sales/')
    return TemplateResponse(request, 'sales/request_details.html', locals())


@login_required(login_url='/login/')
def management(request):
    def pct_fun(min):

        pct = float(min) / 60 * 100

        return pct

    def left_pct(min):
        result = float(min) * 100 / 60
        return result

    hours = [datetime.time(9 + x) for x in range(7)]

    today = timezone.now()
    if request.GET and 'next' in request.GET:
        today = today + datetime.timedelta(weeks=int(request.GET['next']))
    if request.GET and 'prev' in request.GET:
        today = today - datetime.timedelta(weeks=int(request.GET['prev']))

    start_date = today
    days = []
    today.replace(hour=0, minute=0)
    days.append(today)
    for elem in range(4):
        day = today + datetime.timedelta(days=1)
        today = workdayadd(day, 1)
        days.append(today)
    end_date = days[4]
    start_date = start_date.replace(hour=0, minute=0)

    orders_list = Request.objects.filter(Q(status__in=['5', '6', '7', '8', ]) | Q(status__in=['4', ])).order_by('id')
    quotation_requests = Request.objects.filter(status__in=['1', '2', ]).order_by('id')
    orders_report = []
    expired_list = []
    for order in orders_list:
        order_item = []
        print order.id, 'iddddddddddddd'



        # days = [start_date + datetime.timedelta(days=x) for x in range(5)]



        if order.status == "4":
            for day in days:
                for hour in hours:
                    td = []
                    order_item.append(td)
                    # orders_report.append(order_item)


        else:
            if order.installation_date_end:
                if order.installation_date_end + datetime.timedelta(hours=4) < timezone.now():
                    expired_list.append(order.id)
            elif order.product_date_end:
                if order.product_date_end + datetime.timedelta(hours=4) < timezone.now():
                    expired_list.append(order.id)
            elif order.graphics_date_end:
                if order.graphics_date_end + datetime.timedelta(hours=4) < timezone.now():
                    expired_list.append(order.id)
            print expired_list, 'expired_list'
            for day in days:
                for hour in hours:
                    td = []

                    add = True
                    append_hour = False
                    # day = day.hour == hour.hour
                    # if order_graphics_date_start and order_graphics_date_end:
                    day = day.replace(hour=hour.hour, minute=0, )



                    # day = day.strftime('%d.%m.%Y %H:%M')
                    # day = day.strftime( '%d.%m.%Y %H:%M%S')
                    # print day, type(day)
                    # day = datetime.datetime.strftime(day,'%d.%m.%Y %H:%M%S' )
                    # print day

                    if order.graphics_date_start and order.graphics_date_end:
                        order_graphics_date_start = order.graphics_date_start + datetime.timedelta(hours=11)
                        order_graphics_date_end = order.graphics_date_end + datetime.timedelta(hours=11)
                        print order_graphics_date_start, day

                        if order_graphics_date_start <= day <= order_graphics_date_end or hour.hour == order_graphics_date_start.hour and day.day == order_graphics_date_start.day or hour.hour == order_graphics_date_end.hour and day.day == order_graphics_date_end.day:
                            add = False
                            append_hour = True

                            if hour.hour == order_graphics_date_start.hour and day.day == order_graphics_date_start.day and order_graphics_date_start.hour == order_graphics_date_end.hour:
                                min = order_graphics_date_end.minute - order_graphics_date_start.minute
                                # print min, 'minnnnn', order_product_date_end.minute, order_product_date_start.time().min
                                left = left_pct(order_graphics_date_start.minute)
                                pct = pct_fun(min)
                                td.append({
                                    'class': 'blue-td',
                                    'width': pct,
                                    'left': left
                                })


                            elif hour.hour == order_graphics_date_start.hour and day.day == order_graphics_date_start.day:
                                min = order_graphics_date_start.time().minute
                                left = left_pct(min)
                                if min == 0:
                                    pct = 100
                                else:
                                    pct = 100 - pct_fun(min)
                                td.append({
                                    'class': 'blue-td',
                                    'width': pct,
                                    'left': left
                                })
                            elif hour.hour == order_graphics_date_end.hour and day.day == order_graphics_date_end.day:
                                min = order_graphics_date_end.time().minute
                                left = left_pct(min)
                                if min == 0:
                                    pct = 0
                                else:
                                    pct = pct_fun(min)
                                td.append({
                                    'class': 'blue-td',
                                    'width': pct,
                                    'left': 0
                                })
                            else:
                                td.append({
                                    'class': 'blue-td',
                                    'width': 100,
                                    'left': 0
                                })



                                # print order_report
                                #
                                # else:
                                # print day, hour, '--'
                                #     order_report.append({
                                #         'class': 'white',
                                #         'width': 100,
                                #         'position':'center'
                                #     })
                                # print order_report
                                # elif order_product_date_start and order_product_date_end:
                    if order.product_date_start and order.product_date_end:
                        order_product_date_start = order.product_date_start + datetime.timedelta(hours=11)
                        order_product_date_end = order.product_date_end + datetime.timedelta(hours=11)

                        if order_product_date_start <= day <= order_product_date_end or hour.hour == order_product_date_start.hour and day.day == order_product_date_start.day or hour.hour == order_product_date_end.hour and day.day == order_product_date_end.day:

                            add = False
                            append_hour = True
                            if hour.hour == order_product_date_start.hour and day.day == order_product_date_start.day and order_product_date_start.hour == order_product_date_end.hour:
                                min = order_product_date_end.minute - order_product_date_start.minute
                                # print min, 'minnnnn', order_product_date_end.minute, order_product_date_start.time().min
                                left = left_pct(order_product_date_start.minute)
                                pct = pct_fun(min)
                                td.append({
                                    'class': 'green-td',
                                    'width': pct,
                                    'left': left
                                })
                            elif hour.hour == order_product_date_start.hour and day.day == order_product_date_start.day:

                                min = order_product_date_start.time().minute
                                left = left_pct(min)
                                if min == 0:
                                    pct = 100
                                else:
                                    pct = 100 - pct_fun(min)
                                td.append({
                                    'class': 'green-td',
                                    'width': pct,
                                    'left': left
                                })
                                print  '------------', left, pct
                            elif hour.hour == order_product_date_end.hour and day.day == order_product_date_end.day:
                                # print 'date_end'
                                min = order_product_date_end.time().minute
                                left = left_pct(min)
                                if min == 0:
                                    pct = 0
                                else:
                                    pct = pct_fun(min)
                                td.append({
                                    'class': 'green-td',
                                    'width': pct,
                                    'left': 0
                                })
                            else:
                                td.append({
                                    'class': 'green-td',
                                    'width': 100,
                                    'left': 0
                                })
                                # else:
                                # order_report.append({
                                #         'class': 'white',
                                #         'width': 100,
                                #         'position':'center'
                                #     })

                                # elif order_installation_date_start and order_installation_date_end:
                    if order.installation_date_start and order.installation_date_end:
                        order_installation_date_start = order.installation_date_start + datetime.timedelta(hours=11)
                        order_installation_date_end = order.installation_date_end + datetime.timedelta(hours=11)

                        if order_installation_date_start <= day <= order_installation_date_end or hour.hour == order_installation_date_start.hour and day.day == order_installation_date_start.day or hour.hour == order_installation_date_end.hour and day.day == order_installation_date_end.day:
                            print day, hour, '--====---'
                            add = False
                            append_hour = True
                            if hour.hour == order_installation_date_start.hour and day.day == order_installation_date_start.day and order_installation_date_start.hour == order_installation_date_end.hour:
                                min = order_installation_date_end.minute - order_installation_date_start.minute
                                # print min, 'minnnnn', order_product_date_end.minute, order_product_date_start.time().min
                                pct = pct_fun(min)
                                left = left_pct(order_installation_date_start.minute)
                                td.append({
                                    'class': 'yellow-td',
                                    'width': pct,
                                    'left': left
                                })


                            elif hour.hour == order_installation_date_start.hour and day.day == order_installation_date_start.day:
                                min = order_installation_date_start.time().minute
                                left = left_pct(min)
                                if min == 0:
                                    pct = 100
                                else:
                                    pct = 100 - pct_fun(min)
                                td.append({
                                    'class': 'yellow-td',
                                    'width': pct,
                                    'left': left
                                })
                            elif hour.hour == order_installation_date_end.hour and day.day == order_installation_date_end.day:
                                min = order_installation_date_end.time().minute
                                left = left_pct(min)
                                if min == 0:
                                    pct = 0
                                else:
                                    pct = pct_fun(min)
                                td.append({
                                    'class': 'yellow-td',
                                    'width': pct,
                                    'left': 0
                                })
                            else:
                                td.append({
                                    'class': 'yellow-td',
                                    'width': 100,
                                    'left': 0
                                })
                                # if add:
                                #          th.append({
                                #                     'class': 'white',
                                #                     'width': 100,
                                #                     'left':0
                                #                 })
                                #     td.append(th)
                                # else:
                                #     print '-yoyooyy---'
                                #     order_report.append({
                                #             'class': 'white',
                                #             'width': 100,
                                #             'position':'center'
                                #         })
                                # else:
                                # print order_report, '=-----'

                                # orders_report.append(td)
                    order_item.append(td)
        orders_report.append(order_item)

    print orders_report
    return TemplateResponse(request, 'management/orders-list.html', locals())


@login_required(login_url='/login/')
def quotation_manager(request, id):
    order = Request.objects.get(id=id)
    costumer = Company.objects.get(id=order.company.id)
    contact_person = costumer.contactperson_set.all()
    costumer_group = CompanyCategory.objects.all()
    material_list = Material.objects.all().order_by('index_sort')
    lamination_list = Lamination.objects.all().order_by('index_sort')
    plot_list = Plot.objects.all().order_by('index_sort')
    print_list = Print.objects.all().order_by('index_sort')
    plot_price_sam = PlotPriceSam.objects.all().order_by('index_sort')
    costumer_type = CustomerType.objects.all().order_by('index_sort')
    graphic_cof = float(SettingsContent.objects.get(key='graphic_coefficient').text)
    installation_cof = float(SettingsContent.objects.get(key='installation_coefficient').text)
    production_cof = float(SettingsContent.objects.get(key='production_coefficient').text)
    if order.status == '2':
        position = json.loads(order.calculation_json)
        order_calculate = position['sam']
        order_note = position['note']
        position_list = position['position']
        first_position = False
    else:
        positions_list = []
        first_position = True
    if request.method == "POST":
        request_calculate = request.POST.get('request_calculate', False)
        if request_calculate:
            try:

                order.calculation_json = request_calculate

                return HttpResponse(json.dumps({
                    'status': 'OK im save this',
                }))
            except:
                return HttpResponse(json.dumps({
                    'status': 'OK but problem in format, im fixx it',
                }))
        else:
            return HttpResponse(json.dumps({
                'status': 'Error, you need fixx it, le me pls request_calculate',
            }))
    return TemplateResponse(request, 'management/quotation.html', locals())


def save_json(request, id):
    order = Request.objects.get(id=id)
    if request.method == "POST":
        json_object = request.POST['json_object']
        print json_object
        order.calculation_json = json_object
        order.status = '2'
        order.save()
        return HttpResponse(json.dumps({
            'status': 'OK',
        })
        )


def add_update_note(request):
    if request.POST:
        user = request.user
        date = datetime.datetime.strptime(request.POST.get('date'), '%d.%m.%Y %H:%M %p')
        note = request.POST.get('note')

        try:
            note_object = Organizer.objects.get(user=user, date=date)
            note_object.text = note
            note_object.save()
        except Organizer.DoesNotExist:
            note_object = Organizer.objects.create(
                user=user,
                date=date,
                text=note
            )

        return HttpResponse(json.dumps({
            'status': 'OK',
        })
        )


def update_status(request, id):
    status = request.GET.get('status')
    next = request.GET.get('next', '/')
    r = Request.objects.get(id=id)
    r.status = status
    if status == '2':
        r.sending_date = datetime.datetime.now()
    r.save()
    return redirect(next)


@login_required(login_url='/login/')
def office(request, id):
    order = Request.objects.get(id=id)
    order
    now = datetime.datetime.now()
    status_list = ['4', '5', '6', '7', '8']
    no_change_list = ['5', '6', '7', '8']

    if request.GET and 'sent_invoice' in request.GET:
        order.sent_invoice = True
        order.save()
    if request.GET and 'close' in request.GET:
        if order.payment:
            order.status = '9'
            order.save()
        return HttpResponse('/management/')
    if request.POST:
        if request.POST['payment'] == 'true':
            order.payment = True

        order.installation_description = request.POST['installation_description']
        if request.POST['installation_min'] != '':
            order.installation_min = int(request.POST['installation_min'])
            order.installation_date_start = datetime.datetime.strptime(request.POST['installation_date_start'],
                                                                       '%Y-%m-%d %I:%M %p')
            order.installation_date_end = datetime.datetime.strptime(request.POST['installation_date_end'],
                                                                     '%Y-%m-%d %I:%M %p')
        order.graphics_description = request.POST['design_description']
        if request.POST['design_min'] != '':
            order.graphics_min = int(request.POST['design_min'])
            order.graphics_date_start = datetime.datetime.strptime(request.POST['design_date_start'],
                                                                   '%Y-%m-%d %I:%M %p')
            order.graphics_date_end = datetime.datetime.strptime(request.POST['design_date_end'], '%Y-%m-%d %I:%M %p')
        order.product_description = request.POST['production_description']
        if request.POST['production_min'] != '':
            order.product_min = int(request.POST['production_min'])
            order.product_date_start = datetime.datetime.strptime(request.POST['production_date_start'],
                                                                  '%Y-%m-%d %I:%M %p')
            order.product_date_end = datetime.datetime.strptime(request.POST['production_date_end'],
                                                                '%Y-%m-%d %I:%M %p')
        if order.status not in ['5', '6', '7', '8']:
            if order.graphics_date_end:
                order.status = '5'
            elif order.product_date_end:
                order.status = '6'
            elif order.installation_date_end:
                order.status = '7'
            else:
                order.status = '8'
        order.save()
        # order.production_description = request.POST['installation_description']
        # order.design_description = request.POST['installation_description']
        return HttpResponse(json.dumps({
            'status': 'OK'
        })
        )
    return TemplateResponse(request, 'management/order-office.html', locals())


(MON, TUE, WED, THU, FRI, SAT, SUN) = range(7)


def time_in_range(start, end, x):
    if start <= end:
        return start < x < end
    else:
        return start < x or x < end


def workdayadd(start_date, work_days, whichdays=(MON, TUE, WED, THU, FRI)):
    weeks, days = divmod(work_days, len(whichdays))
    new_date = start_date + datetime.timedelta(weeks=weeks)
    for i in range(days):
        while new_date.weekday() not in whichdays:
            new_date += datetime.timedelta(days=1)
    return new_date


def get_date(start_date, minutes):
    print start_date, 'start_date!!!!!'
    start = datetime.time(9, 0, 0)
    end = datetime.time(16, 0, 0)
    m, s = divmod(0, 60)
    h, m = divmod(minutes, 60)
    print "%d:%02d:%02d" % (h, m, s)
    if h:
        print 'hhhhhh'
        for elem in range(int(h)):
            start_date = start_date + datetime.timedelta(hours=1)

            if time_in_range(start, end, start_date.time()):

                print elem, end.hour, '!!!!'

                if m:
                    print 'end!!!!'
                    for elem in range(m):
                        m = m - 1
                        if time_in_range(start, end, start_date.time()):
                            start_date = start_date + datetime.timedelta(minutes=1)
                        else:
                            start_date = start_date + datetime.timedelta(days=1)
                            start_date = start_date.replace(hour=9)
                            start_date = workdayadd(start_date, 1)
                            start_date = start_date + datetime.timedelta(minutes=1)

            else:
                print '1'
                # start_date = start_date - datetime.timedelta(hours=1)
                start_date = start_date + datetime.timedelta(days=1)
                start_date = start_date.replace(hour=9)
                print start_date
                start_date = workdayadd(start_date, 1)
                for elem in range(m):
                    m = m - 1
                    # print '1'
                    if time_in_range(start, end, start_date.time()):
                        start_date = start_date + datetime.timedelta(minutes=1)
                    else:
                        start_date = start_date + datetime.timedelta(days=1)
                        start_date.replace(hour=9)
                        start_date = workdayadd(start_date, 1)
                        start_date = start_date + datetime.timedelta(minutes=1)

                        # print start_date
    elif m:
        print 'mmmmm'
        for elem in range(m):
            m = m - 1
            start_date = start_date + datetime.timedelta(minutes=1)
            if time_in_range(start, end, start_date.time()):
                pass
            else:
                print 'ss'

                start_date = start_date + datetime.timedelta(days=1)
                start_date.replace(hour=9, minute=0, )
                start_date = workdayadd(start_date, 1)
                # start_date = start_date + datetime.timedelta(minutes=1)
                print start_date
    end_date = start_date
    return end_date


def get_date_range(request, id):
    order = Request.objects.get(id=id)
    if request.method == "POST":
        date_start = request.POST['date_start']

        date_start = datetime.datetime.strptime(date_start, "%Y-%m-%d %I:%M %p")
        print date_start.time().hour, '================='
        if 9 <= date_start.time().hour <= 16:

            min = request.POST['min']
            date_end = get_date(date_start, int(min))
            date_end = date_end.strftime("%Y-%m-%d %I:%M %p")
            print date_start, min, date_end
            return HttpResponse(json.dumps({
                'status': 'OK',
                'date_end': date_end
            })
            )
        else:
            return HttpResponse(json.dumps({
                'status': 'ERROR'
            })
            )


@login_required(login_url='/login/')
def order_graphics(request, id):
    order = Request.objects.get(id=id, status='5')
    if request.GET and 'close' in request.GET:
        if order.product_date_end:
            order.status = '6'
        elif order.installation_date_end:
            order.status = '7'
        else:
            order.status = '8'
        order.graphics_user = MyUser.objects.get(id=request.user.id)
        order.save()
        return HttpResponse('/graphics/')
    return TemplateResponse(request, 'graphics/order.html', locals())


@login_required(login_url='/login/')
def order_production(request, id):
    order = Request.objects.get(id=id, status='6')
    if request.GET and 'close' in request.GET:
        if order.installation_date_end:
            order.status = '7'
        else:
            order.status = '8'
        order.product_user = MyUser.objects.get(id=request.user.id)
        order.save()
        return HttpResponse('/production/')

    return TemplateResponse(request, 'production/order.html', locals())


@login_required(login_url='/login/')
def order_installation(request, id):
    order = Request.objects.get(id=id, status='7')
    if request.GET and 'close' in request.GET:
        print request.GET

        order.status = '8'
        order.installation_user = MyUser.objects.get(id=request.user.id)
        order.save()
        return HttpResponse('/installation/')

    return TemplateResponse(request, 'installation/order.html', locals())


@login_required(login_url='/login/')
def new_costumer(request):
    form = NewCostumerForm()
    company_category_list = CompanyCategory.objects.all()
    if request.POST:

        form = NewCostumerForm(request.POST)
        if form.is_valid():
            obj = form.save(commit=False)
            category = request.POST.get('category', False)
            if category:
                obj.category = CompanyCategory.objects.get(title=request.POST['category'])
            form.save()
            contacts_person = request.POST.getlist('contact-person-name')
            for elem in range(1, len(contacts_person)):
                name = request.POST.getlist('contact-person-name')[elem]
                email = request.POST.getlist('contact-person-email')[elem]
                phone = request.POST.getlist('contact-person-phone')[elem]
                position = request.POST.getlist('contact-person-position')[elem]
                if name != '' or email != '' or phone != '':
                    ContactPerson.objects.create(name=name, phone=phone, email=email, position=position, company=obj)
            return redirect('/archive/')

    return TemplateResponse(request, 'database/new-customer.html', locals())


@login_required(login_url='/login/')
def archive(request):
    customers_list = Company.objects.all().order_by('name')
    category_list = CompanyCategory.objects.all().order_by('title')
    return TemplateResponse(request, 'database/customers.html', locals())


@login_required(login_url='/login/')
def customer_card(request, id):
    company = Company.objects.get(id=id)
    contact_person_list = company.contactperson_set.all()
    company_category_list = CompanyCategory.objects.all()
    order_list = company.company.all()
    status_list = ['1', '2', '3']
    if request.POST:
        form = NewCostumerForm(request.POST, instance=company)
        if form.is_valid():
            obj = form.save(commit=False)
            obj.category = CompanyCategory.objects.get(title=request.POST['category'])
            form.save()
            return TemplateResponse(request, 'database/customer-card.html', locals())
    return TemplateResponse(request, 'database/customer-card.html', locals())


def add_person(request, id):
    company = Company.objects.get(id=id)
    if request.POST:
        form = NewPersonForm(request.POST)
        if form.is_valid():
            obj = form.save(commit=False)
            obj.company = Company.objects.get(id=id)
            form.save()
            return redirect('/archive/customer-card/%s/' % id)


@login_required(login_url='/login/')
def graphics_list(request):
    def pct_fun(min):

        pct = float(min) / 60 * 100

        return pct

    from django.utils import timezone

    hours = [datetime.time(9 + x) for x in range(7)]

    today = timezone.now()
    if request.GET and 'next' in request.GET:
        today = today + datetime.timedelta(weeks=int(request.GET['next']))
    if request.GET and 'prev' in request.GET:
        today = today - datetime.timedelta(weeks=int(request.GET['prev']))

    start_date = today
    days = []
    today.replace(hour=0, minute=0)
    days.append(today)
    for elem in range(4):
        day = today + datetime.timedelta(days=1)
        today = workdayadd(day, 1)
        days.append(today)
    end_date = days[4]
    start_date = start_date.replace(hour=0, minute=0)

    orders_list = Request.objects.filter(Q(status__in=['5']))
    quotation_requests = Request.objects.filter(status='1')
    orders_report = []
    for order in orders_list:
        order_report = []
        order_graphics_date_start = order.graphics_date_start
        order_graphics_date_end = order.graphics_date_end

        for day in days:
            for hour in hours:

                day = day.replace(hour=hour.hour, minute=0, )
                # if day.day < today.day:
                # order_report.append({
                #             'class': 'white',
                #             'whidth': 100,
                #             'position':'center'
                #         })
                # else:
                if order_graphics_date_start <= day <= order.graphics_date_end or hour.hour == order_graphics_date_start.hour and day.day == order_graphics_date_start.day or hour.hour == order_graphics_date_end.hour and day.day == order_graphics_date_end.day:
                    if hour.hour == order_graphics_date_start.hour and day.day == order_graphics_date_start.day and order_graphics_date_start.hour == order_graphics_date_end.hour:
                        min = order_graphics_date_end.minute - order_graphics_date_start.minute
                        # print min, 'minnnnn', order_product_date_end.minute, order_product_date_start.time().min
                        pct = pct_fun(min)
                        order_report.append({
                            'class': 'graphics',
                            'width': pct,
                            'position': 'left'
                        })
                    elif hour.hour == order_graphics_date_start.hour and day.day == order_graphics_date_start.day:
                        min = order_graphics_date_start.time().minute
                        if min == 0:
                            pct = 100
                        else:
                            pct = 100 - pct_fun(min)
                        order_report.append({
                            'class': 'graphics',
                            'width': pct,
                            'position': 'right'
                        })
                    elif hour.hour == order_graphics_date_end.hour and day.day == order_graphics_date_end.day:
                        min = order_graphics_date_end.time().minute
                        if min == 0:
                            pct = 100
                        else:
                            pct = pct_fun(min)
                        order_report.append({
                            'class': 'graphics',
                            'width': pct,
                            'position': 'left'
                        })
                    else:
                        order_report.append({
                            'class': 'graphics',
                            'width': 100,
                            'position': 'center'
                        })
                else:
                    order_report.append({
                        'class': 'white',
                        'width': 100,
                        'position': 'center'
                    })
                    # elif order_product_date_start and order_product_date_end:

                    # else:
        # print order_report, '=-----'
        orders_report.append(order_report)
    # print orders_report
    return TemplateResponse(request, 'graphics/order-department.html', locals())


@login_required(login_url='/login/')
def installation_list(request):
    def pct_fun(min):

        pct = float(min) / 60 * 100

        return pct

    hours = [datetime.time(9 + x) for x in range(7)]

    today = timezone.now()
    if request.GET and 'next' in request.GET:
        today = today + datetime.timedelta(weeks=int(request.GET['next']))
    if request.GET and 'prev' in request.GET:
        today = today - datetime.timedelta(weeks=int(request.GET['prev']))

    start_date = today
    days = []
    today.replace(hour=0, minute=0)
    days.append(today)
    for elem in range(4):
        day = today + datetime.timedelta(days=1)
        today = workdayadd(day, 1)
        days.append(today)
    end_date = days[4]
    start_date = start_date.replace(hour=0, minute=0)
    orders_list = Request.objects.filter(status__in=['7', ])
    quotation_requests = Request.objects.filter(status='1')
    orders_report = []
    for order in orders_list:
        order_report = []
        order_installation_date_start = order.installation_date_start
        order_installation_date_end = order.installation_date_end

        for day in days:
            for hour in hours:
                # day = day.hour == hour.hour
                # if order_graphics_date_start and order_graphics_date_end:
                day = day.replace(hour=hour.hour, minute=0, )

                if order_installation_date_start <= day <= order_installation_date_end or hour.hour == order_installation_date_start.hour and day.day == order_installation_date_start.day or hour.hour == order_installation_date_end.hour and day.day == order_installation_date_end.day:
                    if hour.hour == order_installation_date_start.hour and day.day == order_installation_date_start.day and order_installation_date_start.hour == order_installation_date_end.hour:
                        min = order_installation_date_end.minute - order_installation_date_start.minute
                        # print min, 'minnnnn', order_product_date_end.minute, order_product_date_start.time().min
                        pct = pct_fun(min)
                        order_report.append({
                            'class': 'installation',
                            'width': pct,
                            'position': 'left'
                        })
                    elif hour.hour == order_installation_date_start.hour and day.day == order_installation_date_start.day:
                        min = order_installation_date_end.time().minute

                        if min == 0:
                            pct = 100
                        else:
                            pct = 100 - pct_fun(min)
                        print 'date_start', pct

                        order_report.append({
                            'class': 'installation',
                            'width': pct,
                            'position': 'right'

                        })
                    elif hour.hour == order_installation_date_end.hour and day.day == order_installation_date_end.day:
                        # print 'date_end'
                        min = order_installation_date_start.time().minute
                        if min == 0:
                            pct = 100
                        else:
                            pct = pct_fun(min)
                        print 'date_end', pct
                        order_report.append({
                            'class': 'installation',
                            'width': pct,
                            'position': 'left'
                        })
                    else:
                        # print 'in_range'
                        order_report.append({
                            'class': 'installation',
                            'width': 100,
                            'position': 'center'
                        })
                else:
                    order_report.append({
                        'class': 'white',
                        'width': 100,
                        'position': 'center'
                    })
                    # else:
        # print order_report, '=-----'
        orders_report.append(order_report)
    # print orders_report
    return TemplateResponse(request, 'installation/order-department.html', locals())


@login_required(login_url='/login/')
def production_list(request):
    def pct_fun(min):
        pct = float(min) / 60 * 100

    hours = [datetime.time(9 + x) for x in range(7)]

    today = timezone.now()
    if request.GET and 'next' in request.GET:
        today = today + datetime.timedelta(weeks=int(request.GET['next']))
    if request.GET and 'prev' in request.GET:
        today = today - datetime.timedelta(weeks=int(request.GET['prev']))

    start_date = today
    days = []
    today.replace(hour=0, minute=0)
    days.append(today)
    for elem in range(4):
        day = today + datetime.timedelta(days=1)
        today = workdayadd(day, 1)
        days.append(today)
    end_date = days[4]
    start_date = start_date.replace(hour=0, minute=0)
    orders_list = Request.objects.filter(status__in=['6', ])
    quotation_requests = Request.objects.filter(status='1')
    orders_report = []
    for order in orders_list:
        order_report = []
        order_product_date_start = order.product_date_start
        order_product_date_end = order.product_date_end

        for day in days:
            for hour in hours:
                # day = day.hour == hour.hour
                # if order_graphics_date_start and order_graphics_date_end:
                day = day.replace(hour=hour.hour, minute=0, )



                # day = day.strftime('%d.%m.%Y %H:%M')
                # day = day.strftime( '%d.%m.%Y %H:%M%S')
                # print day, type(day)
                # day = datetime.datetime.strftime(day,'%d.%m.%Y %H:%M%S' )
                # print day

                # elif order_product_date_start and order_product_date_end:

                if order_product_date_start <= day <= order_product_date_end or hour.hour == order_product_date_start.hour and day.day == order_product_date_start.day or hour.hour == order_product_date_end.hour and day.day == order_product_date_end.day:
                    print hour, day
                    if hour.hour == order_product_date_start.hour and day.day == order_product_date_start.day and order_product_date_start.hour == order_product_date_end.hour:
                        min = order_product_date_end.minute - order_product_date_start.minute
                        # print min, 'minnnnn', order_product_date_end.minute, order_product_date_start.time().min
                        pct = pct_fun(min)
                        order_report.append({
                            'class': 'product',
                            'width': pct,
                            'position': 'left'
                        })
                    elif hour.hour == order_product_date_start.hour and day.day == order_product_date_start.day:
                        print 'date_satrt'
                        min = order_product_date_start.time().minute
                        if min == 0:
                            pct = 100
                        else:
                            pct = 100 - pct_fun(min)
                        print pct, 'pctpctpctpct', order.id
                        order_report.append({
                            'class': 'product',
                            'width': pct,
                            'position': 'right'
                        })
                    elif hour.hour == order_product_date_end.hour and day.day == order_product_date_end.day:
                        print 'date_end'
                        min = order_product_date_end.time().minute
                        print min
                        if min == 0:
                            pct = 100
                        else:
                            pct = pct_fun(min)
                        print pct, 'pctpctpctpct', order.id
                        order_report.append({
                            'class': 'product',
                            'width': pct,
                            'position': 'left'
                        })
                    else:
                        print 'date_range'
                        order_report.append({
                            'class': 'product',
                            'width': 100,
                            'position': 'center'
                        })

                        # elif order_installation_date_start and order_installation_date_end:

                else:
                    order_report.append({
                        'class': 'white',
                        'width': 100,
                        'position': 'center'
                    })
                    # else:
        # print order_report, '=-----'
        orders_report.append(order_report)
    # print orders_report
    return TemplateResponse(request, 'production/order-department.html', locals())


@login_required(login_url='/login/')
def offer(request, id):
    order = Request.objects.get(id=id)
    offer = order.calculation_json
    try:
        offer = json.loads(offer)
    except ValueError:
        offer = {}

    positions = offer.get('position', {})
    return TemplateResponse(request, 'offer.html', locals())


@login_required(login_url='/login/')
def quotation_list(request):
    orders_list = Request.objects.filter(status__in=['4', '5', '6', '7', '8'])
    quotation_requests = Request.objects.filter(status='1')
    return TemplateResponse(request, 'management/quotation_list.html', locals())


# def write_pdf(template_src, context_dict):
# template = get_template(template_src)
#     context = Context(context_dict)
#     html  = template.render(context)
#     result = StringIO.StringIO()
#     pdf = pisa.pisaDocument(StringIO.StringIO(
#         html.encode("UTF-8")), result)
#     if not pdf.err:
#         return http.HttpResponse(result.getvalue(), \
#              mimetype='application/pdf')
#     return http.HttpResponse('Gremlin%s' % cgi.escape(html))
#
# def view_pdf(request, id):
#     order = Request.objects.get(id=id)
#     offer = order.calculation_json
#     offer = json.loads(offer)
#     positions = offer['position']
#     return write_pdf('offer.html',{
#         'positions' : positions,
#         'offer':offer,
#         'order':order
#
#
#     })


# from django_xhtml2pdf.utils import generate_pdf
#
#
#
#
# def myview(request, id):
#     resp = HttpResponse(content_type='application/pdf')
#     result = generate_pdf('offer.html', file_object=resp)
#     return result

def render_to_pdf(template_src, context_dict):
    template = get_template(template_src)
    context = Context(context_dict)
    html = template.render(context)
    result = StringIO.StringIO()

    pdf = pisa.pisaDocument(StringIO.StringIO(html.encode("UTF-8")), result, link_callback=links, encoding='UTF-8')
    if not pdf.err:
        return HttpResponse(result.getvalue(), content_type='application/pdf')
    return HttpResponse('We had some errors<pre>%s</pre>' % escape(html))


def render_to_pdf_save(template_src, context_dict):
    template = get_template(template_src)
    context = Context(context_dict)
    html = template.render(context)
    result = StringIO.StringIO()

    pdf = pisa.pisaDocument(StringIO.StringIO(html.encode("ISO-8859-1")), result)
    if not pdf.err:
        return result
    return HttpResponse('We had some errors<pre>%s</pre>' % escape(html))


@login_required(login_url='/login/')
def myview(request, id):
    order = Request.objects.get(id=id)
    offer = order.calculation_json
    try:
        offer = json.loads(offer)
    except ValueError:
        offer = {}

    positions = offer.get('position', {})
    #Retrieve data or whatever you need
    return render_to_pdf(
        'offer_pdf.html',
        {
            'pagesize': 'A3',
            'positions': positions,
            'order': order,
            'offer': offer
        }
    )


@login_required(login_url='/login/')
def myview_test(request, id):
    order = Request.objects.get(id=id)
    offer = order.calculation_json
    try:
        offer = json.loads(offer)
    except ValueError:
        offer = {}

    positions = offer.get('position', {})
    #Retrieve data or whatever you need
    return TemplateResponse(request, 'offer_pdf.html', locals())


# def fetch_resources(uri, rel):
#     import os.path
#     from django.conf import settings
#     path = os.path.join(
#             settings.STATIC_ROOT,
#             uri.replace(settings.STATIC_URL, ""))
#     return path
links = lambda uri, rel: os.path.join(settings.MEDIA_ROOT, uri.replace(settings.MEDIA_URL, ''))


def export_pdf(request, id):
    order = Request.objects.get(id=id)
    offer = order.calculation_json
    try:
        offer = json.loads(offer)
    except ValueError:
        offer = {}
    positions = offer.get('position', {})
    html = render_to_string('offer_pdf.html', {'pagesize': 'A4', }, context_instance=RequestContext(request, {
        'pagesize': 'A4',
        'positions': positions,
        'order': order,
        'offer': offer
    }))
    result = StringIO.StringIO()
    pdf = pisa.pisaDocument(StringIO.StringIO(html.encode("UTF-8")), dest=result, link_callback=links)
    if not pdf.err:
        return result
    else:
        return None


def save_send_json(request, id):
    order = Request.objects.get(id=id)
    if request.method == "POST":
        print request.POST
        json_object = request.POST['json_object']
        order.calculation_json = json_object
        order.status = "2"
        order.save()
        file_to_be_sent = export_pdf(request, id).getvalue()
        email = request.POST['email']
        text = '''
Please find quatation attached
Thanks & regards
Paul Pullukaran
www.graphicsforcars.com.au
info@graphicsforcars.com.au
Tel. 969 709 53
'''
        msg = EmailMultiAlternatives('offer', text, 'test@test.ru', [email, ])

        msg.attach("offer.pdf", file_to_be_sent, "application/pdf")

        msg.send()
        return HttpResponse(json.dumps({
            'status': 'OK',
        })
        )


def add_category(request):
    if request.method == "POST":
        title = request.POST.get('category', False)
        if title:
            title = title.strip(' ')
            try:
                category = CompanyCategory.objects.get(title=title)
                return HttpResponse(json.dumps({
                    'status': 'GET',
                })
                )

            except:
                category = CompanyCategory.objects.create(title=title)
                return HttpResponse(json.dumps({
                    'status': 'OK',
                })
                )


def costumer_card_pdf(request, id):
    company = Company.objects.get(id=id)
    contact_person_list = company.contactperson_set.all()
    company_category_list = CompanyCategory.objects.all()
    order_list = company.company.all()
    status_list = ['1', '2', '3']

    return render_to_pdf(
        'database/customer-card-pdf.html',
        {
            'pagesize': 'A4',
            'company': company,
            'contact_person_list': contact_person_list,
            'company_category_list': company_category_list,
            'order_list': order_list,
            'status_list': status_list
        }
    )