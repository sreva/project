from django import forms
from django.forms import ModelForm
from .models import Company, ContactPerson



class NewCostumerForm(ModelForm):
    class Meta:
        model = Company
        exclude = ('category',)


class NewPersonForm(ModelForm):
    class Meta:
        model = ContactPerson
        exclude = ('company',)
