# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('office', '0016_myuser_group'),
    ]

    operations = [
        migrations.CreateModel(
            name='SettingsContent',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('title', models.CharField(max_length=300, verbose_name='\u041d\u0430\u0437\u0432\u0430\u043d\u0438\u0435')),
                ('key', models.CharField(max_length=100, verbose_name='\u041a\u043b\u044e\u0447', db_index=True)),
                ('image', models.ImageField(upload_to=b'')),
                ('is_image', models.BooleanField(default=False)),
                ('text', models.TextField(null=True, blank=True)),
                ('is_text', models.BooleanField(default=False)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
    ]
