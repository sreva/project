# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('office', '0018_auto_20150113_0843'),
    ]

    operations = [
        migrations.AddField(
            model_name='request',
            name='quotation_declined',
            field=models.TextField(null=True, blank=True),
            preserve_default=True,
        ),
    ]
