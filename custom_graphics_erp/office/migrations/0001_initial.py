# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import django.utils.timezone
from django.conf import settings
import office.models


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='MyUser',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('password', models.CharField(max_length=128, verbose_name='password')),
                ('last_login', models.DateTimeField(default=django.utils.timezone.now, verbose_name='last login')),
                ('email', models.EmailField(unique=True, max_length=255, verbose_name=b'email address')),
                ('date_of_birth', models.DateField()),
                ('is_active', models.BooleanField(default=True)),
                ('is_admin', models.BooleanField(default=False)),
            ],
            options={
                'abstract': False,
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Attachment',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('attachment_file', models.FileField(upload_to=b'uploads/')),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Company',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=100, null=True, verbose_name=b'name', blank=True)),
                ('email', models.EmailField(max_length=75, null=True, verbose_name=b'email', blank=True)),
                ('phone', models.CharField(max_length=50, null=True, verbose_name=b'phone', blank=True)),
                ('city', models.CharField(max_length=100, null=True, verbose_name=b'city', blank=True)),
                ('website', models.CharField(max_length=200, null=True, verbose_name=b'website', blank=True)),
                ('address', models.CharField(max_length=200, null=True, verbose_name=b'address', blank=True)),
                ('state', models.CharField(max_length=200, null=True, verbose_name=b'state', blank=True)),
                ('post_code', models.CharField(max_length=200, null=True, verbose_name=b'Post Code', blank=True)),
                ('description', models.TextField(null=True, verbose_name=b'description', blank=True)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='CompanyCategory',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('title', models.CharField(max_length=200, verbose_name=b'title')),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='ContactPerson',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=200, null=True, verbose_name=b'name', blank=True)),
                ('email', models.EmailField(max_length=75, null=True, verbose_name=b'email', blank=True)),
                ('phone', models.CharField(max_length=50, null=True, verbose_name=b'phone', blank=True)),
                ('position', models.CharField(max_length=100, null=True, verbose_name=b'Position', blank=True)),
                ('company', models.ForeignKey(to='office.Company')),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='CustomerType',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('title', models.CharField(max_length=200)),
                ('index_sort', models.IntegerField()),
            ],
            options={
                'verbose_name': 'customer type',
                'verbose_name_plural': 'customer type',
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Lamination',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('title', models.CharField(max_length=200)),
                ('index_sort', models.IntegerField()),
            ],
            options={
                'verbose_name': 'lamination',
                'verbose_name_plural': 'lamination',
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Material',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('title', models.CharField(max_length=200)),
                ('index_sort', models.IntegerField()),
            ],
            options={
                'verbose_name': 'material',
                'verbose_name_plural': 'material',
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Organizer',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('text', models.TextField()),
                ('date', models.DateTimeField()),
                ('user', models.FileField(upload_to=b'', verbose_name=office.models.MyUser)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Plot',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('title', models.CharField(max_length=200)),
                ('index_sort', models.IntegerField()),
            ],
            options={
                'verbose_name': 'plot',
                'verbose_name_plural': 'plot',
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='PlotPriceSam',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('title', models.CharField(max_length=200)),
                ('index_sort', models.IntegerField()),
            ],
            options={
                'verbose_name': 'Plot price sam',
                'verbose_name_plural': 'Plot price sam',
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Print',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('title', models.CharField(max_length=200)),
                ('index_sort', models.IntegerField()),
            ],
            options={
                'verbose_name': 'print',
                'verbose_name_plural': 'print',
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Request',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('order_details', models.TextField(verbose_name=b'Order Details')),
                ('price', models.IntegerField(verbose_name=b'Price')),
                ('payment', models.BooleanField(default=False, verbose_name=b'Payment')),
                ('graphics_description', models.TextField()),
                ('graphics_date_start', models.DateTimeField()),
                ('graphics_date_end', models.DateTimeField()),
                ('installation_description', models.TextField()),
                ('installation_date_start', models.DateTimeField()),
                ('installation_date_end', models.DateTimeField()),
                ('product_description', models.TextField()),
                ('product_date_start', models.DateTimeField()),
                ('product_date_end', models.DateTimeField()),
                ('status', models.CharField(max_length=50, choices=[(b'1', b'1'), (b'2', b'2'), (b'3', b'3'), (b'4', b'4'), (b'5', b'5')])),
                ('company', models.ForeignKey(related_name=b'company', verbose_name=b'Company', to='office.Company')),
                ('contact_person', models.ForeignKey(related_name=b'contact_person', verbose_name=b'Contact Person', to='office.ContactPerson')),
                ('graphics_user', models.ForeignKey(related_name=b'graphics_user', to=settings.AUTH_USER_MODEL)),
                ('installation_user', models.ForeignKey(related_name=b'installation_user', to=settings.AUTH_USER_MODEL)),
                ('office_manager', models.ForeignKey(related_name=b'office_manager', to=settings.AUTH_USER_MODEL)),
                ('product_user', models.ForeignKey(related_name=b'product_user', to=settings.AUTH_USER_MODEL)),
                ('sales_manager', models.ForeignKey(related_name=b'sales_manager', to=settings.AUTH_USER_MODEL)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.AddField(
            model_name='company',
            name='category',
            field=models.ForeignKey(related_name=b'category', verbose_name=b'category', to='office.CompanyCategory'),
            preserve_default=True,
        ),
    ]
