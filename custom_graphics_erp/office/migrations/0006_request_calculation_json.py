# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('office', '0005_auto_20141223_1537'),
    ]

    operations = [
        migrations.AddField(
            model_name='request',
            name='calculation_json',
            field=models.TextField(null=True, blank=True),
            preserve_default=True,
        ),
    ]
