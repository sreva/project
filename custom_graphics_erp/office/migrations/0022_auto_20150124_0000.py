# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('office', '0021_auto_20150119_0846'),
    ]

    operations = [
        migrations.AlterField(
            model_name='lamination',
            name='value',
            field=models.FloatField(),
        ),
        migrations.AlterField(
            model_name='material',
            name='value',
            field=models.FloatField(),
        ),
        migrations.AlterField(
            model_name='plot',
            name='value',
            field=models.FloatField(),
        ),
        migrations.AlterField(
            model_name='plotpricesam',
            name='value',
            field=models.FloatField(),
        ),
        migrations.AlterField(
            model_name='print',
            name='value',
            field=models.FloatField(),
        ),
    ]
