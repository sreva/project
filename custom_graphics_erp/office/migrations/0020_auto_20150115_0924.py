# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('office', '0019_request_quotation_declined'),
    ]

    operations = [
        migrations.AddField(
            model_name='request',
            name='sent_invoice',
            field=models.BooleanField(default=False),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='request',
            name='status',
            field=models.CharField(default=b'1', max_length=50, choices=[(b'1', b'Need Quotation'), (b'2', b'Quotation Sent'), (b'3', b'Offer Fail'), (b'4', b'New Order'), (b'5', b'Design'), (b'6', b'Production'), (b'7', b'Installation'), (b'8', b'Installation Finished'), (b'9', b'Archive Success'), (b'10', b'Archive Fail')]),
        ),
    ]
