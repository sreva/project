# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('office', '0022_auto_20150124_0000'),
    ]

    operations = [
        migrations.AlterField(
            model_name='company',
            name='category',
            field=models.ForeignKey(related_name=b'category', verbose_name=b'category', blank=True, to='office.CompanyCategory', null=True),
        ),
    ]
