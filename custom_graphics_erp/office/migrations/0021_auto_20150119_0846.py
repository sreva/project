# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('office', '0020_auto_20150115_0924'),
    ]

    operations = [
        migrations.AlterField(
            model_name='company',
            name='address',
            field=models.CharField(max_length=1000, null=True, verbose_name=b'address', blank=True),
        ),
        migrations.AlterField(
            model_name='request',
            name='status',
            field=models.CharField(default=b'1', max_length=50, choices=[(b'1', b'Need Quotation'), (b'2', b'Quotation Sent'), (b'3', b'Offer Fail'), (b'4', b'New Order'), (b'5', b'Design'), (b'6', b'Production'), (b'7', b'Installation'), (b'8', b'Order Finished'), (b'9', b'Archive Success'), (b'10', b'Archive Fail')]),
        ),
    ]
