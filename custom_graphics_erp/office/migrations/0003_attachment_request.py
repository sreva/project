# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('office', '0002_auto_20141223_1351'),
    ]

    operations = [
        migrations.AddField(
            model_name='attachment',
            name='request',
            field=models.ForeignKey(default=1, to='office.Request'),
            preserve_default=False,
        ),
    ]
