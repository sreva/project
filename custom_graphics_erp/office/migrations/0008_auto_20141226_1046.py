# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('office', '0007_auto_20141226_0837'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='customertype',
            name='value',
        ),
        migrations.AddField(
            model_name='customertype',
            name='material',
            field=models.FloatField(default=1),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='customertype',
            name='tisk',
            field=models.FloatField(default=1),
            preserve_default=False,
        ),
    ]
