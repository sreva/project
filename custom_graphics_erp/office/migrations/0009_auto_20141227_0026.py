# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('office', '0008_auto_20141226_1046'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='organizer',
            name='user',
        ),
        migrations.AlterField(
            model_name='companycategory',
            name='title',
            field=models.CharField(max_length=200, verbose_name=b'name'),
        ),
    ]
