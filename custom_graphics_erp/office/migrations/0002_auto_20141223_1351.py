# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        ('office', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='request',
            name='company',
            field=models.ForeignKey(related_name=b'company', verbose_name=b'Company', blank=True, to='office.Company', null=True),
        ),
        migrations.AlterField(
            model_name='request',
            name='contact_person',
            field=models.ForeignKey(related_name=b'contact_person', verbose_name=b'Contact Person', blank=True, to='office.ContactPerson', null=True),
        ),
        migrations.AlterField(
            model_name='request',
            name='graphics_date_end',
            field=models.DateTimeField(null=True, blank=True),
        ),
        migrations.AlterField(
            model_name='request',
            name='graphics_date_start',
            field=models.DateTimeField(null=True, blank=True),
        ),
        migrations.AlterField(
            model_name='request',
            name='graphics_description',
            field=models.TextField(null=True, blank=True),
        ),
        migrations.AlterField(
            model_name='request',
            name='graphics_user',
            field=models.ForeignKey(related_name=b'graphics_user', blank=True, to=settings.AUTH_USER_MODEL, null=True),
        ),
        migrations.AlterField(
            model_name='request',
            name='installation_date_end',
            field=models.DateTimeField(null=True, blank=True),
        ),
        migrations.AlterField(
            model_name='request',
            name='installation_date_start',
            field=models.DateTimeField(null=True, blank=True),
        ),
        migrations.AlterField(
            model_name='request',
            name='installation_description',
            field=models.TextField(null=True, blank=True),
        ),
        migrations.AlterField(
            model_name='request',
            name='installation_user',
            field=models.ForeignKey(related_name=b'installation_user', blank=True, to=settings.AUTH_USER_MODEL, null=True),
        ),
        migrations.AlterField(
            model_name='request',
            name='office_manager',
            field=models.ForeignKey(related_name=b'office_manager', blank=True, to=settings.AUTH_USER_MODEL, null=True),
        ),
        migrations.AlterField(
            model_name='request',
            name='order_details',
            field=models.TextField(null=True, verbose_name=b'Order Details', blank=True),
        ),
        migrations.AlterField(
            model_name='request',
            name='price',
            field=models.IntegerField(null=True, verbose_name=b'Price', blank=True),
        ),
        migrations.AlterField(
            model_name='request',
            name='product_date_end',
            field=models.DateTimeField(null=True, blank=True),
        ),
        migrations.AlterField(
            model_name='request',
            name='product_date_start',
            field=models.DateTimeField(null=True, blank=True),
        ),
        migrations.AlterField(
            model_name='request',
            name='product_description',
            field=models.TextField(null=True, blank=True),
        ),
        migrations.AlterField(
            model_name='request',
            name='product_user',
            field=models.ForeignKey(related_name=b'product_user', blank=True, to=settings.AUTH_USER_MODEL, null=True),
        ),
        migrations.AlterField(
            model_name='request',
            name='sales_manager',
            field=models.ForeignKey(related_name=b'sales_manager', blank=True, to=settings.AUTH_USER_MODEL, null=True),
        ),
        migrations.AlterField(
            model_name='request',
            name='status',
            field=models.CharField(default=b'1', max_length=50, choices=[(b'1', b'1'), (b'2', b'2'), (b'3', b'3'), (b'4', b'4'), (b'5', b'5')]),
        ),
    ]
