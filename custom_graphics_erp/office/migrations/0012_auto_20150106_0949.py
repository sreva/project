# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('office', '0011_auto_20150106_0827'),
    ]

    operations = [
        migrations.AddField(
            model_name='request',
            name='graphics_finish',
            field=models.BooleanField(default=False),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='request',
            name='installation_finish',
            field=models.BooleanField(default=False),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='request',
            name='product_finish',
            field=models.BooleanField(default=False),
            preserve_default=True,
        ),
    ]
