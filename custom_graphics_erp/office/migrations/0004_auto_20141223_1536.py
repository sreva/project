# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('office', '0003_attachment_request'),
    ]

    operations = [
        migrations.AddField(
            model_name='request',
            name='sending_sate',
            field=models.DateTimeField(null=True, blank=True),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='request',
            name='status',
            field=models.CharField(default=b'1', max_length=50, choices=[(b'1', b'1'), (b'2', b'Quotation Calculation'), (b'3', b'Quotation Sent'), (b'4', b'4'), (b'5', b'5')]),
        ),
    ]
