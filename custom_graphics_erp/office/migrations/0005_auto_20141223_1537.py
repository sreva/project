# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('office', '0004_auto_20141223_1536'),
    ]

    operations = [
        migrations.RenameField(
            model_name='request',
            old_name='sending_sate',
            new_name='sending_date',
        ),
    ]
