# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('office', '0017_settingscontent'),
    ]

    operations = [
        migrations.AlterField(
            model_name='settingscontent',
            name='image',
            field=models.ImageField(null=True, upload_to=b'', blank=True),
        ),
    ]
