# --*-- coding: utf-8 --*--

from django.db import models
from django.contrib.auth.models import (
    BaseUserManager, AbstractBaseUser,
    Group)


class MyUserManager(BaseUserManager):
    def create_user(self, email, date_of_birth, password=None):
        """
        Creates and saves a User with the given email, date of
        birth and password.
        """
        if not email:
            raise ValueError('Users must have an email address')

        user = self.model(
            email=self.normalize_email(email),
            date_of_birth=date_of_birth,
        )

        user.set_password(password)
        user.save(using=self._db)
        return user

    def create_superuser(self, email, date_of_birth, password):
        """
        Creates and saves a superuser with the given email, date of
        birth and password.
        """
        user = self.create_user(email,
                                password=password,
                                date_of_birth=date_of_birth
        )
        user.is_admin = True
        user.save(using=self._db)
        return user


class MyUser(AbstractBaseUser):
    email = models.EmailField(
        verbose_name='email address',
        max_length=255,
        unique=True,
    )
    name = models.CharField(max_length=400)
    phone = models.CharField(max_length=40)
    date_of_birth = models.DateField(blank=True, null=True)
    is_active = models.BooleanField(default=True)
    is_admin = models.BooleanField(default=False)
    group = models.ForeignKey(Group, blank=True, null=True)
    objects = MyUserManager()

    USERNAME_FIELD = 'email'
    REQUIRED_FIELDS = ['date_of_birth']

    def get_full_name(self):
        # The user is identified by their email address
        return self.email

    def get_short_name(self):
        # The user is identified by their email address
        return self.email

    def __str__(self):  # __unicode__ on Python 2
        return self.email

    def has_perm(self, perm, obj=None):
        "Does the user have a specific permission?"
        # Simplest possible answer: Yes, always
        return True

    def has_module_perms(self, app_label):
        "Does the user have permissions to view the app `app_label`?"
        # Simplest possible answer: Yes, always
        return True

    @property
    def is_staff(self):
        "Is the user a member of staff?"
        # Simplest possible answer: All admins are staff
        return self.is_admin


class CompanyCategory(models.Model):
    title = models.CharField(max_length=200, verbose_name='name')

    def __unicode__(self):
        return self.title


class Company(models.Model):
    name = models.CharField(max_length=100, verbose_name='name', null=True, blank=True)
    email = models.EmailField(verbose_name='email', null=True, blank=True)
    phone = models.CharField(max_length=50, verbose_name='phone', null=True, blank=True)
    category = models.ForeignKey(CompanyCategory, verbose_name='category', related_name='category', null=True,
                                 blank=True)
    city = models.CharField(max_length=100, verbose_name='city', null=True, blank=True)
    website = models.CharField(max_length=200, verbose_name='website', null=True, blank=True)
    address = models.CharField(max_length=1000, verbose_name='address', null=True, blank=True)
    state = models.CharField(max_length=200, verbose_name='state', null=True, blank=True)
    post_code = models.CharField(max_length=200, verbose_name='Post Code', null=True, blank=True)
    description = models.TextField(verbose_name='description', null=True, blank=True)

    def __unicode__(self):
        return self.name

    def get_contact_person_list(self):
        return self.contactperson_set.all()

        # def get_request_list(self):
        # return self.company


class ContactPerson(models.Model):
    company = models.ForeignKey(Company)
    name = models.CharField(max_length=200, verbose_name='name', null=True, blank=True)
    email = models.EmailField(verbose_name='email', null=True, blank=True)
    phone = models.CharField(max_length=50, verbose_name='phone', null=True, blank=True)
    position = models.CharField(max_length=100, verbose_name='Position', null=True, blank=True)

    def __unicode__(self):
        return self.name


STATUS = (
    ('1', 'Need Quotation'),
    ('2', 'Quotation Sent'),
    ('3', 'Offer Declined'),
    ('4', 'New Order'),
    ('5', 'Design'),
    ('6', 'Production'),
    ('7', 'Installation'),
    ('8', 'Order Finished'),
    ('9', 'Archive Success'),
    ('10', 'Archive Fail'),
)


class Request(models.Model):
    contact_person = models.ForeignKey(ContactPerson, verbose_name='Contact Person', related_name='contact_person',
                                       null=True, blank=True)
    company = models.ForeignKey(Company, verbose_name='Company', related_name='company', null=True, blank=True)
    order_details = models.TextField(verbose_name='Order Details', null=True, blank=True)
    sending_date = models.DateTimeField(null=True, blank=True)
    price = models.IntegerField(verbose_name='Price', null=True, blank=True)
    payment = models.BooleanField(verbose_name='Payment', default=False)
    sent_invoice = models.BooleanField(default=False)
    sales_manager = models.ForeignKey(MyUser, related_name='sales_manager', null=True, blank=True)
    office_manager = models.ForeignKey(MyUser, related_name='office_manager', null=True, blank=True)
    graphics_user = models.ForeignKey(MyUser, related_name='graphics_user', null=True, blank=True)
    graphics_description = models.TextField(null=True, blank=True)
    graphics_min = models.IntegerField(null=True, blank=True)
    graphics_date_start = models.DateTimeField(null=True, blank=True)
    graphics_date_end = models.DateTimeField(null=True, blank=True)
    graphics_finish = models.BooleanField(default=False)
    installation_user = models.ForeignKey(MyUser, related_name='installation_user', null=True, blank=True)
    installation_description = models.TextField(null=True, blank=True)
    installation_date_start = models.DateTimeField(null=True, blank=True)
    installation_min = models.IntegerField(null=True, blank=True)
    installation_date_end = models.DateTimeField(null=True, blank=True)
    installation_finish = models.BooleanField(default=False)
    product_user = models.ForeignKey(MyUser, related_name='product_user', null=True, blank=True)
    product_description = models.TextField(null=True, blank=True)
    product_min = models.IntegerField(null=True, blank=True)
    product_date_start = models.DateTimeField(null=True, blank=True)
    product_date_end = models.DateTimeField(null=True, blank=True)
    product_finish = models.BooleanField(default=False)
    status = models.CharField(max_length=50, choices=STATUS, default='1')
    quotation_declined = models.TextField(null=True, blank=True)
    calculation_json = models.TextField(null=True, blank=True)


    def get_attachment_list(self):
        return self.attachment_set.all()


class Attachment(models.Model):
    request = models.ForeignKey(Request)
    attachment_file = models.FileField(upload_to='uploads/')

    def get_file_name(self):
        return self.attachment_file.name.split('/')[1]


class Organizer(models.Model):
    text = models.TextField()
    date = models.DateTimeField()
    user = models.ForeignKey(MyUser)


class AbstractModel(models.Model):
    title = models.CharField(max_length=200)
    value = models.FloatField()
    index_sort = models.IntegerField()

    class Meta:
        abstract = True
        ordering = ['index_sort', ]


class Material(AbstractModel):
    class Meta:
        verbose_name = 'material'
        verbose_name_plural = 'material'

    def __unicode__(self):
        return self.title


class Lamination(AbstractModel):
    class Meta:
        verbose_name = 'lamination'
        verbose_name_plural = 'lamination'

    def __unicode__(self):
        return self.title


class PlotPriceSam(AbstractModel):
    class Meta:
        verbose_name = 'Plot price sam'
        verbose_name_plural = 'Plot price sam'

    def __unicode__(self):
        return self.title


class CustomerType(models.Model):
    title = models.CharField(max_length=200)
    material = models.FloatField()
    tisk = models.FloatField()
    index_sort = models.IntegerField()


    class Meta:
        verbose_name = 'customer type'
        verbose_name_plural = 'customer type'

    def __unicode__(self):
        return self.title


class Print(AbstractModel):
    class Meta:
        verbose_name = 'print'
        verbose_name_plural = 'print'

    def __unicode__(self):
        return self.title


class Plot(AbstractModel):
    class Meta:
        verbose_name = 'plot'
        verbose_name_plural = 'plot'

    def __unicode__(self):
        return self.title


class SettingsContent(models.Model):
    """
        {% load get_keys %}
        {% get_keys 'kest' %}
    """
    title = models.CharField(max_length=300, verbose_name=u'Название')
    key = models.CharField(max_length=100, db_index=True, verbose_name=u'Ключ')

    image = models.ImageField(blank=True, null=True)
    is_image = models.BooleanField(default=False)
    text = models.TextField(blank=True, null=True)
    is_text = models.BooleanField(default=False)


    def get_value(self):
        if self.is_image:
            return self.image.url
        elif self.is_text:
            return self.text
        else:
            return ''
