from django import template
from house.models import SettingsContent


register = template.Library()
# from product.models import SettingsContent


@register.simple_tag
def get_keys(inputs):
    try:
        content = SettingsContent.objects.get(key=inputs)
        return content.get_value
    except SettingsContent.DoesNotExist:
        return ''